import { Action } from "@ngrx/store";
import { IOrder } from "../../core/models/order";
import * as OrderActions from "../../states/actions/order.action";

const initialState: IOrder={
  orderId: "",
  customerRef: "",
  typeGoodRef: "",
  recommandedVehiclesIds: "",
  customerBillRef: "",
  status: "",
  tonnage: 0,
  price: 0,
  isActive: false,
  mpl: 0,
  route: [],
  typePalletteRef: "",
  palletteQte: 0,
  comment: ""
}

export function reducer(state: IOrder[]=[initialState], action: OrderActions.Actions) {
  switch (action.type) {
    case OrderActions.ADD_ORDER:
      return [...state, action.payload]
    default:
      return state;
  }
}
