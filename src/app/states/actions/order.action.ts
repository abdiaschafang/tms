import { Injectable } from "@angular/core";
import { Action } from "@ngrx/store";
import { IOrder } from "../../core/models/order";

export const ADD_ORDER = '[ORDER] Add'
export const DELETE_ORDER='[ORDER] Remove'

export class AddOrder implements Action {
  readonly type=ADD_ORDER

  constructor(public payload: IOrder) {}
}

export class DeleteOrder implements Action {
  readonly type=DELETE_ORDER

  constructor(public payload: IOrder) {}
}

export type Actions = AddOrder | DeleteOrder
