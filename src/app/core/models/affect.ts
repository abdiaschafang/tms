export interface IAffect {
    carrier: string;
    price: number;
}

export class Affect implements IAffect {
    constructor(
        public carrier: string,
        public price: number,
    ) {}
}