
export interface ITransportTracking {
  lastModifiedBy?: string,
  createdBy?: string,
  transportTrackingId?: string,
  transport?: string,
  cmrFile?: string,
  createdAt?: Date,
  lastModifiedDate?: Date,
  orderFile?: string,
  status?: string
}

export class transport implements ITransportTracking {
  constructor(
    public lastModifiedBy: string,
    public createdBy: string,
    public transportTrackingId: string,
    public transport: string,
    public cmrFile: string,
    public createdAt: Date,
    public lastModifiedDate: Date,
    public orderFile: string,
    public status: string
  ) { }
}
