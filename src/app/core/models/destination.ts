import { Time } from "@angular/common";

export interface IDestination {
    loadRef: string;
    address: string;
    city: string;
    countryName: string;
    countryCode: string;
    dateStr: Date;
    destinationType: string;
    tonnage: number;
    zipcode: string;
    mpl: string;
}

export class Destination implements IDestination {
    constructor(
        public loadRef: string,
        public address: string,
        public city: string,
        public countryName: string,
        public countryCode: string,
        public dateStr: Date,
        public destinationType: string,
        public tonnage: number,
        public zipcode: string,
        public mpl: string,
    ) {}
}
