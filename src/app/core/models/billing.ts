import { Time } from "@angular/common";

export interface IBilling {
    createdAt: string;
    lastModifiedDate: string;
    lastModifiedBy: string;
    createdBy: string;
    billId: string;
    code: Date;
    status: string;
    paymentDate: number;
    comment: string;
    description: string;
    customerId: number;
    customerName: string;
    customerVat: string;
    customerAdress: string;
    amount: number;
    billDate: string;
    dueDate: number;
    vatValue: number;
    transportCodes: string;
    transportPrices: string;
}

export class Billing implements IBilling {
    constructor(
        public createdAt: string,
        public lastModifiedDate: string,
        public lastModifiedBy: string,
        public createdBy: string,
        public billId: string,
        public code: Date,
        public status: string,
        public paymentDate: number,
        public comment: string,
        public description: string,
        public customerId: number,
        public customerName: string,
        public customerVat: string,
        public customerAdress: string,
        public amount: number,
        public billDate: string,
        public  dueDate: number,
        public vatValue: number,
        public transportCodes: string,
        public transportPrices: string,
    ) {}
}