export interface IOrder {
  orderId: string;
  customerRef: string;
  typeGoodRef: string;
  recommandedVehiclesIds: any;
  customerBillRef: string;
  status: string;
  tonnage: number;
  price: number;
  isActive: boolean;
  mpl: number;
  route: never[];
  typePalletteRef: string;
  palletteQte: number;
  comment: string;
}


export class Order implements IOrder {
  constructor(
    public orderId: string,
    public customerRef: string,
    public typeGoodRef: string,
    public recommandedVehiclesIds: any,
    public customerBillRef: string,
    public status: string,
    public tonnage: number,
    public isActive: boolean,
    public price: number,
    public mpl: number,
    public route: [],
    public typePalletteRef: string,
    public palletteQte: number,
    public comment: string
  ) { }
}
