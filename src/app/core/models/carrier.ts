export interface ICarrier {
    paymentPeriod: number;
    licenceDAte: Date;
    insuranceDAte: Date;
}

export class Carrier implements ICarrier {
    constructor(
        public paymentPeriod: number,
        public licenceDAte: Date,
        public insuranceDAte: Date,
    ) {}
}