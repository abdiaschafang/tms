export interface ICustomer {
    name: string,
    code: string
}

export class Customer implements ICustomer {
    constructor(
        public name: string,
        public code: string,
    ) {}
}