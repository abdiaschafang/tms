export interface IConfiguration {
    key: string,
    value: string,

}


export class Configuration implements IConfiguration {
    constructor(
     public key: string,
     public value: string,
    ) {}
}