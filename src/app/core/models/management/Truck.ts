

export interface ITruck {
    name: string,
    mpl: number,
    tonnage: number,
    description: string,
    status: string,
}


export class Truck implements ITruck {
    constructor(
     public name: string,
     public mpl: number,
     public tonnage: number,
     public description: string,
     public status: string,
    ) {}
}