

export interface IGoods {
    name: string,
    description: string,
    status: string,
}


export class Goods implements IGoods {
    constructor(
     public name: string,
     public description: string,
     public status: string,
    ) {}
}