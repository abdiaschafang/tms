import { Transporter } from './transporter';

export interface IBankingInfo {
    iban: string,
    bic: string,
    transporter: Transporter
}


export class BankingInfo implements IBankingInfo {
    constructor(
     public iban: string,
     public bic: string,
     public transporter: Transporter
    ) {}
}