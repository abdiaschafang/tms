export interface ITransporter {
    name: string,
    tva: string,
    siren: string,
    siret: string,
    countryCode: string,
    city: string,
    address: string,
    zipcode: string,
    phone: string,
    fax: string,
    email: string,
    paymentPeriod: number,
    licenceDate: string,
    assuranceDate: string,
    licenceUrl: string,
    assuranceUrl: string,
    status: string,
    bankingInfos: [ {}],
    drivers: [{}],
    trucks:[ {}]
}


export class Transporter implements ITransporter {
    constructor(
     public name: string,
     public tva: string,
     public siren: string,
     public siret: string,
     public countryCode: string,
     public city: string,
     public address: string,
     public zipcode: string,
     public phone: string,
     public fax: string,
     public email: string,
     public paymentPeriod: number,
     public licenceDate: string,
     public assuranceDate: string,
     public licenceUrl: string,
     public assuranceUrl: string,
     public status: string,
     public bankingInfos: [ {}],
     public drivers: [{}],
     public trucks:[ {}]
    ) {}
}