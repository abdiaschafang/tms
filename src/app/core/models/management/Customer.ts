export interface ICustomer {
  name: string,
  tva: string,
  siren: string,
  siret: string,
  countryCode: string,
  city: string,
  address: string,
  zipcode: string,
  valueTva: string,
  phone: string,
  fax: string,
  email: string,
  paymentsDaysNumber: number,
  paymentsDaysPeriod: number,
  status: string,
  outstanding: string,
  payments: [{}],
}


export class Customer implements ICustomer {
  constructor(
    public name: string,
    public tva: string,
    public siren: string,
    public siret: string,
    public countryCode: string,
    public city: string,
    public address: string,
    public zipcode: string,
    public valueTva: string,
    public phone: string,
    public fax: string,
    public email: string,
    public paymentsDaysNumber: number,
    public paymentsDaysPeriod: number,
    public status: string,
    public outstanding: string,
    public payments: [{}],

  ) { }
}
