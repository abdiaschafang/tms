export interface IPaymentMethod {
    type: string,
    description: string,
    status: string,
}


export class M_Payment_Method implements IPaymentMethod {
    constructor(
     public type: string,
     public description: string,
     public status: string,
    ) {}
}