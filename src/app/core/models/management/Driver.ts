import { Transporter } from './transporter';

export interface IDriver {
    name: string,
    surname: string,
    phone: number,
    email: string,
    status: string,
    transporter: Transporter
}


export class Driver implements IDriver {
    constructor(
     public name: string,
     public surname: string,
     public phone: number,
     public email: string,
     public status: string,
     public transporter: Transporter
    ) {}
}