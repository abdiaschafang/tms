export interface ICustomerUser {
    tmsUserName: string
}


export class CustomerUser implements ICustomerUser {
    constructor(
     public tmsUserName: string
    ) {}
}
