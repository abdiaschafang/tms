
export interface IPallets {
    type: string,
    length: number,
    width: number,
    status: string,
}


export class Pallets implements IPallets {
    constructor(
     public type: string,
     public length: number,
     public width: number,
     public status: string,
    ) {}
}