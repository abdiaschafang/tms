
export interface ITransport {
  transportId?: string,
  carrierRef?: string,
  code?: string,
  comment?: string,
  driverRef?: string,
  loadDate?: Date,
  orderRef?: string,
  price?: number
  status?: string,
  userCompany?: string,
  vehicleRef?: string
}

export class transport implements ITransport {
    constructor(
        public transportId: string,
        public carrierRef: string,
        public code: string,
        public comment: string,
        public driverRef: string,
        public orderRef: string,
        public loadDate: Date,
        public price: number,
        public status: string,
        public userCompany: string,
        public vehicleRef: string
    ) {}
}
