import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'format-date'})
export class FormatDatePipe implements PipeTransform {
    transform(date: Date): any {
      let dt = new Date(date);
      let mm = dt.getMonth() + 1;
      let dd = dt.getDate();
      let yyyy = dt.getFullYear();
      return dd + '/' + mm + '/' + yyyy;
    }
}