import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITransporter } from '../../models/management/transporter';
import { IDriver } from '../../models/management/Driver';
import { SharedService } from '../shared/shared.service';
import { catchError, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ICustomer } from '../../models/management/Customer';
import { IBankingInfo } from '../../models/management/bankingInfo';
import { IGoods } from '../../models/management/Goods';
import { IPallets } from '../../models/management/Pallets';
import { IConfiguration } from '../../models/management/Configuration';
import { ITruck } from '../../models/management/Truck';
import { IPaymentMethod } from '../../models/management/PaymentMethod';

@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(
    protected http: HttpClient,
    protected sharedService: SharedService) { }

  protected resourcePath={

    urlCreateCustomer: '/customer',
    urlSearchCustomer: '/order/management/customer/search',
    urlUpdateCustomer: '/customer',
    urldeleteCustomer: '/customer',
    urlCustomerAddPaymentMethod: '/customer/attributePaymentMethod',
    urlRemoveCustomerPaymentMethod: '/customer/removePaymentMethod',

    urlCreateTransporter: '/transporter',
    urlTransporterAddTruck: '/transporter/attributeTruck',
    urlRemoveTransporterTruck: '/transporter/removeTruck',
    urlSearchTransporter: '/transport/management/transporter/search',
    urlUpdateTransporter: '/transporter',
    urlDeleteTransporter: '/transporter',

    urlCreateDriver: '/driver',
    urlSearchDriver: '/driver/search',
    urlDeleteDriver: '/driver',
    urlUpdateDriver: '/driver',

    urlCreateBankingInfo: '/bankingInformations',
    urlSearchBankingInfo: '/bankingInformations/search',
    urlUpdateBankingInfo: '/bankingInformations',
    urlDeleteBankingInfo: '/bankingInformations',

    urlCreateGoods: '/goods',
    urlSearchGoods: '/order/management/goods/search',
    urlUpdateGoods: '/goods',
    urlDeleteGoods: '/goods',

    urlUpdatePallets: '/pallet',
    urlSearchPallets: '/order/management/pallet/search',
    urlCreatePallets: '/pallet',
    urlDeletePallets: '/pallet',

    urlSearchConfiguration: '/configuration/search',
    urlUpdateConfiguration: '/configuration',
    urlCreateConfiguration: '/configuration',
    urlDeleteConfiguration: '/configuration',

    urlSearchTruck: '/truck/search',
    urlUpdateTruck: '/truck',
    urlCreateTruck: '/truck',
    urlDeleteTruck: '/truck',

    urlCreatePaymentMethod: '/paymentMethod',
    urlSearchPaymentMethod: '/paymentMethod/search',
    urlUpdatePaymentMethod: '/paymentMethod',
    urlDeletePaymentMethod: '/paymentMethod',

  }

  public createTransporter(body: any): Observable<ITransporter[]> {
    return this.http.post<ITransporter[]>(environment.microserviceManagement+this.resourcePath.urlCreateTransporter, body).pipe(
      tap(transporter => console.log('transporter: ', transporter)),
      catchError(this.sharedService.handleError)
    );
  }

  public AddTruckOnTransporter(trucksId: any, transporterId: string): Observable<ITransporter[]> {
    return this.http.post<ITransporter[]>(`${environment.microserviceManagement+this.resourcePath.urlTransporterAddTruck}?transporter=${transporterId}&trucks=${trucksId}`, trucksId).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllTransporter(body: any): Observable<ITransporter[]> {
    return this.http.put<ITransporter[]>(`${environment.microserviceExploitation+this.resourcePath.urlSearchTransporter}`, body).pipe(
      tap(transporters => console.log('transporters: ', transporters)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateTransporter(transporter: any, id: string): Observable<ITransporter[]> {
    return this.http.put<ITransporter[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateTransporter}/${id}`, transporter).pipe(
      tap(transporter => console.log('transporter updated: ', transporter)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteTransporter(transporterId: string): Observable<ITransporter[]> {
    return this.http.delete<ITransporter[]>(`${environment.microserviceManagement+this.resourcePath.urlDeleteTransporter}/${transporterId}`).pipe(
      tap(transporter => console.log('transporter deleted: ', transporter)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteTransporterTruck(transporterId: string, truckId: string): Observable<ITransporter[]> {
    return this.http.delete<ITransporter[]>(`${environment.microserviceManagement+this.resourcePath.urlRemoveTransporterTruck}?transporter=${transporterId}&truck=${truckId}`).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public AddPaymentMethodOnTransporter(paymentMethodId: any, customerId: string): Observable<ICustomer[]> {
    return this.http.post<ICustomer[]>(`${environment.microserviceManagement+this.resourcePath.urlCustomerAddPaymentMethod}?customer=${customerId}&payment_methods=${paymentMethodId}`, paymentMethodId).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public removePaymentMethodOnCustomer(customerId: string, paymentMethodId: string): Observable<ICustomer[]> {
    return this.http.delete<ICustomer[]>(`${environment.microserviceManagement+this.resourcePath.urlRemoveCustomerPaymentMethod}?customer=${customerId}&payment_method=${paymentMethodId}`).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public createCustomer(body: any): Observable<ICustomer[]> {
    return this.http.post<ICustomer[]>(environment.microserviceManagement+this.resourcePath.urlCreateCustomer, body).pipe(
      tap(customer => console.log('customer: ', customer)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllCustomer(body: any): Observable<ICustomer[]> {
    return this.http.put<ICustomer[]>(`${environment.microserviceCommercial+this.resourcePath.urlSearchCustomer}`, body).pipe(
      tap(customer => console.log()),
      catchError(this.sharedService.handleError)
    );
  }

  public updateCustomer(customer: any, id: string): Observable<ICustomer[]> {
    return this.http.put<ICustomer[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateCustomer}/${id}`, customer).pipe(
      tap(customer => console.log('customer updated: ', customer)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteCustomer(customerId: string): Observable<ICustomer[]> {
    return this.http.delete<ICustomer[]>(`${environment.microserviceManagement+this.resourcePath.urldeleteCustomer}/${customerId}`).pipe(
      tap(customer => console.log('customer deleted: ', customer)),
      catchError(this.sharedService.handleError)
    );
  }

  public createDriver(body: any): Observable<IDriver[]> {
    return this.http.post<IDriver[]>(environment.microserviceManagement+this.resourcePath.urlCreateDriver, body).pipe(
      tap(driver => console.log('driver: ', driver)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllDriver(body: any): Observable<IDriver[]> {
    return this.http.put<IDriver[]>(`${environment.microserviceManagement+this.resourcePath.urlSearchDriver}`, body).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public updateDriver(body: any, id: any): Observable<IDriver[]> {
    return this.http.put<IDriver[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateDriver}/${id}`, body).pipe(
      tap(driver => console.log('driver: ', driver)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteDriver(driverId: string): Observable<IDriver[]> {
    return this.http.delete<IDriver[]>(`${environment.microserviceManagement+this.resourcePath.urlDeleteDriver}/${driverId}`).pipe(
      tap(driver => console.log('driver deleted: ', driver)),
      catchError(this.sharedService.handleError)
    );
  }

  public createBankingInfo(body: any): Observable<IBankingInfo[]> {
    return this.http.post<IBankingInfo[]>(environment.microserviceManagement+this.resourcePath.urlCreateBankingInfo, body).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllBankingInfo(body: any): Observable<IBankingInfo[]> {
    return this.http.put<IBankingInfo[]>(`${environment.microserviceManagement+this.resourcePath.urlSearchBankingInfo}`, body).pipe(
      tap(bankinginfo => console.log('bankinginfo: ', bankinginfo)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateBankingInfo(body: any, id: any): Observable<IBankingInfo[]> {
    return this.http.put<IBankingInfo[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateBankingInfo}/${id}`, body).pipe(
      tap(bankinginfo => console.log('bankinginfo: ', bankinginfo)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteBankingInfo(bankingInfoId: string): Observable<IBankingInfo[]> {
    return this.http.delete<IBankingInfo[]>(`${environment.microserviceManagement+this.resourcePath.urlDeleteBankingInfo}/${bankingInfoId}`).pipe(
      tap(bankingInfo => console.log('bankingInfo deleted: ', bankingInfo)),
      catchError(this.sharedService.handleError)
    );
  }

  public createGoods(body: any): Observable<IGoods[]> {
    return this.http.post<IGoods[]>(environment.microserviceManagement+this.resourcePath.urlCreateGoods, body).pipe(
      tap(goods => console.log('goods: ', goods)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllGoods(body: any): Observable<IGoods[]> {
    return this.http.put<IGoods[]>(`${environment.microserviceCommercial+this.resourcePath.urlSearchGoods}`, body).pipe(
      tap(goods => console.log('goods: ', goods)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateGoods(body: any, id: any): Observable<IGoods[]> {
    return this.http.put<IGoods[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateGoods}/${id}`, body).pipe(
      tap(good => console.log('good: ', good)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteGoods(goodsId: string): Observable<IGoods[]> {
    return this.http.delete<IGoods[]>(`${environment.microserviceManagement+this.resourcePath.urlDeleteGoods}/${goodsId}`).pipe(
      tap(goods => console.log('goods deleted: ', goods)),
      catchError(this.sharedService.handleError)
    );
  }

  public updatePallets(body: any, id: any): Observable<IPallets[]> {
    return this.http.put<IPallets[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdatePallets}/${id}`, body).pipe(
      tap(pallets => console.log('pallets: ', pallets)),
      catchError(this.sharedService.handleError)
    );
  }

  public createPallets(body: any): Observable<IPallets[]> {
    return this.http.post<IPallets[]>(environment.microserviceManagement+this.resourcePath.urlCreatePallets, body).pipe(
      tap(pallets => console.log('pallets: ', pallets)),
      catchError(this.sharedService.handleError)
    );
  }

  public deletePallets(palletsId: string): Observable<IPallets[]> {
    return this.http.delete<IPallets[]>(`${environment.microserviceManagement+this.resourcePath.urlDeletePallets}/${palletsId}`).pipe(
      tap(pallets => console.log('pallets deleted: ', pallets)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllPallets(body: any): Observable<IPallets[]> {
    return this.http.put<IPallets[]>(`${environment.microserviceCommercial+this.resourcePath.urlSearchPallets}`, body).pipe(
      tap(pallets => console.log('pallets: ', pallets)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllConfiguration(body: any): Observable<IConfiguration[]> {
    return this.http.put<IConfiguration[]>(`${environment.microserviceManagement+this.resourcePath.urlSearchConfiguration}`, body).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public createConfiguration(body: any): Observable<IConfiguration[]> {
    return this.http.post<IConfiguration[]>(environment.microserviceManagement+this.resourcePath.urlCreateConfiguration, body).pipe(
      tap(configuration => console.log('configuration: ', configuration)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateConfiguration(body: any, id: any): Observable<IConfiguration[]> {
    return this.http.put<IConfiguration[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateConfiguration}/${id}`, body).pipe(
      tap(configuration => console.log('configuration: ', configuration)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteConfiguration(configurationId: string): Observable<IConfiguration[]> {
    return this.http.delete<IConfiguration[]>(`${environment.microserviceManagement+this.resourcePath.urlDeleteConfiguration}/${configurationId}`).pipe(
      tap(truck => console.log('Configuration deleted: ', truck)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllTruck(body: any): Observable<ITruck[]> {
    return this.http.put<ITruck[]>(`${environment.microserviceManagement+this.resourcePath.urlSearchTruck}`, body).pipe(
      tap(trucks => console.log('trucks: ', trucks)),
      catchError(this.sharedService.handleError)
    );
  }

  public createTruck(body: any): Observable<ITruck[]> {
    return this.http.post<ITruck[]>(environment.microserviceManagement+this.resourcePath.urlCreateTruck, body).pipe(
      tap(truck => console.log('truck: ', truck)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateTruck(body: any, id: any): Observable<ITruck[]> {
    return this.http.put<ITruck[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdateTruck}/${id}`, body).pipe(
      tap(truck => console.log('truck: ', truck)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteTruck(truckId: string): Observable<ITruck[]> {
    return this.http.delete<ITruck[]>(`${environment.microserviceManagement+this.resourcePath.urlDeleteTruck}/${truckId}`).pipe(
      tap(truck => console.log('truck deleted: ', truck)),
      catchError(this.sharedService.handleError)
    );
  }

  public createPaymentMethod(body: any): Observable<IPaymentMethod[]> {
    return this.http.post<IPaymentMethod[]>(environment.microserviceManagement+this.resourcePath.urlCreatePaymentMethod, body).pipe(
      tap(paymentMethod => console.log('paymentMethod: ', paymentMethod)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllPaymentMethod(body: any): Observable<IPaymentMethod[]> {
    return this.http.put<IPaymentMethod[]>(`${environment.microserviceManagement+this.resourcePath.urlSearchPaymentMethod}`, body).pipe(
      tap(),
      catchError(this.sharedService.handleError)
    );
  }

  public updatePaymentMethod(body: any, id: any): Observable<IPaymentMethod[]> {
    return this.http.put<IPaymentMethod[]>(`${environment.microserviceManagement+this.resourcePath.urlUpdatePaymentMethod}/${id}`, body).pipe(
      tap(paymentMethod => console.log('paymentMethod: ', paymentMethod)),
      catchError(this.sharedService.handleError)
    );
  }

  public deletePaymentMethod(paymentMethodId: string): Observable<IPaymentMethod[]> {
    return this.http.delete<IPaymentMethod[]>(`${environment.microserviceManagement+this.resourcePath.urlDeletePaymentMethod}/${paymentMethodId}`).pipe(
      tap(paymentMethod => console.log('paymentMethod deleted: ', paymentMethod)),
      catchError(this.sharedService.handleError)
    );
  }

}
