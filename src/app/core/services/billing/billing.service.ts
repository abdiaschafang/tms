import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from '../shared/shared.service';
import { IOrder } from '../../models/order';
import { ITransport } from '../../models/transport';
import { ICustomer } from '../../models/management/Customer';
import { catchError, Observable, tap, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Billing, IBilling } from '../../models/billing';
import { ITransporter } from '../../models/management/transporter';

@Injectable({
  providedIn: 'root'
})
export class BillingService {

  constructor(
    protected http: HttpClient,
    protected sharedService: SharedService) { }

  protected ressourcePath={

    urlBillingCustomerInformation: '/billing/customer/informations',
    urlBillingAdd: '/billing/add',
    urlCreditAdd: '/avoir/add',
    urlCustomerBillingInformations: '/billing/customerList/informations',
    urlTransportsInformationsByBillId : '/avoir/transport/informations',
    urlGetAllBill : '/search',

    urlGetAllCarrierBill : '/carrierVat/carrierBills', 
    urlGetCarrierBillInformation : '/carrierVat/transportList/',
    urlTransporterBillingAdd : '/carrier/add',
  

  }
 
  public getBillingCustomerInformations(customerRef: string): Observable<ICustomer> {
    return this.http.get<ICustomer>(`${environment.microserviceBilling+this.ressourcePath.urlBillingCustomerInformation}?customerRef=${customerRef}`).pipe(
      tap(customer => console.log('customer: ', customer)),
      catchError(this.sharedService.handleError)
    );
  }
  

  public saveBill(billingDto: object): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceBilling}${this.ressourcePath.urlBillingAdd}`, billingDto).pipe(
      tap(Billing => console.log('billing: ', Billing)),
      catchError(this.sharedService.handleError)
    );
  }

  public getCustomerBillingInformations(customerRef : string): Observable<ICustomer> {
    return this.http.get<ICustomer>(`${environment.microserviceBilling+this.ressourcePath.urlCustomerBillingInformations}?customerRef=${customerRef}`).pipe(
      tap(billingInfos => console.log('billingInfos: ', billingInfos)),
      catchError(this.sharedService.handleError)
    );
  }

  public getTransportsInformationsByBillId(billId : string): Observable<ICustomer> {
    return this.http.get<ICustomer>(`${environment.microserviceBilling+this.ressourcePath.urlTransportsInformationsByBillId}?billId=${billId}`).pipe(
      tap(TransportInfos => console.log('TransportInfos: ', TransportInfos)),
      catchError(this.sharedService.handleError)
    );
  }

  public saveCredit(avoirDto: object): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceBilling}${this.ressourcePath.urlCreditAdd}`, avoirDto).pipe(
      tap(credit => console.log('credit: ', credit)),
      catchError(this.sharedService.handleError)
    );
  }

  public getAllBill(body: any): Observable<IBilling[]> {
    return this.http.put<IBilling[]>(`${environment.microserviceBilling+this.ressourcePath.urlGetAllBill}`, body).pipe(
      tap(billing => console.log()),
      catchError(this.sharedService.handleError)
    );
  }

  //transporter bill

  public getAllCarrierBill(): Observable<IBilling> {
    return this.http.get<IBilling>(`${environment.microserviceCarrierBilling+this.ressourcePath.urlGetAllCarrierBill}`).pipe(
      tap(TransportCarrierBill => console.log('TransportCarrierBill: ', TransportCarrierBill)),
      catchError(this.sharedService.handleError)
    );
  }
  
  public getCarrierBillInformation(carrierRef: string): Observable<ITransporter> {
    return this.http.get<ITransporter>(`${environment.microserviceCarrierBilling+this.ressourcePath.urlGetCarrierBillInformation}${carrierRef}`).pipe(
      tap(customer => console.log('customer: ', customer)),
      catchError(this.sharedService.handleError)
    );
  }

  public saveTransporterBill(billingDto: object): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceCarrierBilling}${this.ressourcePath.urlTransporterBillingAdd}`, billingDto).pipe(
      tap(Billing => console.log('billing: ', Billing)),
      catchError(this.sharedService.handleError)
    );
  }

}
