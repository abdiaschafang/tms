import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BehaviorSubject, catchError, Observable, tap, throwError } from 'rxjs';
import { CommercialService } from '../../../core/services/commercial/commercial.service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SharedService {

  protected ressourcePath={
    urlGetCountries: 'country/listCountries',
    urlGetCyties: 'country/listCitiesByCountry'
  }

  private elementState='close';
  private elementStateChanged$=new BehaviorSubject<string>(this.elementState);
  public elementStateObservable$=this.elementStateChanged$.asObservable();
  public dataTest!: any;
  public destinations!: any;



  private showOrHideModal=new BehaviorSubject(false);
  Modal=this.showOrHideModal.asObservable();

  private showOrHideDocModal=new BehaviorSubject(false);
  docModal=this.showOrHideDocModal.asObservable();

  private messageSource=new BehaviorSubject([]);
  currentMessage=this.messageSource.asObservable();

  constructor(
    protected http: HttpClient,
    private messageService: MessageService) {
    this.elementStateChanged$.next('open');
  }

  public getCountries(): Observable<any[]> {
    return this.http.get<[]>(`${environment.restCountries}/${this.ressourcePath.urlGetCountries}?param=`).pipe(
      tap(countries => console.log('countries: ', countries)),
      catchError(this.handleError)
    );
  }

  public getCities(country: string, town: string): Observable<any[]> {
    return this.http.get<[]>(`${environment.restCities}/${this.ressourcePath.urlGetCyties}?country=${country}&town=${town}`).pipe(
      tap(cities => console.log('cities: ', cities)),
      catchError(this.handleError)
    );
  }

  showOrHideAllModal(displayModal: boolean) {
    this.showOrHideModal.next(!displayModal);
  }

  showOrHideAllDocModal(displayModal: boolean) {
    this.showOrHideDocModal.next(!displayModal);
  }

  toggle() {
    this.elementState=this.elementState==='open'? 'close':'open';
    this.elementStateChanged$.next(this.elementState);
  }

  changeMessage(message: any) {
    this.messageSource.next(message);
  }

  toastMessageOrders(data: any) {
    if (data?.length>0) {
      this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Orders retrieved with success' });
    } else if (data?.length===0) {
      this.messageService.add({ severity: 'info', summary: 'Service Message', detail: 'Oops no order found' });
    } else {
      this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'Error while retrieving orders' });
    }
  }

  toastMessageTransports(data: any) {
    if (data?.length>0) {
      this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Transports retrieved with success' });
    } else if (data?.length===0) {
      this.messageService.add({ severity: 'info', summary: 'Service Message', detail: 'Oops no transport found' });
    } else {
      this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'Error while retrieving transports' });
    }
  }

  containsSpecialChars(str: string) {
    const specialChars=/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
  }

  public handleError(error: HttpErrorResponse) {
    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error);
      errorMessage=`An error occurred: ${error}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, `+
        `body was: ${JSON.stringify(error.message)}`);
      errorMessage=`Backend returned code ${error.status}, `+
        `body was: ${JSON.stringify(error.message)}`;
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new Error(
      'Something bad happened; please try again later.'+
      '\n'+
      errorMessage
    ))
  }
}
