import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupMapService {

  constructor() { }

  makeCapitalPopup(data: any): any {
    return `` +
    `<div>Address:&nbsp; <span  class="text-primary">${ data?.address }</span> </div>` +
    `<div>City: &nbsp;<span  class="text-primary">${ data?.city }</span> </div>` +
    `<div>Country Name: &nbsp; <span  class="text-primary">${ data?.countryName }</span></div>` +
    `<div>Country Code: &nbsp; <span  class="text-primary">${ data?.countryCode }</span></div>`+
    `<div>Type: &nbsp; <span  class="text-warning">${ data?.destinationType }</span> </div>`+
    `<p-tag [severity]="data.destinationType === 'LOAD' ? 'warning' : 'primary'"  value="{{destination.destinationType}}"></p-tag>`
   }
}
