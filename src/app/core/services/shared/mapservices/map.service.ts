import { Injectable } from '@angular/core'
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing'
import { ActivatedRoute } from '@angular/router'
import 'leaflet'

// Leaflet and Leaflet Routing Machine have been installed with npm
import 'leaflet-routing-machine'
import { CommercialService } from '../../commercial/commercial.service'
import { PopupMapService } from '../popupservices/popup-map.service'
declare var L: any

const iconRetinaUrl = 'assets/img/marker-icon-gold.png'
const iconUrl = 'assets/img/marker-icon1.png'
const shadowUrl = 'assets/img/marker-shadow.png'
const iconDefault = L.icon({
    iconRetinaUrl,
    iconUrl,
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
})

const firstPointIcon = L.icon({
    iconRetinaUrl,
    iconUrl : 'assets/img/marker-icon-2x-green.png',
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
})
const lastPointIcon = L.icon({
    iconRetinaUrl,
    iconUrl : 'assets/img/marker-icon-red.png',
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41],
})




@Injectable({
    providedIn: 'root',
})
export class MapService {
    public destinations!: any
    public dataTest!: any
    public orderId: string = this.route.snapshot.paramMap.get('id') || ''
    latlngs: any = []
    public marker: any
    public locations: any[] = []
    public locations2: any[] = []
    public firstlocalisation: any
    public secondlocalisation: any
    errorMessage: any

    constructor(
        private commercialService: CommercialService,
        private route: ActivatedRoute,
        private popupMapService: PopupMapService,
    ) {}

    getOrderIdFormMapComponent(Id: any) {
        this.orderId = Id
    }

    getOrderById(orderId: any) {
        return new Promise((resolve) => {
            this.commercialService.getOrderById(orderId).subscribe({
                next: (order) => {
                    this.destinations = order.route
                    resolve(this.destinations)
                },
                error: (error) => (this.errorMessage = error),
            })
        })
    }

    resolveAfter1Seconds(x: any) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(x)
            }, 100)
        })
    }

    async makeMarkersByOrderGeolocationId(map: L.Map): Promise<void> {
        await this.getOrderById(this.orderId)
        let firstDestination = this.destinations[0];
        let lastDestination = this.destinations.at(-1);
        let firstPoint = this.destinations[0].geolocation;
        let lastPoint = this.destinations.at(-1).geolocation;
        for (const c of this.destinations) {
            this.locations.push(c.geolocation);
            this.marker = L.marker(c.geolocation);
            this.marker.addTo(map);
            this.marker.bindPopup(this.popupMapService.makeCapitalPopup(c))
        }
        var routeControl = L.Routing.control({
            routeWhileDragging: true,
        }).addTo(map)
        routeControl.setWaypoints(this.locations)
        this.locations = []
        for (const c of this.destinations) {
            this.locations.push(c.geolocation);
            this.marker = L.marker(c.geolocation);
            this.marker.addTo(map);
            this.marker.bindPopup(this.popupMapService.makeCapitalPopup(c));
        }

        this.marker= L.marker([lastPoint[0],lastPoint[1]], {icon: lastPointIcon});
        this.marker.addTo(map);
        this.marker.bindPopup(this.popupMapService.makeCapitalPopup(lastDestination));
        this.marker= L.marker([firstPoint[0],firstPoint[1]], {icon: firstPointIcon});
        this.marker.addTo(map);
        this.marker.bindPopup(this.popupMapService.makeCapitalPopup(firstDestination));
        this.locations = []
    }
}
