import { Injectable } from '@angular/core';
import { User, UserManager } from 'oidc-client';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user!: User;
  private userManager: UserManager;
  public currentUser!: any;
  private _loginChangedSubject = new Subject<boolean>();
  public loginChanged = this._loginChangedSubject.asObservable();

  // TODO Renseigner les éléments exacts retournées par le backend
  constructor() { 
    this.userManager = new UserManager({
      authority: environment.microserviceCommercial,
    });
   }

   login() {
    return this.userManager.signinRedirect();
  }

  async completeAuthentication() {
    this.user = await this.userManager.signinRedirectCallback();
  }

  isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  // TODO Vérification effective de la récupération de l'utilisateur
  getUser() {
    this.userManager.getUser().then((user) => {
      this.currentUser = user;
    }).catch(function (error) {
      console.log(error);
    });
  }

  signout() {
    this.userManager.signoutRedirect();
  }
}
