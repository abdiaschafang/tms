import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ITransport } from '../../models/transport';
import { SharedService } from '../shared/shared.service';

@Injectable({
  providedIn: 'root'
})
export class TransportService {

  protected resourcePath={
    urlGetTransport: 'transport/search',
    urlDecommissionTransport: 'transport',
    urlSetTransportStatus: 'transport/set',
    urlGetTransportByCode: 'transport/byCode',
    urlGetOrderByOrderId: 'transport/getOrder',
    urlUpdateTransport: 'transport',
    urlGetnotInvoiceByCode : 'transport/notInvoicedByCode'
  }

  constructor(
    protected http: HttpClient,
    protected sharedService: SharedService) { }

  public getTransports(body: {}): Observable<ITransport[]> {
    return this.http.put<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlGetTransport}`, body).pipe(
      tap(transports => console.log('transports: ', transports)),
      catchError(this.sharedService.handleError)
    );
  }

  public getTransportByCode(code: string): Observable<ITransport[]> {
    return this.http.get<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlGetTransportByCode}/${code}`).pipe(
      tap(transport => console.log('transport: ', transport)),
      catchError(this.sharedService.handleError)
    );
  }

  public getOrderByOrderId(orderId: string): Observable<ITransport[]> {
    return this.http.get<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlGetOrderByOrderId}/${orderId}`).pipe(
      tap(transport => console.log('transport: ', transport)),
      catchError(this.sharedService.handleError)
    );
  }

  public setTransportStatus(transportId: string, status: string): Observable<ITransport[]> {
    return this.http.put<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlSetTransportStatus}/${transportId}/status/${status}`, {}).pipe(
      tap(transport => console.log('status changed: ', transport)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateTransport(transportId: string, transport: {}): Observable<ITransport[]> {
    return this.http.put<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlUpdateTransport}/${transportId}`, transport).pipe(
      tap(transport => console.log('transport updated: ', transport)),
      catchError(this.sharedService.handleError)
    );
  }

  public decommissionTransport(transportId: string): Observable<ITransport[]> {
    return this.http.delete<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlDecommissionTransport}/${transportId}`).pipe(
      tap(transport => console.log('transport: ', transport)),
      catchError(this.sharedService.handleError)
    );
  }


  // public getNotInvoiceByCode(body: any): Observable<ITransport[]> {
  //   return this.http.get<ITransport[]>(`${environment.microserviceExploitation+this.resourcePath.urlGetnotInvoiceByCode}`, body).pipe(
  //     tap(TransportInfo => console.log('TransportInfo: ', TransportInfo)),
  //     catchError(this.sharedService.handleError)
  //   );
  // }

  public getNotInvoiceByCode(code: string): Observable<ITransport[]> {
    return this.http.get<ITransport[]>(`${environment.microserviceExploitation}/${this.resourcePath.urlGetnotInvoiceByCode}/${code}`).pipe(
      tap(TransportInfo => console.log('TransportInfo: ', TransportInfo)),
      catchError(this.sharedService.handleError)
    );
  }
}
