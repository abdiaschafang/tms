import { Injectable } from '@angular/core';
import { IOrder } from '../../models/order';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, tap, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SharedService } from '../shared/shared.service';

@Injectable({
  providedIn: 'root'
})
export class CommercialService {

  constructor(
    protected http: HttpClient,
    protected sharedService: SharedService) { }

  public subscription: Subscription=new Subscription();
  public addOrderObject: any;

  protected ressourcePath={
    urlGetOrders: '/order/search',
    urlPathOrder: '/order',
    urlPathRoute: 'route',
    urlShareOrder: '/share',
    urlDuplicateOrder: '/duplicate',
    urlSearchOrder: '/order/search',
    urlPublishOrder: 'toggleActive',
    urlAssignOrder: '/transport'
  }

  public getOrders(body: any): Observable<IOrder[]> {
    return this.http.put<IOrder[]>(`${environment.microserviceCommercial+this.ressourcePath.urlGetOrders}`, body).pipe(
      tap(orders => console.log('orders: ', orders)),
      catchError(this.sharedService.handleError)
    );
  }

  public getOrderById(orderId: string): Observable<IOrder> {
    return this.http.get<IOrder>(`${environment.microserviceCommercial+this.ressourcePath.urlPathOrder}/${orderId}`).pipe(
      tap(order => console.log('order: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public getOrdersByParam(param: {}): Observable<IOrder[]> {
    return this.http.put<IOrder[]>(`${environment.microserviceCommercial+this.ressourcePath.urlSearchOrder}`, param).pipe(
      tap(orders => console.log('orders: ', orders)),
      catchError(this.sharedService.handleError)
    );
  }

  public deleteOrder(orderId: string): Observable<IOrder[]> {
    return this.http.delete<IOrder[]>(`${environment.microserviceCommercial+this.ressourcePath.urlPathOrder}/${orderId}`).pipe(
      tap(order => console.log('order deleted: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public shareOrder(orderId: string, users: any): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceCommercial}${this.ressourcePath.urlPathOrder}/${orderId}${this.ressourcePath.urlShareOrder}`, users).pipe(
      tap(order => console.log('order shared: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public assignOrder(transportDto: string): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceCommercial}${this.ressourcePath.urlPathOrder}`, transportDto).pipe(
      tap(order => console.log('assign order: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public duplicateOrder(orderId: any): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceCommercial}${this.ressourcePath.urlPathOrder}/${orderId}${this.ressourcePath.urlDuplicateOrder}`, {}).pipe(
      tap(order => console.log('order duplicate: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public toggleActive(orderId: any): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(`${environment.microserviceCommercial}${this.ressourcePath.urlPathOrder}/${orderId}/${this.ressourcePath.urlPublishOrder}`, {}).pipe(
      tap(order => console.log('status updated: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public createOrder(body: any): Observable<IOrder[]> {
    return this.http.post<IOrder[]>(environment.microserviceCommercial+this.ressourcePath.urlPathOrder, body).pipe(
      tap(order => console.log('order: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateOrder(orderObject: any, orderId: string): Observable<IOrder[]> {
    return this.http.put<IOrder[]>(`${environment.microserviceCommercial+this.ressourcePath.urlPathOrder}/${orderId}`, orderObject).pipe(
      tap(order => console.log('order updated: ', order)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateRoute(routeObject: any, orderId: string): Observable<IOrder[]> {
    return this.http.put<IOrder[]>(`${environment.microserviceCommercial+this.ressourcePath.urlPathOrder}/${orderId}/${this.ressourcePath.urlPathRoute}`, routeObject).pipe(
      tap(route => console.log('route updated: ', route)),
      catchError(this.sharedService.handleError)
    );
  }

  public updateOrderFromTransport(orderObject: any, orderId: string): Observable<IOrder[]> {
    return this.http.put<IOrder[]>(`${environment.microserviceCommercial+this.ressourcePath.urlPathOrder}/${orderId}`, orderObject).pipe(
      tap(order => console.log('order status changed: ', order)),
      catchError(this.sharedService.handleError)
    );
  }
}
