import { ITransportTracking } from './../../models/transport-tracking';
import { catchError, tap, Observable } from 'rxjs';
import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { SharedService } from './../shared/shared.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  protected resourcePath={
    urlGetTransportTracking: 'transportTracking/search',
    urlUploadCmr: 'transportTracking/cmr',
    urlUploadDoc: 'transportTracking/order',
    urlGetTransportTrackingDetail: 'transportTracking/details',
    urlValidTransport: 'transportTracking/valid',
    urlPutTransportOnDispute: 'transportTracking/litigation',
    urlDownloadCmr: 'transportTracking/cmr/download',
    urlUpdateUploadCmr: 'transportTracking/update/cmr',
    urlDownloadDoc: 'transportTracking/order/download',
    urlUpdateUploadDoc: 'transportTracking/update/order'
  }

  constructor(
    protected sharedService: SharedService,
    protected http: HttpClient) { }

  public getTransports(transportTrackingSearchDto: {}): Observable<ITransportTracking[]> {
    return this.http.put<ITransportTracking[]>(`${environment.microserviceTransport}/${this.resourcePath.urlGetTransportTracking}`, transportTrackingSearchDto).pipe(
      tap(transportsTracking => console.log('transportsTracking: ', transportsTracking)),
      catchError(this.sharedService.handleError)
    );
  }

  public uploadCmr(file: File, codeTransport: string): Observable<any> {
    let formParams=new FormData();
    formParams.append('file', file);
    return this.http.put<any>(`${environment.microserviceTransport}/${this.resourcePath.urlUploadCmr}?transport=${codeTransport}`, formParams).pipe(
      tap(file => console.log('file uploaded: ', file)));
  }

  public uploadDoc(file: File, codeTransport: string): Observable<any> {
    let formParams=new FormData();
    formParams.append('file', file);
    return this.http.put<any>(`${environment.microserviceTransport}/${this.resourcePath.urlUploadDoc}?transport=${codeTransport}`, formParams).pipe(
      tap(file => console.log('file uploaded: ', file)));
  }

  public updateUploadCmr(file: File, codeTransport: string) {
    let formParams=new FormData();
    formParams.append('file', file);
    return this.http.put<any>(`${environment.microserviceTransport}/${this.resourcePath.urlUpdateUploadCmr}?transport=${codeTransport}`, formParams).pipe(
      tap(file => console.log('file uploaded: ', file)));
  }

  public updateUploadDoc(file: File, codeTransport: string): Observable<any[]> {
    let formParams=new FormData();
    formParams.append('file', file);
    return this.http.put<any>(`${environment.microserviceTransport}/${this.resourcePath.urlUpdateUploadDoc}?transport=${codeTransport}`, formParams).pipe(
      tap(file => console.log('file uploaded: ', file)));
  }

  public getTransportTrackingDetail(transportTrackingId: string): Observable<ITransportTracking[]> {
    return this.http.get<ITransportTracking[]>(`${environment.microserviceTransport}/${this.resourcePath.urlGetTransportTrackingDetail}?transport=${transportTrackingId}`).pipe(
      tap(transportsTracking => console.log('transportsTrackingDetails: ', transportsTracking)),
      catchError(this.sharedService.handleError)
    );
  }

  public validTransports(transport: string): Observable<ITransportTracking[]> {
    return this.http.put<ITransportTracking[]>(`${environment.microserviceTransport}/${this.resourcePath.urlValidTransport}?transport=${transport}`, transport).pipe(
      tap(transportsTracking => console.log('transportsTracking: ', transportsTracking)),
      catchError(this.sharedService.handleError)
    );
  }

  public putTransportOnDispute(transport: string): Observable<ITransportTracking[]> {
    return this.http.put<ITransportTracking[]>(`${environment.microserviceTransport}/${this.resourcePath.urlPutTransportOnDispute}?transport=${transport}`, transport).pipe(
      tap(transportsTracking => console.log('transportsTracking: ', transportsTracking)),
      catchError(this.sharedService.handleError)
    );
  }

  public downloadCmr(param: string) {
    return this.http.get(`${environment.microserviceTransport}/${this.resourcePath.urlDownloadCmr}?transport=${param}`, { observe: 'response', responseType: 'blob' }).pipe(
      tap(file => console.log('cmr downloaded: ', file)),
      catchError(this.sharedService.handleError)
    );
  }

  public downloadDoc(param: string) {
    return this.http.get(`${environment.microserviceTransport}/${this.resourcePath.urlDownloadDoc}?transport=${param}`, { observe: 'response', responseType: 'blob' }).pipe(
      tap(file => console.log('file downloaded: ', file)),
      catchError(this.sharedService.handleError)
    );
  }

}
