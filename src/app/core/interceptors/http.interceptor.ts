import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, retry, throwError } from 'rxjs';

@Injectable()

export class HttpInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token='';
    const requestHeader=request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      },
      withCredentials: false,
    });

    return next.handle(requestHeader).pipe(
      retry(0),
      catchError((error: HttpErrorResponse) => {
        console.log(`HTTP Error: ${requestHeader.url}`);
        return throwError(error);
      })
    );
  }
}
