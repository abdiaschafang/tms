import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { SharedModule } from './modules/shared/shared.module';
import { AppService } from './core/services/app/app.service';
import { ConfirmationService, MessageService } from "primeng/api";

import { DatePipe } from '@angular/common';
import { FormatDatePipe } from 'src/app/core/pipes/format-date.pipe';
import { HttpInterceptorProviders } from './core/interceptors';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store'
import { reducer } from './states/reducers/order.reducer';
import { BillingListComponent } from './microservices/billing/billing-list/billing-list.component';

export function initApp(appService: AppService) {
  return () => appService.initApp();
}

@NgModule({
  declarations: [
    AppComponent,
    BillingListComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, FormsModule, AppRoutingModule,
    SharedModule,
    StoreModule.forRoot(reducer),
  ],
  providers: [
    HttpInterceptorProviders,
    // { provide: APP_INITIALIZER, useFactory: initApp, deps: [AppService], multi: true },
    ConfirmationService,
    MessageService,
    DatePipe,
    FormatDatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
