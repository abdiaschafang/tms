import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { mainContentAnimation } from './animations/animations';
import { AuthService } from './core/services/auth/auth.service';
import { SidebarService } from './core/services/sidebar/sidebar.service';
import { Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [mainContentAnimation(),]
})
export class AppComponent implements OnInit {
  title = 'tms-frontend';
  isLogged: any;
  sidebarState!: string;
  authUser=false;
  loading=false;
  public User!: any;

  constructor(private translate: TranslateService,
    private authService: AuthService,
    private sidebarService: SidebarService,
    private router: Router) {
     this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  // TODO Mettre une directive dans la navbar et la sidebar pour conditionner leur affichages dans le login component
  ngOnInit(): void {
    this.User = this.authService.getUser();
    // this.checkUser();
    this.sidebarService.sidebarStateObservable$
      .subscribe((newState: string) => {
        this.sidebarState = newState;
      });
  }

  isExpanding = false;
  toggleSideBar() {
    this.isExpanding = !this.isExpanding;
  }

  checkUser() {
    console.log('ici');
    console.log(this.User);

    if (!!this.User) {
      this.authUser = true;
    }
  }
}
