import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes=[
  { path: 'commercial', loadChildren: () => import('./microservices/commercial/commercial.module').then(m => m.CommercialModule) },
  { path: 'exploitation', loadChildren: () => import('./microservices/exploitation/exploitation.module').then(m => m.ExploitationModule) },
  { path: 'transport', loadChildren: () => import('./microservices/transport/transport.module').then(m => m.TransportModule) },
  { path: 'management', loadChildren: () => import('./microservices/management/management.module').then(m => m.ManagementModule) },
  { path: 'monitoring', loadChildren: () => import('./microservices/monitoring/monitoring.module').then(m => m.MonitoringModule) },
  { path: 'billing', loadChildren: () => import('./microservices/billing/billing.module').then(m => m.BillingModule) },
  { path: 'transporterBilling', loadChildren: () => import('./microservices/billing-transporter/transporter-billing.module').then(m => m.TransporterBillingModule) },
  { path: 'login', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
})
export class AppRoutingModule { }
