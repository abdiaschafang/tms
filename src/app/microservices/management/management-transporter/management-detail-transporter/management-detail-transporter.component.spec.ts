import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementDetailTransporterComponent } from './management-detail-transporter.component';

describe('ManagementDetailTransporterComponent', () => {
  let component: ManagementDetailTransporterComponent;
  let fixture: ComponentFixture<ManagementDetailTransporterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementDetailTransporterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementDetailTransporterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
