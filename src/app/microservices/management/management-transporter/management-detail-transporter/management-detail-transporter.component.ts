import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../management-data.service';
import { Subscription } from 'rxjs';
import { ManagementService } from 'src/app/core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-management-detail-transporter',
  templateUrl: './management-detail-transporter.component.html',
  styleUrls: ['./management-detail-transporter.component.scss']
})
export class ManagementDetailTransporterComponent implements OnInit, OnDestroy {
  public displayedSideModal=true;
  public bankingInfoForm!: FormGroup;
  public driverForm!: FormGroup;
  public truckForm!: FormGroup;
  transporter: any;
  trucks: any
  truckList: any;
  public truckListId: any[]=[];
  transporterTruckList: any;
  public spinner: boolean=false;
  subscription!: Subscription;
  public searchForm!: FormGroup;
  @Input() transporterForDetail: any;
  @Output() public actiontoPerformed: EventEmitter<boolean>=new EventEmitter<boolean>();
  bankingInfo: any;
  drivers: any;
  public formErrors: { [key: string]: string }={};
  private validationMessages: { [key: string]: { [key: string]: string } }={
    iban: { required: 'Ce champ est obligatoire' },
    bic: { required: 'Ce champ est obligatoire' },
    transporter: { required: 'Ce champ est obligatoire' },
    name: { required: 'Ce champ est obligatoire' },
    surname: { required: 'Ce champ est obligatoire' },
    phone: { required: 'Ce champ est obligatoire' },
    email: { required: 'Ce champ est obligatoire' },
  }
  errorMessage: any;
  transporterBankingInfoList: any;
  transporterDriverList: any;

  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription=this.data.currentsSideModal.subscribe(displayedSideModal => this.displayedSideModal=displayedSideModal);
    this.bankingInfoForm=this.formBuilder.group({
      iban: ['', Validators.required],
      bic: ['', Validators.required],
      status: ['', Validators.required],
      transporter: ['', Validators.required]
    })
    this.driverForm=this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      status: [''],
      transporter: ['', Validators.required],
    })
    this.truckForm=this.formBuilder.group({
      truck: ['', Validators.required],
    })
    this.searchForm=this.formBuilder.group({
      param: [''],
      status: ['']
    })

    this.getAllTruck()

    this.bankingInfo=this.transporterForDetail.bankingInfos;
    this.drivers=this.transporterForDetail.drivers;
    this.bankingInfoForm.patchValue({
      transporter: this.transporterForDetail.name
    })

    this.getTransporterByTva();
  }

  closeSideModal() {
    this.data.showOrHideSideModal(!this.displayedSideModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onRowEditInit(bankingInfo: any) {
    console.log(bankingInfo);
  }

  actionPerformed() {
    this.actiontoPerformed.emit(true);
  }


  onRowEditCancel(bankingInfo: any) {
    console.log(bankingInfo);
  }

  onRowEditSave(bankingInfo: any) {
    bankingInfo.transporter={};
    this.managementService.updateBankingInfo(bankingInfo, bankingInfo.id).subscribe({
      next: (data) => {
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Banking Info Updated' });
      },
      error: error => (
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: error.details })
      )
    })
  }

  deleteBankingInfo(bankingInfo: any) {
    this.managementService.deleteBankingInfo(bankingInfo.id).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Banking Info Deleted' });
      },
      error: error => (
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: error.details })
      )
    })
  }

  addBankingInfo() {
    this.bankingInfoForm.patchValue({
      status: 'ACTIVATED',
      transporter: this.transporterForDetail
    })
    this.managementService.createBankingInfo(this.bankingInfoForm.value).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Banking Info added' });
        this.getTransporterByTva();
      },
      error: error => (
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: error.details })
      )
    })
  }

  addDriver() {
    this.driverForm.patchValue({
      status: 'ACTIVATED',
      transporter: this.transporterForDetail
    })
    this.managementService.createDriver(this.driverForm.value).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Driver added' });
        this.getTransporterByTva();
      },
      error: error => (
        this.messageService.add({ severity: 'warning', summary: 'Service Message', detail: error.details })

      )
    })
  }


  deleteDriver(driverSelected: any) {
    this.managementService.deleteDriver(driverSelected.id).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Driver Deleted' });
        this.getTransporterByTva();
      },
      error: error => (
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: error.details })
      )
    })
  }

  deleteTransporterTruck(truckSelected: any) {
    this.confirmationService.confirm({
      message: "Do you want to delete Truck &nbsp; :"+truckSelected.name,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deleteTransporterTruck(this.transporterForDetail.id, truckSelected.id).subscribe({
          next: () => {
            this.getTransporterByTva();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Truck Removed On Your Trucks List' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  updateTransporterDriver(driverSelected: any) {
    driverSelected.transporter={};
    this.managementService.updateDriver(driverSelected, driverSelected.id).subscribe({
      next: (data) => {
        this.getTransporterByTva();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'driver Updated' });
      },
      error: error => (
        this.messageService.add({ severity: 'warning', summary: 'Service Message', detail: error.details })
      )
    })
  }

  getAllTruck() {
    this.spinner=true;
    let searchParam={
      param: '',
      status: 'ACTIVATED'
    }
    this.managementService.getAllTruck(searchParam).subscribe({
      next: trucks => {
        this.trucks=trucks;
        this.truckList=this.trucks.trucks;
        console.log('this.truckList');
        console.log(this.truckList);

        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  addTruckOnTransporter() {
    let trucks=this.truckForm.get('truck')?.value;
    for (let t of trucks) {
      this.truckListId!.push(t.id);
    }
    this.managementService.AddTruckOnTransporter(this.truckListId, this.transporterForDetail.id).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Truck Added On Transporter' });
        this.getTransporterByTva();
      },
      error: error => this.errorMessage=error
    })
  }

  getTransporterByTva() {
    this.searchForm.patchValue({
      status: this.transporterForDetail.status,
      param: this.transporterForDetail.tva,
    })
    this.spinner=true;
    this.managementService.getAllTransporter(this.searchForm.value).subscribe({
      next: transporter => {
        this.transporter=transporter;
        this.spinner=false;
        this.transporterTruckList=this.transporter.transporters[0].trucks;
        this.transporterBankingInfoList=this.transporter.transporters[0].bankingInfos;
        this.transporterDriverList=this.transporter.transporters[0].drivers;
      },
      error: error => this.errorMessage=error
    })
  }

}
