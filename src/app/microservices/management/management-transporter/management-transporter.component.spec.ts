import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementTransporterComponent } from './management-transporter.component';

describe('ManagementTransporterComponent', () => {
  let component: ManagementTransporterComponent;
  let fixture: ComponentFixture<ManagementTransporterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementTransporterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementTransporterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
