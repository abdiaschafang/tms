import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { MatPaginator } from '@angular/material/paginator';
import { Transporter } from '../../../core/models/management/transporter';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as FileSaver from 'file-saver';
import { SharedService } from 'src/app/core/services/shared/shared.service';

@Component({
  selector: 'app-management-transporter',
  templateUrl: './management-transporter.component.html',
  styleUrls: ['./management-transporter.component.scss']
})
export class ManagementTransporterComponent implements OnInit, OnDestroy {
  sales!: any[];
  public ObjecFromTransporterParentForUpdate: any;
  public transporterDetail: any;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public transporterDataForPagination!: Observable<any>;
  //  @Input() transporterForDetail : any;
  public displayModal!: boolean;
  public displayedSideModal!: boolean;
  public subscription: Subscription=new Subscription();
  products!: any[];
  selectedProducts!: [];
  TransportersList: any;
  Transporter: any;
  errorMessage: any;
  public searchForm!: FormGroup;
  public spinner: boolean=false;
  public allStatus!: any[];
  //propriety for export
   cols!: any[];
  exportColumns!: any[];
    clicked: any=true;
  clicked1: any=true;
  clicked2: any=true;
  clicked3: any=true;
  clicked4: any=true;
  clicked5: any=true;
  clicked6: any=true;
  clicked7: any=true;
  compteur: any=0;
  storedPayload: any;
  name: any;
  tva: any;
  city: any;
  phone: any;
  email: any;
  loadCountryCode: any;
  loadZipCode: any;
  statusE: any;
  zipCodes: { name: string; }[];
  selectedCountryCode: any;
  countries!: any[];
  dataCities!: any;
  cities: any;

  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder) {
       this.zipCodes=[ { name: 'Select' },{ name: 'FR4533' }, { name: 'DF5225' }, { name: 'RF5225' }, { name: 'DFR2266' }];
     }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal=displayModal));
    this.subscription.add(this.data.currentsSideModal.subscribe(displayedSideModal => this.displayedSideModal=displayedSideModal));

    this.searchForm=this.formBuilder.group({
      name: [''],
      tva: [''],
      city: ['', { disabled: true }],
      phone: [''],
      email: [''],
      loadCountryCode: [''],
      loadZipCode: [''],
      status: ['']
    })

    this.allStatus=[
      { id: "ACTIVATED", name: "ACTIVATED" },
      { id: "BLOCKED", name: "BLOCKED" },
    ]
    this.getAllTransporter();

    this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }

  showModal() {
    this.data.changeMessage(this.displayModal);
    this.ObjecFromTransporterParentForUpdate=null;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  editTransporter(TransportersList: any) {
    this.ObjecFromTransporterParentForUpdate=TransportersList;
    this.data.changeMessage(this.displayModal);
  }

  detailTransporter(TransportersList: any) {
    this.transporterDetail=TransportersList;
    this.data.showOrHideSideModal(this.displayedSideModal);
  }


  getAllTransporter() {

    this.name=this.searchForm.get('name')?.value||'';
    this.tva=this.searchForm.get('tva')?.value||'';
    this.city=this.searchForm.get('city')?.value.name||'';
    this.email=this.searchForm.get('email')?.value||'';
    this.phone=this.searchForm.get('phone')?.value||'';
    this.statusE=this.searchForm.get('status')?.value||'';
    this.loadCountryCode=this.searchForm.get('loadCountryCode')?.value||'';
    this.loadZipCode=this.searchForm.get('loadZipCode')?.value.name||'';
    let payload={
      name: this.name,
      tva: this.tva,
      city: this.city,
      email: this.email,
      phone: this.phone,
      status: this.statusE,
      loadCountryCode: this.loadCountryCode,
      loadZipCode:this.loadZipCode,
    }
    this.storedPayload=payload;
    console.log(this.storedPayload);
    if (this.storedPayload.status==='') {
      this.storedPayload.status='ACTIVATED';
    }
    // if (this.searchForm.get('status')?.value===null) {
    //   this.searchForm.patchValue({
    //     status: ''
    //   })
    // }
    this.spinner=true;
    this.managementService.getAllTransporter(payload).subscribe({
      next: transporter => {
        this.Transporter=transporter;
        this.spinner=false;
        this.TransportersList=this.Transporter.transporters;
        const dataSource: MatTableDataSource<Transporter>=new MatTableDataSource<Transporter>(this.TransportersList);
        dataSource.paginator=this.paginator;
        this.transporterDataForPagination=dataSource.connect();
      },
      error: error => this.errorMessage=error
    })
  }

  deleteTransporter(transporter: any) {
    console.log(transporter)
    this.confirmationService.confirm({
      message: "Do you want to delete this Transporter"+transporter.name,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deleteTransporter(transporter.id).subscribe({
          next: () => {
            this.getAllTransporter();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'The transporter has been deleted ' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  SearchTransporter() {
  }
  //load cities an country

   public getCountriesByEvent(param: any) {
    this.selectedCountryCode=param.code;
    this.sharedService.getCountries().subscribe({
      next: countries => {
        this.countries=countries;
      },
      error: error => this.errorMessage=error
    })
  }

   public getCountries() {
    this.spinner=true;
    this.sharedService.getCountries().subscribe({
      next: countries => {
        this.countries=countries;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

    public getCities(param: any) {
    this.sharedService.getCities(this.selectedCountryCode, param.query).subscribe({
      next: cities => {
        this.dataCities=cities;
        this.cities=this.dataCities.cities;
      },
      error: error => this.errorMessage=error
    })
  }



  setSearch(input: any) {
    console.log(input);
    switch (input) {
      case '1': {
        this.clicked=false;
        this.storedPayload.name='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }

      case '2': {
        this.clicked1=false;
        this.storedPayload.tva='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '3': {
        this.clicked2=false;
        this.storedPayload.city='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '4': {
        this.clicked3=false;
          this.storedPayload.phone='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '5': {
        this.clicked4=false;
        this.storedPayload.email='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '6': {
        this.clicked5=false;
        this.storedPayload.loadCountryCode='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }

      case '7': {
        this.clicked6=false;
        this.storedPayload.status='ACTIVITED';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '8': {
        this.clicked7=false;
        this.storedPayload.loadZipCode='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      default: {
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
    }
    console.log(this.storedPayload,this.clicked,this.clicked1,this.clicked,this.clicked);
  }

  sendSearchInBackend(formValue:any) {
         this.managementService.getAllTransporter(formValue).subscribe({
      next: transporter => {
        this.Transporter=transporter;
        this.spinner=false;
        this.TransportersList=this.Transporter.transporters;
        const dataSource: MatTableDataSource<Transporter>=new MatTableDataSource<Transporter>(this.TransportersList);
        dataSource.paginator=this.paginator;
        this.transporterDataForPagination=dataSource.connect();
      },
      error: error => this.errorMessage=error
    })
  }


    // fonction of export file

    exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.TransportersList);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "TransportersList");
    });
  }
    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

     initialize() {

      this.compteur++;
    if (this.compteur>1) {
    // this.storedPayload.name='';
    // this.storedPayload.tva='';
    // this.storedPayload.city='';
    // this.storedPayload.email='';
    // this.storedPayload.phone='';
    // this.storedPayload.loadCountryCode='';
    // this.storedPayload.status='ACTIVATED';
    // this.storedPayload.loadZipCode='';

      this.name='';
    this.tva='';
    this.city='';
    this.email='';
    this.phone='';
    this.statusE='';
    this.loadCountryCode='';
    this.loadZipCode='';

      this.clicked=true;
      this.clicked1=true;
      this.clicked2=true;
      this.clicked3=true;
      this.clicked4=true;
      this.clicked5=true;
      this.clicked6=true;
      this.clicked7=true;
     }
  }

}
