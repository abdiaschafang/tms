import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private messageSource = new BehaviorSubject(false);
  currentMessage = this.messageSource.asObservable();

  private showOrHiSidedeModal = new BehaviorSubject(false);
  currentsSideModal = this.showOrHiSidedeModal.asObservable();

  constructor() { }

  changeMessage(displayModal: boolean) {
    this.messageSource.next(!displayModal)
  }

  showOrHideSideModal(displayedSideModal: boolean) {
    this.showOrHiSidedeModal.next(!displayedSideModal); 
  }
}
