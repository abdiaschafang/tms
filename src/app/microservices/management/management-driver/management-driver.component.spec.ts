import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementDriverComponent } from './management-driver.component';

describe('ManagementDriverComponent', () => {
  let component: ManagementDriverComponent;
  let fixture: ComponentFixture<ManagementDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementDriverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
