import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-management-driver',
  templateUrl: './management-driver.component.html',
  styleUrls: ['./management-driver.component.scss']
})
export class ManagementDriverComponent implements OnInit, OnDestroy {
  public displayModal!: boolean;
  public subscription: Subscription=new Subscription();
  goods: any
  selectedGood: any;
  goodsList: any;
  errorMessage: any;
  selectedDriver!: [];
  searchForm!: FormGroup
  public driversData!: Observable<any>;

  paginator: any;


  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formbuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal=displayModal));
    this.searchForm=this.formbuilder.group({
      status: [''],
      param: ['']
    })

    this.getAllGoods();
  }

  showModal() {
    this.selectedGood="";
    this.data.changeMessage(this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllGoods() {
    this.searchForm.patchValue({
      status: 'ACTIVATED'
    })
    this.managementService.getAllGoods(this.searchForm.value).subscribe({
      next: goods => {
        this.goods=goods;
        this.goodsList=this.goods.goods;
      },
      error: error => this.errorMessage=error
    })
  }

  editGoods(selectedGood: any) {
    this.selectedGood=selectedGood
    this.data.changeMessage(this.displayModal);
  }




  deleteGoods(selectedGood: any) {
    this.confirmationService.confirm({
      message: "Do you want to delete this Goods &nbsp;: &nbsp;"+selectedGood.name,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deleteGoods(selectedGood.id).subscribe({
          next: () => {
            this.getAllGoods();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Goods has been deleted ' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }


}
