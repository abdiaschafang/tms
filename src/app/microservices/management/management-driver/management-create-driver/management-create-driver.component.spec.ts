import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreateDriverComponent } from './management-create-driver.component';

describe('ManagementCreateDriverComponent', () => {
  let component: ManagementCreateDriverComponent;
  let fixture: ComponentFixture<ManagementCreateDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreateDriverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreateDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
