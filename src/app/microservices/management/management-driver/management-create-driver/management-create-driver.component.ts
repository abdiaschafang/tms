import { Component, OnInit, OnDestroy, Output, EventEmitter, Input, ElementRef, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { DataService } from '../../management-data.service';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { GlobalGenericValidator } from '../../../../core/validators/global-generic.validator';


@Component({
  selector: 'app-management-create-driver',
  templateUrl: './management-create-driver.component.html',
  styleUrls: ['./management-create-driver.component.scss']
})
export class ManagementCreateDriverComponent implements OnInit, OnDestroy {
  @Input() public editDriversData: any;
  @Output() public actiontoPerformed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public Transporter: any;
  TransportersList: any;
  public displayModal = true;
  subscription!: Subscription;
  public driversForm!: FormGroup;
  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    name: { required: 'Ce champ est obligatoire' },
    description: { required: 'Ce champ est obligatoire' },
  };

  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];

  constructor(private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService: MessageService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.driversForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      status: [''],
    })
    this.setdriversForm();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.driversForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.driversForm, this.isFormSubmitted);
      });
  }

  setdriversForm() {
    if (this.editDriversData != null || this.editDriversData != undefined) {
      this.driversForm.patchValue({
        name: this.editDriversData.name,
        description: this.editDriversData.description,
      })
    }
  }

  closeModal() {
    this.data.changeMessage(!this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  actionPerformed() {
    this.actiontoPerformed.emit(true);
  }

  addDrivers() {
    this.isFormSubmitted = true;
    this.driversForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    this.driversForm.patchValue({
      status: 'ACTIVATED'
    })
    if (this.driversForm.valid) {
      this.managementService.createDriver(this.driversForm.value,).subscribe({
        next: (data) => {
          this.actionPerformed();
          this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Drivers added' });
        },
        error: error => (
          this.messageService.add({ severity: 'warning', summary: 'Service Message', detail: error.details })

        )
      })
    }
  }

  updateDrivers() {
    this.driversForm.patchValue({
      status: this.editDriversData.status,
    })
    this.managementService.updateDriver(this.driversForm.value, this.editDriversData.id).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Driver updated' });
      },
      error: error => (
        this.messageService.add({ severity: 'warning', summary: 'Service Message', detail: error.details })

      )
    })
  }

}
