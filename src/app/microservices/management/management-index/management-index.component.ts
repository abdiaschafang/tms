import { Component, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatCardHarness} from '@angular/material/card/testing';
import { Router } from '@angular/router';
import { NotFoundError } from 'rxjs';
import { ManagementCustomerComponent } from '../management-customer/management-customer.component';

@Component({
  selector: 'app-management-index',
  templateUrl: './management-index.component.html',
  styleUrls: ['./management-index.component.scss']
})
export class ManagementIndexComponent implements OnInit {

  constructor(private  router: Router) { }

  ngOnInit(): void {
  }


  openCustomer(){
    this.router.navigateByUrl('/management/customer');
  }

  openTransporter(){
    this.router.navigateByUrl('/management/transporter');
  }

  openTruck(){
this.router.navigateByUrl('/management/truck');
  }

  openDriver(){
    this.router.navigateByUrl('/management/driver');
      }

  openGoods(){
    this.router.navigateByUrl('/management/goods');
     }

  openPellets(){
    this.router.navigateByUrl('/management/pallets');
    }

  openConfigurations(){
    this.router.navigateByUrl('/management/configurations');
  }

  openBankingInfo(){
    this.router.navigateByUrl('/management/bankingInformation')
  } 

  openPaymentMethod(){
    this.router.navigateByUrl('/management/paymentMethod')
  } 
  

}
