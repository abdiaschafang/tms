import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-management-pallets',
  templateUrl: './management-pallets.component.html',
  styleUrls: ['./management-pallets.component.scss']
})
export class ManagementPalletsComponent implements OnInit {
  public displayModal!: boolean;
  public subscription: Subscription=new Subscription();
  pallets: any
  selectedpallet: any;
  palletsList: any;
  errorMessage: any;
  selectedpallets!: [];
  searchForm!: FormGroup
  public spinner: boolean=false;
  public allStatus!: any[];
    //propriety for export
   cols!: any[];
    exportColumns!: any[];



  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formbuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal=displayModal));
    this.searchForm=this.formbuilder.group({
      status: [''],
      param: ['']
    })

    this.allStatus=[
      { id: "ACTIVATED", name: "ACTIVER" },
      { id: "BLOCKED", name: "BLOQUER" },
    ]

    this.getAllPallets();

         this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }


  showModal() {
    this.selectedpallet="";
    this.data.changeMessage(this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllPallets() {
    this.searchForm.patchValue({
      status: 'ACTIVATED'
    })
    this.managementService.getAllPallets(this.searchForm.value).subscribe({
      next: pallets => {
        this.pallets=pallets;
        this.palletsList=this.pallets.pallets;
      },
      error: error => this.errorMessage=error
    })
  }

  searchPallets() {
    this.spinner=true;
    if (this.searchForm.get('status')?.value===null) {
      this.searchForm.patchValue({
        status: ''
      })
    }
    this.managementService.getAllPallets(this.searchForm.value).subscribe({
      next: pallets => {
        this.pallets=pallets;
        this.palletsList=this.pallets.pallets;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  editPallets(selectedpallet: any) {
    this.selectedpallet=selectedpallet
    this.data.changeMessage(this.displayModal);
  }




  deleteGoods(selectedpallet: any) {
    this.confirmationService.confirm({
      message: "Do you want to delete this pallets &nbsp;: &nbsp;"+selectedpallet.type,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deletePallets(selectedpallet.id).subscribe({
          next: () => {
            this.getAllPallets();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'palets has been deleted ' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  // fonction of export file

    exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.palletsList);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "palletsList");
    });
  }
    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

}
