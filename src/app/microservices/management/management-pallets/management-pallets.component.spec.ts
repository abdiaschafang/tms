import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementPalletsComponent } from './management-pallets.component';

describe('ManagementPalletsComponent', () => {
  let component: ManagementPalletsComponent;
  let fixture: ComponentFixture<ManagementPalletsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementPalletsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementPalletsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
