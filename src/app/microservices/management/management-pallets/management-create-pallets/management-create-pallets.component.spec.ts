import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreatePalletsComponent } from './management-create-pallets.component';

describe('ManagementCreatePalletsComponent', () => {
  let component: ManagementCreatePalletsComponent;
  let fixture: ComponentFixture<ManagementCreatePalletsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreatePalletsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreatePalletsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
