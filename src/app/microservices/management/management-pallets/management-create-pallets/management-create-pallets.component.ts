import { Component, OnInit, OnDestroy, Output,EventEmitter, Input,ViewChildren,ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { DataService } from '../../management-data.service';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';



@Component({
  selector: 'app-management-create-pallets',
  templateUrl: './management-create-pallets.component.html',
  styleUrls: ['./management-create-pallets.component.scss']
})

export class ManagementCreatePalletsComponent implements OnInit {
  @Input() public editPalletsData: any;
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
public displayModal= true;
subscription!: Subscription;
public palletsForm!: FormGroup;
public errorMessage!: string;
public formErrors: { [key: string]: string } = {};
private validationMessages: { [key: string]: { [key: string]: string } } = {
type: { required: 'Ce champ est obligatoire' },
length: { required: 'Ce champ est obligatoire' },
width: { required: 'Ce champ est obligatoire' },
description: { required: 'Ce champ est obligatoire' },
};
private globalGenericValidator!: GlobalGenericValidator;
private isFormSubmitted!: boolean;
@ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];

constructor( private data: DataService,
  private formBuilder: FormBuilder,
  private managementService: ManagementService,
  private messageService:MessageService,
) { } 

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.palletsForm=this.formBuilder.group({
      type : ['',Validators.required], 
      length : ['',Validators.required], 
      width : ['',Validators.required], 
      description : ['',Validators.required], 
      status: [''],
    })
    this.setPalletsForm()
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.palletsForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.palletsForm, this.isFormSubmitted);
      });
  }

// préremplire le formulaire pour modifier
setPalletsForm(){
  if(this.editPalletsData != null || this.editPalletsData != undefined){
    this.palletsForm.patchValue({
      type : this.editPalletsData?.type,
      length : this.editPalletsData?.length,
      width : this.editPalletsData?.width,   
      description : this.editPalletsData?.description,   
    })
  }
}

closeModal(){
  this.data.changeMessage(!this.displayModal);
}

ngOnDestroy() {
  this.subscription.unsubscribe();
}

  actionPerformed(){
this.actiontoPerformed.emit(true);
 }

 createPallets(){
  this.palletsForm.patchValue({
    status : 'ACTIVATED'
    })
    
  this.isFormSubmitted = true;
  this.palletsForm.updateValueAndValidity({
    onlySelf: true,
    emitEvent: true
  });

   if (this.palletsForm.valid){
    this.managementService.createPallets(this.palletsForm.value, ).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({severity:'success', summary:'Service Message', detail:'Pallets added'});
      },
      error: error => (
        this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})
      )
    })
   }

  }
  
  updatePallets(){

    this.isFormSubmitted = true;
  this.palletsForm.updateValueAndValidity({
    onlySelf: true,
    emitEvent: true
  });

    this.palletsForm.patchValue({
      status : this.editPalletsData.status,
      })

      if(this.palletsForm.valid){
        this.managementService.updatePallets(this.palletsForm.value, this.editPalletsData.id).subscribe({
          next: (data) => {
            this.actionPerformed();
            this.messageService.add({severity:'success', summary:'Service Message', detail:'Pallets updated'});
          },
          error: error => (
            this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})
          )
        })
      }

   
  }


}

 