import { Component, OnInit, OnDestroy, Input, Output,EventEmitter, ViewChildren, ElementRef } from '@angular/core';
import { DataService } from "../management-data.service";
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControlName  } from '@angular/forms';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';


@Component({
  selector: 'app-management-create-customer',
  templateUrl: './management-create-customer.component.html',
  styleUrls: ['./management-create-customer.component.scss']
})
export class ManagementCreateCompanyComponent implements OnInit, OnDestroy{
  @Input() ObjecFromCustomerParentForUpdate : any;
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal= true;
  subscription!: Subscription;
  public customerForm!: FormGroup;
  public transporterForm!: FormGroup;
  public customerToUpdate : any

  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    name: { required: 'Ce champ est obligatoire' },
    tva: { required: 'Ce champ est obligatoire' },
    siren: { required: 'Ce champ est obligatoire' },
    siret: { required: 'Ce champ est obligatoire' },
    countryCode: { required: 'Ce champ est obligatoire' },
    city: { required: 'Ce champ est obligatoire' },
    address: { required: 'Ce champ est obligatoire' },
    zipcode: { required: 'Ce champ est obligatoire' },
    valueTva: { required: 'Ce champ est obligatoire' },
    fax: { required: 'Ce champ est obligatoire' },
    phone: { required: 'Ce champ est obligatoire' },
    email: { required: 'Ce champ est obligatoire' },
    paymentsDaysNumber: { required: 'Ce champ est obligatoire' },
    paymentsDaysPeriod: { required: 'Ce champ est obligatoire' },
    outstanding: { required: 'Ce champ est obligatoire' },
  }
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];


  constructor(private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService:MessageService) { }

  ngOnInit(): void {
    this.customerToUpdate= this.ObjecFromCustomerParentForUpdate;
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.customerForm = this.formBuilder.group({
      name : ['',Validators.required],
      tva : ['',Validators.required],
      siren : ['',Validators.required],
      siret : ['',Validators.required],
      countryCode : ['',Validators.required],
      city : ['',Validators.required],
      address : ['',Validators.required],
      zipcode : ['',Validators.required],
      phone : ['',Validators.required],
      fax : ['',Validators.required],
      email : ['',Validators.required],
      valueTva : ['',Validators.required],
      paymentsDaysNumber : ['',Validators.required],
      paymentsDaysPeriod : ['',Validators.required],
      outstanding : ['',Validators.required],
      comment : ['',''],
    })
    this.setFormToUpdate();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.customerForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.customerForm, this.isFormSubmitted);
      });
  }

 closeModal(){
    this.data.changeMessage(!this.displayModal);
  }

  CancelModal(){
       this.data.changeMessage(!this.displayModal)
     }

  ngOnDestroy() {
    this.data.changeMessage(!this.displayModal)
    this.subscription.unsubscribe();
  }

  actionPerformed(){
    this.actiontoPerformed.emit(true);
  }


  createCustomer(){
    this.isFormSubmitted = true;
    this.customerForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });
    this.customerForm.patchValue({
      payments : [],
    })

    if(this.customerForm.valid){
      this.managementService.createCustomer(this.customerForm.value).subscribe({
        next: (data) => {
          console.log(data);
          this.actionPerformed();
          this.messageService.add({severity:'success', summary:'Service Message', detail:'Customer added'});
        },
        error: error => this.errorMessage = error
      })
    }
 
  }

  updateCustomer(){
    this.isFormSubmitted = true;
    this.customerForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });
    if(this.customerForm.valid){
      this.managementService.updateCustomer(this.customerForm.value, this.customerToUpdate.id).subscribe({
        next: (data) => {
          this.actionPerformed();
          this.messageService.add({severity:'success', summary:'Service Message', detail:'Customer Updated'});
        },
        error: error => this.errorMessage = error
      })
    }
  }

  setFormToUpdate(){
    if((this.customerToUpdate != null) || (this.customerToUpdate != undefined)){
     this.customerForm.patchValue({
      name : this.customerToUpdate.name,
      tva  : this.customerToUpdate.tva,
      siren: this.customerToUpdate.siren,
      siret: this.customerToUpdate.siret ,
      countryCode : this.customerToUpdate.countryCode ,
      city : this.customerToUpdate.city ,
      address : this.customerToUpdate.address ,
      zipcode : this.customerToUpdate.zipcode ,
      phone : this.customerToUpdate.phone,
      fax : this.customerToUpdate.fax ,
      email : this.customerToUpdate.email,
      valueTva : this.customerToUpdate.valueTva,
      paymentsDaysNumber : this.customerToUpdate.paymentsDaysNumber,
      paymentsDaysPeriod : this.customerToUpdate.paymentsDaysPeriod ,
      outstanding : this.customerToUpdate.outstanding,
      comment :  this.customerToUpdate.comment,
     })
    }
  }

}
