import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreateCompanyComponent } from './management-create-customer.component';

describe('ManagementCreateCompanyComponent', () => {
  let component: ManagementCreateCompanyComponent;
  let fixture: ComponentFixture<ManagementCreateCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreateCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreateCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
