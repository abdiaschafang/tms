import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreateTransporterComponent } from './management-create-transporter.component';

describe('ManagementCreateTransporterComponent', () => {
  let component: ManagementCreateTransporterComponent;
  let fixture: ComponentFixture<ManagementCreateTransporterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreateTransporterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreateTransporterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
