import { Component, OnInit, OnDestroy,Output, Input,EventEmitter,ViewChildren,ElementRef } from '@angular/core';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControlName} from '@angular/forms';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';


@Component({
  selector: 'app-management-create-transporter',
  templateUrl: './management-create-transporter.component.html',
  styleUrls: ['./management-create-transporter.component.scss']
}) 
export class ManagementCreateTransporterComponent implements OnInit ,OnDestroy {
  @Input() ObjecFromTransporterParentForUpdate : any; 
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal= true;
  subscription!: Subscription;
  public customerForm!: FormGroup;
  public transporterForm!: FormGroup;
  public bankingInfoForm!: FormGroup;

  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    name: { required: 'Ce champ est obligatoire' },
    tva: { required: 'Ce champ est obligatoire' },
    siren: { required: 'Ce champ est obligatoire' },
    siret: { required: 'Ce champ est obligatoire' },
    countryCode: { required: 'Ce champ est obligatoire' },
    city: { required: 'Ce champ est obligatoire' },
    address: { required: 'Ce champ est obligatoire' },
    zipcode: { required: 'Ce champ est obligatoire' },
    phone: { required: 'Ce champ est obligatoire' },
    email: { required: 'Ce champ est obligatoire' },
    licenceUrl: { required: 'Ce champ est obligatoire' },
    licence: { required: 'Ce champ est obligatoire' },
    assuranceUrl: { required: 'Ce champ est obligatoire' },
    assuarance: { required: 'Ce champ est obligatoire' },
    licenceDate: { required: 'Ce champ est obligatoire' },
    assuranceDate: { required: 'Ce champ est obligatoire' },
    paymentPeriod: { required: 'Ce champ est obligatoire' },
    outstanding: { required: 'Ce champ est obligatoire' },
  }
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];



  constructor(private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService:MessageService) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.transporterForm = this.formBuilder.group({
      name : ['',Validators.required], 
      id : [''],
      tva : ['',Validators.required],
      siren : ['',Validators.required],
      siret : ['',Validators.required],
      countryCode : ['',Validators.required],
      city : ['',Validators.required],
      address : ['',Validators.required],
      zipcode : ['',Validators.required],
      phone : ['',Validators.required],
      fax : ['',''],
      email : ['',Validators.required],
      licenceUrl : ['',Validators.required],
      licenceDate : ['',Validators.required],
      assuranceDate : ['',Validators.required],
      assuranceUrl : ['',Validators.required],
      paymentPeriod : ['',Validators.required],
      bankingInfos : ['',''],
      drivers : ['',''],
      trucks : ['',''],
      comment : ['',''],
    })
    this.bankingInfoForm = this.formBuilder.group({
      iban : ['',Validators.required],
      bic : ['',Validators.required],
      status : ['',Validators.required],
      transporter: ['',Validators.required]
    })
    this.setTransporterForm();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.transporterForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.transporterForm, this.isFormSubmitted);
      });
  }

  closeModal(){
    this.data.changeMessage(!this.displayModal);
    this.ObjecFromTransporterParentForUpdate ="";
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  actionPerformed(){
    this.actiontoPerformed.emit(true);
  }

    addTransporter(){
      this.isFormSubmitted = true;
      this.transporterForm.updateValueAndValidity({
        onlySelf: true,
        emitEvent: true
      });
      
      this.transporterForm.patchValue({
        bankingInfos : [],
        trucks : [],
        drivers : [],
      })
      if(this.transporterForm.valid){
        this.managementService.createTransporter(this.transporterForm.value).subscribe({
          next: (data) => {
            console.log(data);
            this.actionPerformed();
            this.messageService.add({severity:'success', summary:'Service Message', detail:'Transporter added'});
          },
          error: error => (
            this.messageService.add({severity:'error', summary:'Service Message', detail:error.details})
  
          )
        })
      }
   
    }

    // préremplire le formulaire pour modifier un transporteur
    setTransporterForm(){
      if(this.ObjecFromTransporterParentForUpdate != null || this.ObjecFromTransporterParentForUpdate != undefined){
        this.transporterForm.patchValue({
          id : this.ObjecFromTransporterParentForUpdate.id,
          name : this.ObjecFromTransporterParentForUpdate.name,
          tva : this.ObjecFromTransporterParentForUpdate.tva,
          siren : this.ObjecFromTransporterParentForUpdate.siren ,
          siret : this.ObjecFromTransporterParentForUpdate.siret ,
          countryCode : this.ObjecFromTransporterParentForUpdate.countryCode,
          city : this.ObjecFromTransporterParentForUpdate.city ,
          address : this.ObjecFromTransporterParentForUpdate.address,
          zipcode : this.ObjecFromTransporterParentForUpdate.zipcode,
          phone : this.ObjecFromTransporterParentForUpdate.phone ,
          fax : this.ObjecFromTransporterParentForUpdate.fax,
          email : this.ObjecFromTransporterParentForUpdate.email ,
          licenceUrl : this.ObjecFromTransporterParentForUpdate.licenceUrl ,
          licenceDate : this.ObjecFromTransporterParentForUpdate.licenceDate,
          assuranceDate : this.ObjecFromTransporterParentForUpdate.assuranceDate,
          assuranceUrl : this.ObjecFromTransporterParentForUpdate.assuranceUrl,
          paymentPeriod : this.ObjecFromTransporterParentForUpdate.paymentPeriod ,
          bankingInfos : this.ObjecFromTransporterParentForUpdate.bankingInfos ,
          drivers : this.ObjecFromTransporterParentForUpdate.drivers ,
          trucks : this.ObjecFromTransporterParentForUpdate.trucks ,
        })
      }
    }


    
  //fonction qui modifie un Transporter
  updateTransporter(){
    this.isFormSubmitted = true;
      this.transporterForm.updateValueAndValidity({
        onlySelf: true,
        emitEvent: true
      });

      if(this.transporterForm.valid){
        this.managementService.updateTransporter(this.transporterForm.value, this.ObjecFromTransporterParentForUpdate.id).subscribe({
          next: (data) => {
            this.actionPerformed();
            this.messageService.add({severity:'success', summary:'Service Message', detail:'Transporter Updated'});
          },
          error: error => this.errorMessage = error 
        })
      }
  }

  addBankingInfo(){
    this.bankingInfoForm.patchValue({
     status : 'ACTIVATED'
     }) 
     this.managementService.createBankingInfo(this.bankingInfoForm.value).subscribe({
       next: (data) => {
         this.actionPerformed();
         this.messageService.add({severity:'success', summary:'Service Message', detail:'Banking Info added'});
       },
       error: error => (
         this.messageService.add({severity:'error', summary:'Service Message', detail:error.details})
      )
     })
   }
 
   

}
