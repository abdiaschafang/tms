import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementBankingInformationsComponent } from './management-banking-informations.component';

describe('ManagementBankingInformationsComponent', () => {
  let component: ManagementBankingInformationsComponent;
  let fixture: ComponentFixture<ManagementBankingInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementBankingInformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementBankingInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
