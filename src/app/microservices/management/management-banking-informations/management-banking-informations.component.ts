import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { DataService } from '../management-data.service';
import { MatTableDataSource } from '@angular/material/table';
import { BankingInfo } from '../../../core/models/management/bankingInfo';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-management-banking-informations',
  templateUrl: './management-banking-informations.component.html',
  styleUrls: ['./management-banking-informations.component.scss']
})
export class ManagementBankingInformationsComponent implements OnInit,OnDestroy {
  public displayModal!: boolean;
  public subscription: Subscription = new Subscription();
  bankingInfoList : any;
  bankinInfo: any;
  public searchForm! : FormGroup;
  public spinner: boolean = false;
  errorMessage: any;
  selectedBankingInfo!: [];
  public bankingListData!: Observable<any>;
  public bankingInfo :any;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor( private data: DataService,
     private managementService: ManagementService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal));
    this.searchForm = this.formBuilder.group({
      param : [''],
      status: [''],
      tva :['']
  })

  this.getAllBankingInfo();
  }

  showModal(){
    this.bankingInfo = "";
    this.data.changeMessage(this.displayModal);
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  
  getAllBankingInfo(){
    // console.log(this.searchForm.value)
  this.spinner = true;
    this.managementService.getAllBankingInfo(this.searchForm.value).subscribe({
      next: bankinInfo => {
        this.bankinInfo = bankinInfo;
        this.spinner = false;
        this.bankingInfoList = this.bankinInfo.bankingInformations;
        const dataSource: MatTableDataSource<BankingInfo> = new MatTableDataSource<BankingInfo>(this.bankingInfoList);
        dataSource.paginator = this.paginator;
        this.bankingListData = dataSource.connect();
      },
      error: error => this.errorMessage = error 
    })
  }

  deleteBankingInfo(BankingInfoSelected :any){
    this.bankingInfo = BankingInfoSelected
  }

  editBankingInfo(BankingInfoSelected :any){
    this.bankingInfo = BankingInfoSelected ;
    this.data.changeMessage(this.displayModal);
  }


  SearchBankingInfo(){

  }



}
