import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreateBankingInformationsComponent } from './management-create-banking-informations.component';

describe('ManagementCreateBankingInformationsComponent', () => {
  let component: ManagementCreateBankingInformationsComponent;
  let fixture: ComponentFixture<ManagementCreateBankingInformationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreateBankingInformationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreateBankingInformationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
