import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../management-data.service';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-management-create-banking-informations',
  templateUrl: './management-create-banking-informations.component.html',
  styleUrls: ['./management-create-banking-informations.component.scss']
})
export class ManagementCreateBankingInformationsComponent implements OnInit {
  @Input() BankingInfoForUpdate : any;
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal= true;
  public bankingInfoForm!: FormGroup;
  subscription!: Subscription;
  public Transporter:any;
  TransportersList: any;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    iban: { required: 'Ce champ est obligatoire' },
    bic: { required: 'Ce champ est obligatoire' },
    transporter: { required: 'Ce champ est obligatoire' },
  }
  errorMessage: any;


  constructor(private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService:MessageService) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.bankingInfoForm = this.formBuilder.group({
      iban : ['',Validators.required],
      bic : ['',Validators.required],
      status : ['',Validators.required],
      transporter: ['',Validators.required]
    })
    console.log(this.BankingInfoForUpdate) 

    if(this.BankingInfoForUpdate != null || undefined){
      this.setBankingForm();
    }

    this.getAllTransporter();
  }


  setBankingForm(){
    this.bankingInfoForm.patchValue({
      iban : this.BankingInfoForUpdate.iban,
      bic : this.BankingInfoForUpdate.bic,
      // transporter: this.BankingInfoForUpdate?.transporter
    })
  }

  closeModal(){
    this.data.changeMessage(!this.displayModal);
  }

  actionPerformed(){
    this.actiontoPerformed.emit(true)
  }
  

  getAllTransporter(){
    let body = {
      param :'',
      status :''
    }
    this.managementService.getAllTransporter(body).subscribe({
      next: transporter => {
        this.Transporter = transporter;
        this.TransportersList = this.Transporter.transporters;
      },
      error: error => this.errorMessage = error 
    })
  }

  
  addBankingInfo(){
   this.bankingInfoForm.patchValue({
    status : 'ACTIVATED'
    }) 
    this.managementService.createBankingInfo(this.bankingInfoForm.value).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({severity:'success', summary:'Service Message', detail:'Banking Info added'});
      },
      error: error => (
        this.messageService.add({severity:'error', summary:'Service Message', detail:error.details})
     )
    })
  }

  updateBankingInfo(){
    this.bankingInfoForm.patchValue({
      status : this.BankingInfoForUpdate.status
      })
    console.log('update')
    console.log(this.bankingInfoForm.value);

    this.managementService.updateBankingInfo(this.bankingInfoForm.value, this.BankingInfoForUpdate.id).subscribe({
      next:(data)=>{
        this.actionPerformed();
        this.messageService.add({severity:'success', summary:'Service Message', detail:'Banking Info Updated'});
      },
      error: error => (
        this.messageService.add({severity:'error', summary:'Service Message', detail:error.details})
        )
    })
  } 

  

}
