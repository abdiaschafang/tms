import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { DataService } from '../../management-data.service';
import { Subscription } from 'rxjs';
import { ManagementService } from 'src/app/core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-management-detail-customer',
  templateUrl: './management-detail-customer.component.html',
  styleUrls: ['./management-detail-customer.component.scss']
})
export class ManagementDetailCustomerComponent implements OnInit {
  public displayedSideModal=true;
  public truckForm!: FormGroup;
  transporter: any;
  trucks: any
  allPaymentMethod: any;
  customerPaymentList: any;
  public paymentMethodIdList: any[]=[];
  public spinner: boolean=false;
  subscription!: Subscription;
  public searchForm!: FormGroup;
  public paymentForm!: FormGroup;
  @Input() customerForDetail?: any;
  @Output() public actiontoPerformed: EventEmitter<boolean>=new EventEmitter<boolean>();
  bankingInfo: any;
  drivers: any;
  public formErrors: { [key: string]: string }={};
  private validationMessages: { [key: string]: { [key: string]: string } }={
    iban: { required: 'Ce champ est obligatoire' },
    bic: { required: 'Ce champ est obligatoire' },
    transporter: { required: 'Ce champ est obligatoire' },
    name: { required: 'Ce champ est obligatoire' },
    surname: { required: 'Ce champ est obligatoire' },
    phone: { required: 'Ce champ est obligatoire' },
    email: { required: 'Ce champ est obligatoire' },
  }
  errorMessage: any;
  transporterBankingInfoList: any;
  transporterDriverList: any;
  customer: any;
  PaymentMethodList: any;
  paymentMethodList: any;


  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription=this.data.currentsSideModal.subscribe(displayedSideModal => this.displayedSideModal=displayedSideModal);
    this.paymentForm=this.formBuilder.group({
      type: ['', Validators.required],
      description: [''],
      status: [''],
    })
    this.searchForm=this.formBuilder.group({
      param: [''],
      status: ['']
    })

    this.getAllPaymentMethod();
    this.getCustomerTva();
  }

  closeSideModal() {
    this.data.showOrHideSideModal(!this.displayedSideModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onRowEditInit(bankingInfo: any) {
  }

  actionPerformed() {
    this.actiontoPerformed.emit(true);
  }


  getAllPaymentMethod() {
    this.spinner=true;
    let searchParam={ status: 'ACTIVATED', type: "" }
    this.managementService.getAllPaymentMethod(searchParam).subscribe({
      next: paymentMethods => {
        this.allPaymentMethod=paymentMethods;
        this.paymentMethodList=this.allPaymentMethod.payments;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  getCustomerTva() {
    this.searchForm.patchValue({
      status: this.customerForDetail.status,
      param: this.customerForDetail.tva,
    })
    this.spinner=true;
    this.managementService.getAllCustomer(this.searchForm.value).subscribe({
      next: customer => {
        this.customer=customer;
        this.spinner=false;
        this.customerPaymentList=this.customer.customers[0].payments;
      },
      error: error => this.errorMessage=error
    })
  }

  addPaymentMethodOnCustomer() {
    let type=this.paymentForm.get('type')?.value;
    for (let t of type) {
      this.paymentMethodIdList!.push(t.id);
      console.log(this.paymentMethodIdList)
    }
    this.managementService.AddPaymentMethodOnTransporter(this.paymentMethodIdList, this.customerForDetail!.id).subscribe({
      next: (data) => {
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Payment Method Added On Customer' });
        this.getCustomerTva();
      },
      error: error => this.errorMessage=error
    })
  }

  removePaymentMethodOnCustomer(customerSelected: any) {
    this.confirmationService.confirm({
      message: "Do you want to delete Payment Method &nbsp; : "+customerSelected.type,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.removePaymentMethodOnCustomer(this.customerForDetail!.id, customerSelected.id).subscribe({
          next: () => {
            this.getCustomerTva();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Payment Method Removed' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

}
