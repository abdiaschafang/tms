import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementDetailCustomerComponent } from './management-detail-customer.component';

describe('ManagementDetailCustomerComponent', () => {
  let component: ManagementDetailCustomerComponent;
  let fixture: ComponentFixture<ManagementDetailCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementDetailCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementDetailCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
