import { NgModule } from '@angular/core';

import { ManagementRoutingModule } from './management-routing.module';
import { SharedModule } from '../../modules/shared/shared.module';
import { ManagementCustomerComponent } from './management-customer/management-customer.component';
import { ManagementIndexComponent } from './management-index/management-index.component';
import { ManagementCreateCompanyComponent } from './management-create-customer/management-create-customer.component';
import { DataService } from './management-data.service';
import { ManagementCreateTransporterComponent } from './management-create-transporter/management-create-transporter.component';
import { ManagementTransporterComponent } from './management-transporter/management-transporter.component';
import { ManagementDriverComponent } from './management-driver/management-driver.component';
import { ManagementCreateDriverComponent } from './management-driver/management-create-driver/management-create-driver.component';
import { ManagementTruckComponent } from './management-truck/management-truck.component';
import { ManagementTruckCreateComponent } from './management-truck/management-truck-create/management-truck-create.component';
import { ManagementDetailTransporterComponent } from './management-transporter/management-detail-transporter/management-detail-transporter.component';
import { ManagementBankingInformationsComponent } from './management-banking-informations/management-banking-informations.component';
import { ManagementCreateBankingInformationsComponent } from './management-banking-informations/management-create-banking-informations/management-create-banking-informations.component';
import { ManagementGoodsComponent } from './management-goods/management-goods.component';
import { ManagementCreateGoodsComponent } from './management-goods/management-create-goods/management-create-goods.component';
import { ManagementPalletsComponent } from './management-pallets/management-pallets.component';
import { ManagementCreatePalletsComponent } from './management-pallets/management-create-pallets/management-create-pallets.component';
import { ManagementConfigurationsComponent } from './management-configutations/management-configurations.component';
import { ManagementCreateConfigurationComponent } from './management-configutations/management-create-configuration/management-create-configuration.component';
import { ManagementPaymentMethodComponent } from './management-payment-method/management-payment-method.component';
import { ManagementCreatePaymentMethodComponent } from './management-payment-method/management-create-payment-method/management-create-payment-method.component';
import { ManagementDetailCustomerComponent } from './management-customer/management-detail-customer/management-detail-customer.component';





@NgModule({
  providers:[
    DataService
  ],
  declarations: [
    ManagementIndexComponent,
    ManagementCustomerComponent,
    ManagementCreateCompanyComponent,
    ManagementCreateTransporterComponent,
    ManagementTransporterComponent,
    ManagementDriverComponent,
    ManagementCreateDriverComponent,
    ManagementTruckComponent,
    ManagementTruckCreateComponent,
    ManagementDetailTransporterComponent,
    ManagementBankingInformationsComponent,
    ManagementCreateBankingInformationsComponent,
    ManagementGoodsComponent,
    ManagementCreateGoodsComponent,
    ManagementPalletsComponent,
    ManagementCreatePalletsComponent,
    ManagementConfigurationsComponent,
    ManagementCreateConfigurationComponent,
    ManagementPaymentMethodComponent,
    ManagementCreatePaymentMethodComponent,
    ManagementDetailCustomerComponent
  ],
  imports: [
    ManagementRoutingModule,
    SharedModule,
  ]
})
export class ManagementModule { }
