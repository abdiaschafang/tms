import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-management-truck',
  templateUrl: './management-truck.component.html',
  styleUrls: ['./management-truck.component.scss']
})
export class ManagementTruckComponent implements OnInit {
  public displayModal!: boolean;
  public subscription: Subscription=new Subscription();
  trucks: any
  selectedTruck: any;
  truckList: any;
  errorMessage: any;
  selectedTruks!: [];
  searchForm!: FormGroup;
  public spinner: boolean=false;
  public allStatus!: any[];
  //propriety for export
   cols!: any[];
    exportColumns!: any[];


  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formbuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal=displayModal));
    this.searchForm=this.formbuilder.group({
      status: [''],
      param: ['']
    })
    this.allStatus=[
      { id: "ACTIVATED", name: "ACTIVER" },
      { id: "BLOCKED", name: "BLOQUER" },
    ]

    this.getAllTruck();
    
     this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }

  showModal() {
    this.selectedTruck="";
    this.data.changeMessage(this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllTruck() {
    this.spinner=true;
    this.searchForm.patchValue({
      status: 'ACTIVATED'
    })
    this.managementService.getAllTruck(this.searchForm.value).subscribe({
      next: trucks => {
        this.trucks=trucks;
        this.truckList=this.trucks.trucks;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  searchTrucks() {
    if (this.searchForm.get('status')?.value===null) {
      this.searchForm.patchValue({
        status: ''
      })
    }
    this.spinner=true;
    this.managementService.getAllTruck(this.searchForm.value).subscribe({
      next: trucks => {
        this.trucks=trucks;
        this.truckList=this.trucks.trucks;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  editTruck(selectedTruck: any) {
    this.selectedTruck=selectedTruck
    this.data.changeMessage(this.displayModal);
  }


  deleteTruck(selectedTruck: any) {
    this.confirmationService.confirm({
      message: "Do you want to delete this Truck &nbsp;: &nbsp;"+selectedTruck.name,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deleteTruck(selectedTruck.id).subscribe({
          next: () => {
            this.getAllTruck();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Truck has been deleted ' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

     // fonction of export file

    exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.truckList);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "truckList");
    });
  }
    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }
}
