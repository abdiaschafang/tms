import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementTruckComponent } from './management-truck.component';

describe('ManagementTruckComponent', () => {
  let component: ManagementTruckComponent;
  let fixture: ComponentFixture<ManagementTruckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementTruckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementTruckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
