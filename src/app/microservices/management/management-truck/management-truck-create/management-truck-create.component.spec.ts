import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementTruckCreateComponent } from './management-truck-create.component';

describe('ManagementTruckCreateComponent', () => {
  let component: ManagementTruckCreateComponent;
  let fixture: ComponentFixture<ManagementTruckCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementTruckCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementTruckCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
