import { Component, OnInit, OnDestroy, Output, EventEmitter, Input, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { DataService } from '../../management-data.service';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { GlobalGenericValidator } from '../../../../core/validators/global-generic.validator';

@Component({
  selector: 'app-management-truck-create',
  templateUrl: './management-truck-create.component.html',
  styleUrls: ['./management-truck-create.component.scss']
})
export class ManagementTruckCreateComponent implements OnInit {
  @Input() public editTruckData: any;
  @Output() public actiontoPerformed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal = true;
  subscription!: Subscription;
  public TruckForm!: FormGroup;
  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    name: { required: 'Ce champ est obligatoire' },
    mpl: { required: 'Ce champ est obligatoire' },
    tonnage: { required: 'Ce champ est obligatoire' },
    description: { required: 'Ce champ est obligatoire' },
  };
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];

  constructor(private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService: MessageService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.TruckForm = this.formBuilder.group({
      name: ['', Validators.required],
      mpl: ['', Validators.required],
      tonnage: ['', Validators.required],
      description: ['', Validators.required],
      status: [''],
    })
    this.setTruckForm();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.TruckForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.TruckForm, this.isFormSubmitted);
      });
  }

  // préremplire le formulaire pour modifier
  setTruckForm() {
    if (this.editTruckData != null || this.editTruckData != undefined) {
      this.TruckForm.patchValue({
        name: this.editTruckData.name,
        mpl: this.editTruckData.mpl,
        tonnage: this.editTruckData.tonnage,
        description: this.editTruckData.description,
      })
    }
  }

  closeModal() {
    this.data.changeMessage(!this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  actionPerformed() {
    this.actiontoPerformed.emit(true);
  }

  addTruck() {
    this.isFormSubmitted = true;
    this.TruckForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    if (this.TruckForm.valid) {
      this.TruckForm.patchValue({
        status: 'ACTIVATED'
      })
      this.managementService.createTruck(this.TruckForm.value,).subscribe({
        next: (data) => {
          this.actionPerformed();
          this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Truck added' });
        },
        error: error => (
          this.messageService.add({ severity: 'warning', summary: 'Service Message', detail: error.details })

        )
      })
    }
  }

  updateTruck() {
    this.TruckForm.patchValue({
      status: this.editTruckData.status,
    })
    this.managementService.updateTruck(this.TruckForm.value, this.editTruckData.id).subscribe({
      next: (data) => {
        this.actionPerformed();
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Truck updated' });
      },
      error: error => (
        this.messageService.add({ severity: 'warning', summary: 'Service Message', detail: error.details })
      )
    })
  }
}
