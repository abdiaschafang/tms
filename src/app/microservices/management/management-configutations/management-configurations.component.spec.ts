import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementConfigurationsComponent } from './management-configurations.component';

describe('ManagementConfigurationsComponent', () => {
  let component: ManagementConfigurationsComponent;
  let fixture: ComponentFixture<ManagementConfigurationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementConfigurationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
