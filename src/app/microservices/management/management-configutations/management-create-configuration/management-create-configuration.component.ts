import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { DataService } from '../../management-data.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-management-create-configuration',
  templateUrl: './management-create-configuration.component.html',
  styleUrls: ['./management-create-configuration.component.scss']
})
export class ManagementCreateConfigurationComponent implements OnInit {
  
  @Input() public editConfigurationData: any;
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal= true;
  subscription!: Subscription;
  public configurationForm!: FormGroup;
  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    key: { required: 'Ce champ est obligatoire' },
    value: { required: 'Ce champ est obligatoire' },
  };

  constructor( private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService:MessageService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.configurationForm=this.formBuilder.group({
      key : ['',Validators.required], 
      value : ['',Validators.required], 
    })
    this.setGoodsForm()
  }

  // préremplire le formulaire pour modifier
  setGoodsForm(){
    if(this.editConfigurationData != null || this.editConfigurationData != undefined){
      this.configurationForm.patchValue({
        key : this.editConfigurationData.key,
        value : this.editConfigurationData.value,
      })
    }
  }

  closeModal(){
    this.data.changeMessage(!this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

    actionPerformed(){
  this.actiontoPerformed.emit(true);
   }

    addConfiguration(){
      console.log (this.configurationForm.value);
      this.configurationForm.patchValue({
      status : 'ACTIVATED'
      })
      this.managementService.createConfiguration(this.configurationForm.value, ).subscribe({
        next: (data) => {
          this.actionPerformed();
          this.messageService.add({severity:'success', summary:'Service Message', detail:'Configuration added'});
        },
        error: error => (
          this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})

        )
      })
    }
    
    updateConfiguration(){
      this.configurationForm.patchValue({
        status : this.editConfigurationData.status,
        })
      this.managementService.updateConfiguration(this.configurationForm.value, this.editConfigurationData.id).subscribe({
        next: (data) => {
          this.actionPerformed();
          this.messageService.add({severity:'success', summary:'Service Message', detail:'Configuration updated'});
        },
        error: error => (
          this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})

        )
      })
    }


}
