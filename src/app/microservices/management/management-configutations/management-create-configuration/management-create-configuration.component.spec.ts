import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreateConfigurationComponent } from './management-create-configuration.component';

describe('ManagementCreateConfigurationComponent', () => {
  let component: ManagementCreateConfigurationComponent;
  let fixture: ComponentFixture<ManagementCreateConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreateConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreateConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
