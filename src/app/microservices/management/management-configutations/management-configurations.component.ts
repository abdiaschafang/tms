import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-management-configurations',
  templateUrl: './management-configurations.component.html',
  styleUrls: ['./management-configurations.component.scss']
})
export class ManagementConfigurationsComponent implements OnInit {
  public displayModal!: boolean;
  public subscription: Subscription=new Subscription();
  configurations: any
  selectedConfiguration: any;
  selectedConfigurations!: any[];
  configurationsList: any;
  errorMessage: any;
  searchForm!: FormGroup
  public driversData!: Observable<any>;
  public allStatus!: any[];
  public spinner: boolean=false;
    //propriety for export
   cols!: any[];
    exportColumns!: any[];


  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formbuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal=displayModal));
    this.searchForm=this.formbuilder.group({
      key: [''],
    })

    this.allStatus=[
      { id: "ACTIVATED", name: "ACTIVER" },
      { id: "BLOCKED", name: "BLOQUER" },
    ]

    this.getAllConfiguration();

     this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
     }));
    
  }
  showModal() {
    this.selectedConfiguration="";
    this.data.changeMessage(this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllConfiguration() {

    this.managementService.getAllConfiguration(this.searchForm.value).subscribe({
      next: configurations => {
        this.configurations=configurations;
        this.configurationsList=this.configurations.configurations;
      },
      error: error => this.errorMessage=error
    })
  }

  searchConfiguration() {
    this.spinner=true;
    this.managementService.getAllConfiguration(this.searchForm.value).subscribe({
      next: configurations => {
        this.configurations=configurations;
        this.configurationsList=this.configurations.configurations;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  editConfiguration(selectedConfiguration: any) {
    this.selectedConfiguration=selectedConfiguration
    this.data.changeMessage(this.displayModal);
  }




  deleteConfiguration(selectedConfiguration: any) {
    console.log(selectedConfiguration)
    this.confirmationService.confirm({
      message: "Do you want to delete this Goods &nbsp;: &nbsp;"+selectedConfiguration.name,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deleteGoods(selectedConfiguration.id).subscribe({
          next: () => {
            this.getAllConfiguration();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Goods has been deleted ' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

   // fonction of export file

    exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.configurationsList);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "palletsList");
    });
  }
    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }

}
