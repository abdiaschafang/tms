import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagementIndexComponent } from './management-index/management-index.component';
import { ManagementCustomerComponent } from './management-customer/management-customer.component';
import { ManagementCreateCompanyComponent } from './management-create-customer/management-create-customer.component';
import { ManagementCreateTransporterComponent } from './management-create-transporter/management-create-transporter.component';
import { ManagementTransporterComponent } from './management-transporter/management-transporter.component';
import { ManagementCreateDriverComponent } from './management-driver/management-create-driver/management-create-driver.component';
import { ManagementDriverComponent } from './management-driver/management-driver.component';
import { ManagementTruckComponent } from './management-truck/management-truck.component';
import { ManagementBankingInformationsComponent } from './management-banking-informations/management-banking-informations.component';
import { ManagementCreateBankingInformationsComponent } from './management-banking-informations/management-create-banking-informations/management-create-banking-informations.component';
import { ManagementGoodsComponent } from './management-goods/management-goods.component';
import { ManagementCreateGoodsComponent } from './management-goods/management-create-goods/management-create-goods.component';
import { ManagementPalletsComponent } from './management-pallets/management-pallets.component';
import { ManagementCreatePalletsComponent } from './management-pallets/management-create-pallets/management-create-pallets.component';
import { ManagementConfigurationsComponent } from './management-configutations/management-configurations.component';
import { ManagementCreateConfigurationComponent } from './management-configutations/management-create-configuration/management-create-configuration.component';
import { ManagementPaymentMethodComponent } from './management-payment-method/management-payment-method.component';
import { ManagementCreatePaymentMethodComponent } from './management-payment-method/management-create-payment-method/management-create-payment-method.component';

const routes: Routes = [
  { path: 'index', component: ManagementIndexComponent },
  { path: 'customer', component: ManagementCustomerComponent },
  { path: 'transporter', component: ManagementTransporterComponent },
  { path: 'create/company', component: ManagementCreateCompanyComponent },
  { path: 'create/transporter', component: ManagementCreateTransporterComponent },
  { path: 'driver', component: ManagementDriverComponent },
  { path: 'create/driver', component: ManagementCreateDriverComponent },
  { path: 'truck', component: ManagementTruckComponent },
  { path: 'bankingInformation', component: ManagementBankingInformationsComponent },
  { path: 'create/bankingInformation', component: ManagementCreateBankingInformationsComponent },
  { path: 'goods', component: ManagementGoodsComponent },
  { path: 'create/goods', component: ManagementCreateGoodsComponent },
  { path: 'pallets', component: ManagementPalletsComponent },
  { path: 'create/pallets', component: ManagementCreatePalletsComponent },
  { path: 'configurations', component: ManagementConfigurationsComponent },
  { path: 'createConfigurations', component: ManagementCreateConfigurationComponent },
  { path: 'paymentMethod', component: ManagementPaymentMethodComponent },
  { path: 'create/paymentMethod', component: ManagementCreatePaymentMethodComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule { }
