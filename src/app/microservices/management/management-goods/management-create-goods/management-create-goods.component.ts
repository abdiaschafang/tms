import { Component, OnInit, Output, EventEmitter, Input,ViewChildren,ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { fromEvent, merge, Observable, Subscription, takeWhile } from 'rxjs';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { DataService } from '../../management-data.service';
import { IGoods } from '../../../../core/models/management/Goods';
import { GlobalGenericValidator } from '../../../../core/validators/global-generic.validator';

@Component({
  selector: 'app-management-create-goods',
  templateUrl: './management-create-goods.component.html',
  styleUrls: ['./management-create-goods.component.scss']
})
export class ManagementCreateGoodsComponent implements OnInit {
  @Input() public editGoodsData: any;
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal= true;
  public goods!: IGoods;
  subscription!: Subscription;
  public goodsForm!: FormGroup;
  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    name: { required: 'This field is required' },
    description: { required: 'This field is required' },
  };
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];

  constructor( private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService:MessageService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.goodsForm=this.formBuilder.group({
      name : [this.goods?.name, Validators.required], 
      description : ['',], 
      status: [''],
    })
    this.setGoodsForm();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.goodsForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.goodsForm, this.isFormSubmitted);
      });
  }

  setGoodsForm(){
    if(this.editGoodsData != null || this.editGoodsData != undefined){
      this.goodsForm.patchValue({
        name : this.editGoodsData.name,
        description : this.editGoodsData.description,
      })
    }
  }

  closeModal(){
    this.data.changeMessage(!this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

    actionPerformed(){
  this.actiontoPerformed.emit(true);
   }

    addGoods(){
      this.isFormSubmitted = true;
      this.goodsForm.updateValueAndValidity({
        onlySelf: true,
        emitEvent: true
      });
       
      this.goodsForm.patchValue({
        status : 'ACTIVATED'
        }) 

      if(this.goodsForm.valid){
      this.managementService.createGoods(this.goodsForm.value, ).subscribe({
        next: (data) => {
          this.actionPerformed();
          this.messageService.add({severity:'success', summary:'Service Message', detail:'Goods added'});
        },
        error: error => (
          this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})
        )
      })
      }else{
        // console.log('not valid');
      }
 
    }
    
    updateGoods(){
      this.isFormSubmitted = true;
      this.goodsForm.updateValueAndValidity({
        onlySelf: true,
        emitEvent: true
      });
      
      this.goodsForm.patchValue({
        status : this.editGoodsData.status,
        })
        if(this.goodsForm.valid){
          this.managementService.updateGoods(this.goodsForm.value, this.editGoodsData.id).subscribe({
            next: (data) => {
              this.actionPerformed();
              this.messageService.add({severity:'success', summary:'Service Message', detail:'Goods updated'});
            },
            error: error => (
              this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})
            )
          })
        }
    
    }


}
