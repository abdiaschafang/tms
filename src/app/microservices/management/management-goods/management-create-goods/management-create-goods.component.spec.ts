import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreateGoodsComponent } from './management-create-goods.component';

describe('ManagementCreateGoodsComponent', () => {
  let component: ManagementCreateGoodsComponent;
  let fixture: ComponentFixture<ManagementCreateGoodsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreateGoodsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreateGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
