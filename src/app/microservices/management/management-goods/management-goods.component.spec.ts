import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementGoodsComponent } from './management-goods.component';

describe('ManagementGoodsComponent', () => {
  let component: ManagementGoodsComponent;
  let fixture: ComponentFixture<ManagementGoodsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementGoodsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
