import { Component, OnInit, OnDestroy, Output, EventEmitter, Input,ViewChildren,ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControlName } from '@angular/forms';
import { ManagementService } from '../../../../core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { DataService } from '../../management-data.service';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { GlobalGenericValidator } from '../../../../core/validators/global-generic.validator';


@Component({
  selector: 'app-management-create-payment-method',
  templateUrl: './management-create-payment-method.component.html',
  styleUrls: ['./management-create-payment-method.component.scss']
})
export class ManagementCreatePaymentMethodComponent implements OnInit {
  @Input() public paymentMethodData: any;
  @Output() public actiontoPerformed : EventEmitter<boolean> = new EventEmitter<boolean>();
  public displayModal= true;
  subscription!: Subscription;
  public paymentMethodForm!: FormGroup;
  public errorMessage!: string;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    type: { required: 'Ce champ est obligatoire' },
    description: { required: 'Ce champ est obligatoire' },
  };
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];


  constructor( private data: DataService,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService:MessageService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.data.currentMessage.subscribe(displayModal => this.displayModal = displayModal);
    this.globalGenericValidator = new GlobalGenericValidator(this.validationMessages);
    this.paymentMethodForm=this.formBuilder.group({
      type : ['',Validators.required], 
      description : ['',Validators.required], 
      status: [''],
    })
    this.setPaymentMethodForm();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[] = this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.paymentMethodForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors = this.globalGenericValidator.createErrorMessage(this.paymentMethodForm, this.isFormSubmitted);
      });
  }

    // préremplire le formulaire pour modifier
    setPaymentMethodForm(){
      if(this.paymentMethodData != null || this.paymentMethodData != undefined){
        this.paymentMethodForm.patchValue({
          type : this.paymentMethodData.type,
          description : this.paymentMethodData.description,
        
        })
      }
    }

    closeModal(){
      this.data.changeMessage(!this.displayModal);
    }
  
    ngOnDestroy() {
      this.subscription.unsubscribe();
    }
  
      actionPerformed(){
    this.actiontoPerformed.emit(true);
     }
  
      addPaymentMethod(){
        this.isFormSubmitted = true;
        this.paymentMethodForm.updateValueAndValidity({
          onlySelf: true,
          emitEvent: true
        });
        
        this.paymentMethodForm.patchValue({
        status : 'ACTIVATED'
        })
        if(this.paymentMethodForm.valid){
          this.managementService.createPaymentMethod(this.paymentMethodForm.value, ).subscribe({
            next: (data) => {
              this.actionPerformed();
              this.messageService.add({severity:'success', summary:'Service Message', detail:'Goods added'});
            },
            error: error => (
              this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})
            )
          })
        }
   
      }
      
      updatePaymentMethod(){
        this.isFormSubmitted = true;
        this.paymentMethodForm.updateValueAndValidity({
          onlySelf: true,
          emitEvent: true
        });
        this.paymentMethodForm.patchValue({
          status : this.paymentMethodData.status,
          })
          if(this.paymentMethodForm.valid){
            this.managementService.updatePaymentMethod(this.paymentMethodForm.value, this.paymentMethodData.id).subscribe({
              next: (data) => {
                this.actionPerformed();
                this.messageService.add({severity:'success', summary:'Service Message', detail:'Goods updated'});
              },
              error: error => (
                this.messageService.add({severity:'warning', summary:'Service Message', detail:error.details})
              )
            })
          }
      
      }
  
  

}
