import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementCreatePaymentMethodComponent } from './management-create-payment-method.component';

describe('ManagementCreatePaymentMethodComponent', () => {
  let component: ManagementCreatePaymentMethodComponent;
  let fixture: ComponentFixture<ManagementCreatePaymentMethodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementCreatePaymentMethodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementCreatePaymentMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
