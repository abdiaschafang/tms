import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementPaymentMethodComponent } from './management-payment-method.component';

describe('ManagementPaymentMethodComponent', () => {
  let component: ManagementPaymentMethodComponent;
  let fixture: ComponentFixture<ManagementPaymentMethodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementPaymentMethodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementPaymentMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
