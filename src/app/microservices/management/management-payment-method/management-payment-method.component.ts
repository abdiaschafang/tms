import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../management-data.service';
import { ManagementService } from '../../../core/services/management/management.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as FileSaver from 'file-saver';


@Component({
  selector: 'app-management-payment-method',
  templateUrl: './management-payment-method.component.html',
  styleUrls: ['./management-payment-method.component.scss']
})
export class ManagementPaymentMethodComponent implements OnInit {
  public displayModal!: boolean;
  public subscription: Subscription=new Subscription();
  PaymentMethods: any
  selectedPaymentMethod: any;
  PaymentMethodList: any;
  errorMessage: any;
  selectedDriver!: [];
  searchForm!: FormGroup;
  public spinner: boolean=false;
  public allStatus!: any[];

    //propriety for export
   cols!: any[];
    exportColumns!: any[];


  constructor(private data: DataService,
    private managementService: ManagementService,
    private messageService: MessageService,
    private formbuilder: FormBuilder,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.subscription.add(this.data.currentMessage.subscribe(displayModal => this.displayModal=displayModal));
    this.searchForm=this.formbuilder.group({
      status: [''],
      type: ['']
    })
    this.allStatus=[
      { id: "ACTIVATED", name: "ACTIVER" },
      { id: "BLOCKED", name: "BLOQUER" },
    ]

    this.getAllPaymentMethod();
  }


  showModal() {
    this.selectedPaymentMethod="";
    this.data.changeMessage(this.displayModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllPaymentMethod() {
    this.spinner=true;
    this.searchForm.patchValue({
      status: 'ACTIVATED'
    })
    this.managementService.getAllPaymentMethod(this.searchForm.value).subscribe({
      next: PaymentMethods => {
        this.PaymentMethods=PaymentMethods;
        this.PaymentMethodList=this.PaymentMethods.payments;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  } 

  searchPaymentMethod() {
    if (this.searchForm.get('status')?.value===null) {
      this.searchForm.patchValue({
        status: ''
      })
    }
    this.spinner=true;
    this.managementService.getAllPaymentMethod(this.searchForm.value).subscribe({
      next: PaymentMethods => {
        this.PaymentMethods=PaymentMethods;
        this.PaymentMethodList=this.PaymentMethods.payments;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  editPaymentMethod(selectedPaymentMethod: any) {
    this.selectedPaymentMethod=selectedPaymentMethod
    this.data.changeMessage(this.displayModal);
  }




  deletePaymentMethod(selectedPaymentMethod: any) {
    console.log(selectedPaymentMethod)
    this.confirmationService.confirm({
      message: "Do you want to delete this PaymentMethod &nbsp;: &nbsp;"+selectedPaymentMethod.name,
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.managementService.deletePaymentMethod(selectedPaymentMethod.id).subscribe({
          next: () => {
            this.getAllPaymentMethod();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'PaymentMethod has been deleted ' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }


  // fonction of export file

    exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.PaymentMethodList);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: "xlsx",
        type: "array"
      });
      this.saveAsExcelFile(excelBuffer, "PaymentMethodList");
    });
  }
    saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    }


}
