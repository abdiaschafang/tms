import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TrackingService } from 'src/app/core/services/tracking/tracking.service';
import { MonitoringSidebarService } from '../monitoring-sidebar.service';

@Component({
  selector: 'app-detail-transport',
  templateUrl: './detail-transport.component.html',
  styleUrls: ['./detail-transport.component.scss']
})
export class DetailTransportComponent implements OnInit {
  public displayedSideModal = true;
  public displayModal = true;
  subscription!: Subscription;
  @Input() transporterIdForDetail!: any;
  errorMessage: any;
  transportDetail:any;


  constructor(private data: MonitoringSidebarService,
    private trackingService: TrackingService,) { }

  ngOnInit(): void {
    this.subscription = this.data.currentsSideModal.subscribe(displayedSideModal => this.displayedSideModal = displayedSideModal);
    this.getTrasportById();
  }

  closeSideModal() {
    this.data.showOrHideSideModal(!this.displayedSideModal);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getTrasportById() {

    this.trackingService.getTransportTrackingDetail(this.transporterIdForDetail).subscribe({
      next: transport => {
        this.transportDetail= transport;
      },
      error: error => {
        this.errorMessage = error
      }
    })
  }

}
