import { MessageService } from 'primeng/api';
import { TrackingService } from 'src/app/core/services/tracking/tracking.service';
import { SharedService } from './../../../core/services/shared/shared.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-doc',
  templateUrl: './add-doc.component.html',
  styleUrls: ['./add-doc.component.scss']
})
export class AddDocComponent implements OnInit, OnDestroy {

  public displayModal1!: boolean;
  public subscription: Subscription=new Subscription();

  selectedFiles?: FileList;
  currentFile?: File;
  progress=0;
  message='';
  transport!: string;
  errorMessage!: string;
  fileInfos?: Observable<any>;
  public addDocForm!: FormGroup;

  @Input() childTransportTracking: any;
  @Output() transportTracking: EventEmitter<any>=new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private messageService: MessageService,
    private monitoringService: TrackingService
  ) { }

  ngOnInit(): void {
    this.addDocForm=this.formBuilder.group({
      docFile: ['', Validators.required]
    })
    this.subscription.add(this.sharedService.docModal.subscribe(displayModal1 => this.displayModal1=displayModal1));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  selectFile(event: any): void {
    this.selectedFiles=event.target.files;
  }

  uploadFile(): void {
    this.progress=0;
    if (this.selectedFiles) {
      const file: File|null=this.selectedFiles.item(0);
      if (file) {
        this.currentFile=file;
        const fileExtension=this.currentFile.name.split('.').pop();
        const currentFileName=this.currentFile.name.split('.').slice(0, -1).join('.');

        if (fileExtension==='pdf'||fileExtension==='png'||fileExtension==='jpeg'||fileExtension==='jpg') {
          if (this.sharedService.containsSpecialChars(currentFileName)) {
            this.errorMessage='The file name cannot contain special characters';
          } else {
            this.monitoringService.uploadDoc(this.currentFile, this.childTransportTracking.transport).subscribe({
              next: () => {
                this.messageService.add({ severity: 'success', summary: 'Service message', detail: 'Document uploaded successfully' });
                this.addDocForm.reset();
                this.displayModal1=false;
                this.transportTracking.emit(true);
              },
              error: () => {
                this.errorMessage='Error with file';
              }
            });
          }
        } else {
          this.errorMessage='Error with file type we accept pdf and images file !';
        }
      }
    } else {
      this.errorMessage='Please select a file';
    }
  }

  updateUploadFile(): void {
    this.progress=0;
    if (this.selectedFiles) {
      const file: File|null=this.selectedFiles.item(0);
      if (file) {
        this.currentFile=file;
        const fileExtension=this.currentFile.name.split('.').pop();
        const currentFileName=this.currentFile.name.split('.').slice(0, -1).join('.');

        if (fileExtension==='pdf'||fileExtension==='png'||fileExtension==='jpeg'||fileExtension==='jpg') {
          if (this.sharedService.containsSpecialChars(currentFileName)) {
            this.errorMessage='The file name cannot contain special characters';
          } else {
            this.monitoringService.updateUploadDoc(this.currentFile, this.childTransportTracking.transport).subscribe({
              next: () => {
                this.messageService.add({ severity: 'success', summary: 'Service message', detail: 'Document uploaded successfully' });
                this.addDocForm.reset();
                this.displayModal1=false;
                this.transportTracking.emit(true);
              },
              error: () => {
                this.errorMessage='Error with file';
              }
            });
          }
        } else {
          this.errorMessage='Error with file type';
        }
      }
    } else {
      this.errorMessage='Please select a file';
    }
  }

  cancelUploadFile() {
    this.addDocForm.reset();
    this.errorMessage='';
    this.sharedService.showOrHideAllDocModal(!this.displayModal1);
  }

}
