import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCmrComponent } from './add-cmr.component';

describe('AddCmrComponent', () => {
  let component: AddCmrComponent;
  let fixture: ComponentFixture<AddCmrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCmrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCmrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
