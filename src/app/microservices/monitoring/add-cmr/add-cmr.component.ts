import { MessageService } from 'primeng/api';
import { SharedService } from './../../../core/services/shared/shared.service';
import { Subscription, Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { TrackingService } from 'src/app/core/services/tracking/tracking.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-cmr',
  templateUrl: './add-cmr.component.html',
  styleUrls: ['./add-cmr.component.scss']
})
export class AddCmrComponent implements OnInit, OnDestroy {

  public displayModal!: boolean;
  public subscription: Subscription=new Subscription();

  selectedFiles?: FileList;
  currentFile?: File;
  message='';
  transport!: string;
  errorMessage!: string;
  fileInfos?: Observable<any>;
  public addCmrForm!: FormGroup;

  @Input() childTransportTracking: any;
  @Output() transportTracking: EventEmitter<any>=new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private sharedService: SharedService,
    private monitoringService: TrackingService,
    private router: Router,
    private messageService: MessageService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.addCmrForm=this.formBuilder.group({
      cmrFile: ['', [Validators.required]],

    });
    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));

    this.route.paramMap.subscribe(() => {
      this.displayCmrName();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  selectFile(event: any): void {
    this.selectedFiles=event.target.files;
  }

  uploadCmr(): void {
    if (this.selectedFiles) {
      const file: File|null=this.selectedFiles.item(0);
      if (file) {
        this.currentFile=file;
        const fileExtension=this.currentFile.name.split('.').pop();
        const currentFileName=this.currentFile.name.split('.').slice(0, -1).join('.');

        if (fileExtension==='pdf'||fileExtension==='png'||fileExtension==='jpeg'||fileExtension==='jpg') {
          if (this.sharedService.containsSpecialChars(currentFileName)) {
            this.errorMessage='The file name cannot contain special characters';
          } else {
            this.monitoringService.uploadCmr(this.currentFile, this.childTransportTracking.transport).subscribe({
              next: () => {
                this.messageService.add({ severity: 'success', summary: 'Service message', detail: 'Document uploaded successfully' });
                this.addCmrForm.reset();
                this.displayModal=false;
                this.transportTracking.emit(true);
              },
              error: () => {
                this.errorMessage='Error with file';
              }
            });
          }
          this.router.navigateByUrl('monitoring/list');
        } else {
          this.errorMessage='Error with file type';
        }
      }
    } else {
      this.errorMessage='Please select a file';
    }
  }

  updateUploadCmr(): void {
    if (this.selectedFiles) {
      const file: File|null=this.selectedFiles.item(0);
      if (file) {
        this.currentFile=file;
        const fileExtension=this.currentFile.name.split('.').pop();
        const currentFileName=this.currentFile.name.split('.').slice(0, -1).join('.');

        if (fileExtension==='pdf'||fileExtension==='png'||fileExtension==='jpeg'||fileExtension==='jpg') {
          if (this.sharedService.containsSpecialChars(currentFileName)) {
            this.errorMessage='The file name cannot contain special characters';
          } else {
            this.monitoringService.updateUploadCmr(this.currentFile, this.childTransportTracking.transport).subscribe({
              next: () => {
                this.messageService.add({ severity: 'success', summary: 'Service message', detail: 'Document uploaded successfully' });
                this.addCmrForm.reset();
                this.displayModal=false;
                this.transportTracking.emit(true);
              },
              error: () => {
                this.errorMessage='Error with file';
              }
            });
          }
          this.router.navigateByUrl('monitoring/list');
        } else {
          this.errorMessage='Error with file type we accept pdf and images file !';
        }
      }
    } else {
      this.errorMessage='Please select a file';
    }
  }

  displayCmrName(): void {
    this.addCmrForm.patchValue({
      cmrFile: this.childTransportTracking?.cmrFile
    })
  }

  cancelUploadCmr() {
    this.addCmrForm.reset();
    this.errorMessage='';
    this.sharedService.showOrHideAllModal(!this.displayModal);
  }

}
