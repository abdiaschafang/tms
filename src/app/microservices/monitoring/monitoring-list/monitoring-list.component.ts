import { SharedService } from './../../../core/services/shared/shared.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { TrackingService } from 'src/app/core/services/tracking/tracking.service';
import { MonitoringSidebarService } from '../monitoring-sidebar.service';

@Component({
  selector: 'app-monitoring-list',
  templateUrl: './monitoring-list.component.html',
  styleUrls: ['./monitoring-list.component.scss']
})
export class MonitoringListComponent implements OnInit {
  public errorMessage!: string;
  public urlCmrFile!: string;
  public urlDocFile!: string;
  public currentFileExtension!: string;
  public status!: any[]; 
  public searchForm!: FormGroup;
  public transportsTrackingList: any;
  public spinner: boolean=false;
  public cmrFile!: string;
  public transportsTracking: any;
  selectedTransportTracking!: [];
  public displayModal!: boolean;
  public displayModal1!: boolean;

  public parentTransportTracking: any;
  public subscription: Subscription=new Subscription();
  selectedtransportsTracking!: [];
  transporterId!: any;
  public displayedSideModal!: boolean;
  totalElements: any;
  totalLitigation: any;
  public loading: boolean=false;

  constructor(
    private trackingService: TrackingService,
    private formBuilder: FormBuilder,
    private data: MonitoringSidebarService,
    private messageService: MessageService,
    private sharedService: SharedService,
    private confirmationService: ConfirmationService
  ) {
    this.status=[{ name: 'VALID' }, { name: 'CREATED' }];
  }

  ngOnInit(): void {
    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
    this.subscription.add(this.data.currentsSideModal.subscribe(displayedSideModal => this.displayedSideModal=displayedSideModal));
    this.searchForm=this.formBuilder.group({
      status: [''],
      transport: ['']
    })

    this.searchTransport();
  }

  receiveCmrData() {
    this.searchTransport();
  }

  searchTransport() {
    this.loading=true;

    if (this.searchForm.get('status')?.value==='') {
      this.searchForm.patchValue({
        status: "CREATED",
      })
    }
    this.spinner=true;
    this.trackingService.getTransports(this.searchForm.value).subscribe({
      next: transport => {
        this.transportsTracking=transport;
        this.totalElements=this.transportsTracking.totalElements;
        this.totalLitigation=this.transportsTracking.totalLitigation;
        this.transportsTracking=this.transportsTracking.transportTracking;
        this.loading=false;
      },
      error: error => this.errorMessage=error
    })
  }

  uploadCmr(transportsTracking: {}) {
    this.parentTransportTracking=transportsTracking;

    this.sharedService.showOrHideAllModal(this.displayModal);
  }

  uploadDocument(transportsTracking: {}) {
    this.parentTransportTracking=transportsTracking;

    this.sharedService.showOrHideAllDocModal(this.displayModal1);
  }

  detailTransportsTracking(transportsTracking: any) {
    this.transporterId=transportsTracking.transport;
    this.data.showOrHideSideModal(this.displayedSideModal);
  }

  validTransportsTracking(transportsTracking: any) {
    this.confirmationService.confirm({
      message: "Do you want to valid This transport &nbsp; : "+transportsTracking.transport,
      header: 'Valid Transport',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.trackingService.validTransports(transportsTracking.transport).subscribe({
          next: () => {
            this.searchTransport();
            this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Transport Valided' });
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  PutOnLitige(transportsTracking: any) {
    this.confirmationService.confirm({
      message: "Do you want to put this transport on Dispute ? &nbsp; : ",
      header: 'Put Transport on Litigation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.trackingService.putTransportOnDispute(transportsTracking.transport).subscribe({
          next: () => {
            this.searchTransport();
            if (transportsTracking?.litigation==false) {
              this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Transport is put on consolDispute' });
            } else {
              this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'Transport is Remove on Dispute' });
            }
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  downloadCmrFile(transportsTracking: any) {
    const data=transportsTracking;
    this.trackingService.downloadCmr(data.transport).subscribe((response: any) => {
      let blob=new Blob([response.body], { type: response.body.type });
      let url=URL.createObjectURL(blob);
      window.open(url, '_blank');
    });
  }

  downloadDocFile(transportsTracking: any) {
    const data=transportsTracking;
    this.trackingService.downloadDoc(data.transport).subscribe((response: any) => {
      let blob=new Blob([response.body], { type: response.body.type });
      let url=URL.createObjectURL(blob);
      window.open(url, '_blank');
    });
  }
}
