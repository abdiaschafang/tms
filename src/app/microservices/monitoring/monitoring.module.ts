import { NgModule } from '@angular/core';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { MonitoringListComponent } from './monitoring-list/monitoring-list.component';
import { AddCmrComponent } from './add-cmr/add-cmr.component';
import { AddDocComponent } from './add-doc/add-doc.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { DetailTransportComponent } from './detail-transport/detail-transport.component';
import { MonitoringSidebarService } from './monitoring-sidebar.service';


@NgModule({
  providers:[
    MonitoringSidebarService 
  ],
  declarations: [
    MonitoringListComponent,
    AddCmrComponent,
    AddDocComponent,
    DetailTransportComponent
  ],
  imports: [
    MonitoringRoutingModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ]
})
export class MonitoringModule { }
