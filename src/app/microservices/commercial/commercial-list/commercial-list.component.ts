import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PrimeNGConfig } from 'primeng/api';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.state';
import { IOrder } from 'src/app/core/models/order';
import { CommercialService } from 'src/app/core/services/commercial/commercial.service';
import { SharedService } from 'src/app/core/services/shared/shared.service';

@Component({
  selector: 'app-commercial-list',
  templateUrl: './commercial-list.component.html',
  styleUrls: ['./commercial-list.component.scss']
})

export class CommercialListComponent implements OnInit {

  public myOrders!: Observable<IOrder[]>;
  public filteredForm!: any;
  public orders: any;
  public spinner: boolean=false;
  public loader: boolean=true;
  public countries: any;
  public cities: any;
  public filteredData: any;
  public country: any;
  public data: any;
  public dataCities: any;
  public status!: any;
  public zipCodes!: any[];
  public errorMessage!: string;
  public loadCountryCode!: string;
  public loadCity!: string;
  public loadZipCode!: string;
  public unloadCountryCode!: string;
  public unloadCity!: string;
  public statusE!: any;
  public unloadZipCode!: string;
  public search!: string;
  public selectedCountryCode="";
  visibleSidebar4:any;
  param!: any;
  storedPayload: any;

  public searchForm!: FormGroup;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public obs!: Observable<any>;
  public $cities!: Observable<any>;
  clicked: any=true;
  clicked1: any=true;
  clicked2: any=true;
  clicked3: any=true;
  clicked4: any=true;
  clicked5: any=true;
  clicked6: any=true;
  clicked7:any=true;

  public subscription: Subscription=new Subscription();

  constructor(
    private store: Store<AppState>,
    private primengConfig: PrimeNGConfig,
    private commercialService: CommercialService,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
  ) {
    this.myOrders=this.store.select('order')
    this.status=[{ name: 'Select' }, { name: 'DRAFT' }, { name: 'CREATED' }, { name: 'LOAD' }, { name: 'DELIVERED' }, { name: 'CANCELED' }, { name: 'PAID' },  { name: 'SEARCHING_SOLUTION' }];
    this.zipCodes=[ { name: 'Select' },{ name: 'FR4533' }, { name: 'DF5225' }, { name: 'RF5225' }, { name: 'DFR2266' }];
  }

  ngOnInit(): void {
    this.primengConfig.ripple=true;

    this.searchForm=this.formBuilder.group({
      loadCountryCode: [''],
      loadCity: ['', { disabled: true }],
      loadZipCode: [''],
      unloadCountryCode: [''],
      unloadCity: [''],
      unloadZipCode: [''],
      status: [''],
      param: ['']
    })
    this.getOrders();
    this.searchForm.patchValue({
      loadCountryCode: this.loadCountryCode,
      unloadCountryCode: this.unloadCountryCode
    })
  }

  private getOrders() {
    this.commercialService.getOrders(this.searchForm.value).subscribe({
      next: orders => {
        this.data=orders;
        this.orders=this.data.orders
        this.orders.length===0? this.sharedService.toastMessageOrders(this.orders):null;
        const dataSource: MatTableDataSource<IOrder>=new MatTableDataSource<IOrder>(this.orders);
        dataSource.paginator=this.paginator;
        this.obs=dataSource.connect();
        this.loader=false;
      },
      error: error => (
        this.errorMessage=error,
        this.sharedService.toastMessageOrders(this.orders),
        this.loader=false
      )
    })
  }

  public getOrdersByParam() {
    this.loader=true;
    this.loadCity=this.searchForm.get('loadCity')?.value||'';
    this.loadCountryCode=this.searchForm.get('loadCountryCode')?.value||'';
    this.unloadCountryCode=this.searchForm.get('unloadCountryCode')?.value||'';
    this.loadZipCode=this.searchForm.get('loadZipCode')?.value.name||'';
    this.unloadCity=this.searchForm.get('unloadCity')?.value||'';
    this.statusE=this.searchForm.get('status')?.value.name||'';
    this.param=this.searchForm.get('param')?.value||'';
    this.unloadZipCode=this.searchForm.get('unloadZipCode')?.value.name||'';

    let payload={
      loadCity: this.loadCity,
      loadCountryCode: this.loadCountryCode,
      unloadCountryCode: this.unloadCountryCode,
      loadZipCode: this.loadZipCode,
      unloadCity: this.unloadCity,
      param: this.param,
      unloadZipCode: this.unloadZipCode,
      status:this.statusE,
    }

    this.storedPayload=payload;
    this.commercialService.getOrdersByParam(payload).subscribe({
      next: orders => {
        this.data=orders;
        this.orders=this.data.orders;
        this.sharedService.toastMessageOrders(this.orders);
        const dataSource: MatTableDataSource<IOrder>=new MatTableDataSource<IOrder>(this.orders);
        dataSource.paginator=this.paginator;
        this.obs=dataSource.connect();
        this.loader=false;
      },
      error: error => (
        this.errorMessage=error,
        this.sharedService.toastMessageOrders(this.orders),
        this.loader=false
      )
    })

  }

  public getCountriesByEvent(param: any) {
    this.selectedCountryCode=param.code;
    this.sharedService.getCountries().subscribe({
      next: countries => {
        this.countries=countries;
      },
      error: error => this.errorMessage=error
    })
  }

  public getCountries() {
    this.spinner=true;
    this.sharedService.getCountries().subscribe({
      next: countries => {
        this.countries=countries;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  public getCities(param: any) {
    this.sharedService.getCities(this.selectedCountryCode, param.query).subscribe({
      next: cities => {
        this.dataCities=cities;
        this.cities=this.dataCities.cities;
      },
      error: error => this.errorMessage=error
    })
  }

  public isDisable() {
    return this.searchForm.get('loadCountryCode')?.value===null||this.searchForm.get('loadCountryCode')?.value===undefined? true:false;
  }

  changeCity(e: any) {
    this.status?.setValue(e.target.value, {
      onlySelf: true,
    });
  }


  setSearch(input: any) {
    console.log(input);
    switch (input) {
      case '1': {
        this.clicked=false;
        this.storedPayload.loadCity='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }

      case '2': {
        this.clicked1=false;
        this.storedPayload.loadCountryCode='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '3': {
        this.clicked2=false;
        this.storedPayload.unloadCountryCode='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '4': {
        this.clicked3=false;
          this.storedPayload.loadZipCode='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '5': {
        this.clicked4=false;
        this.storedPayload.unloadCity='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '6': {
        this.clicked5=false;
        this.storedPayload.param='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }

      case '7': {
        this.clicked6=false;
        this.storedPayload.status='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '8': {
        this.clicked7=false;
        this.storedPayload.unloadZipCode='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      default: {
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
    }
    console.log(this.storedPayload);
  }

  sendSearchInBackend(formValue:any) {
        this.commercialService.getOrdersByParam(formValue).subscribe({
      next: orders => {
            this.data=orders;
        this.orders=this.data.orders;
        this.sharedService.toastMessageOrders(this.orders);
        const dataSource: MatTableDataSource<IOrder>=new MatTableDataSource<IOrder>(this.orders);
        dataSource.paginator=this.paginator;
        this.obs=dataSource.connect();
        this.loader=false;
      },
      error: error => (
        this.errorMessage=error,
        this.sharedService.toastMessageOrders(this.orders),
        this.loader=false
      )
    })
  }
}
