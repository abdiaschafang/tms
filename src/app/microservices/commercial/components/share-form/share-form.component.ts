import { Component, ElementRef, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder, FormControlName, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { CommercialService } from 'src/app/core/services/commercial/commercial.service';
import { SharedService } from 'src/app/core/services/shared/shared.service';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';

@Component({
  selector: 'app-share-form',
  templateUrl: './share-form.component.html',
  styleUrls: ['./share-form.component.scss']
})
export class ShareFormComponent implements OnInit, OnDestroy {

  public pageTitle!: string;
  public orderId!: string;
  public displayModal!: boolean;
  public users: any[]=[];
  public subscription: Subscription=new Subscription();

  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  public shareOrderForm!: FormGroup;

  public UsersAvailables: any=[];

  public UserSelecteds: any=[];
  public SelectUsers: any=[];

  public errorMessage!: string;
  public formErrors: { [key: string]: string }={};
  private validationMessages: { [key: string]: { [key: string]: string } }={ shareOrderUsers: { required: 'Please inform users to share' } };

  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private commercialService: CommercialService,
    private messageService: MessageService,
    private router: Router
  ) {

    this.UsersAvailables=[
      { name: 'LGL' },
      { name: 'MAT TRANS' },
      { name: 'BELGO' },
      { name: 'ZEND R' },
      { name: 'ZEND R' },
      { name: 'ZEND R' },
      { name: 'ZEND R' },
      { name: 'ZEND R' }
    ];
  }

  ngOnInit(): void {


    this.route.paramMap.subscribe(params => {
      this.orderId=params.get('id')||'';
    });

    this.globalGenericValidator=new GlobalGenericValidator(this.validationMessages);
    this.shareOrderForm=this.formBuilder.group({
      shareOrderUsers: ['', Validators.required]
    })

    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[]=this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));

    merge(this.shareOrderForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors=this.globalGenericValidator.createErrorMessage(this.shareOrderForm, this.isFormSubmitted);
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  showDestinationModal() {
    this.sharedService.showOrHideAllModal(!this.displayModal);
  }

  shareOrder() {
    this.isFormSubmitted=true;
    this.shareOrderForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    for (let key in this.UserSelecteds) {
      this.SelectUsers.push(this.UserSelecteds[key].name);
    }
        this.confirmationService.confirm({
          message: `Do you want to share this order with selected users ?`,
          header: 'You will share this order',
          icon: "pi pi-exclamation-triangle",
          accept: () => {
            this.messageService.add({ severity: "success", summary: "Service Message", detail: "The order has been shared you will be redirected to the list of orders" });
            this.commercialService.shareOrder(this.orderId, this.SelectUsers).subscribe({
              next: () => {
                this.sharedService.showOrHideAllModal(this.displayModal); 
                this.subscription.unsubscribe();
                this.router.navigate(['commercial/list']);
               },
              error: error => this.errorMessage = error
            }) 
          },
          reject: () => {
            this.messageService.add({ severity: "error", summary: "Service Message", detail: "You have rejected" });
          }
        });
  }

}
