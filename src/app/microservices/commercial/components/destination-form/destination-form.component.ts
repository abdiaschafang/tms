import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewChildren } from '@angular/core';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder, FormControlName } from '@angular/forms';
import { SharedService } from 'src/app/core/services/shared/shared.service';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';
import { DatePipe } from '@angular/common';
import { IDestination } from 'src/app/core/models/destination';
import { MessageService } from 'primeng/api';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-destination-form',
  templateUrl: './destination-form.component.html',
  styleUrls: ['./destination-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DestinationFormComponent implements OnInit, AfterViewInit {

  public displayModal!: boolean;
  public spinner: boolean=false;
  public subscription: Subscription=new Subscription();
  public type!: any[];
  public destinations: any;
  public chargementType: any;
  public dataDropdown: any;
  public countrysDropdown: any;
  public citysDropdown: any;
  public countries: any;
  public cities: any;
  public selectedCountryCode="";
  public countryName="";
  public finalGeolocation: string[]=[];

  public pageTitle!: string;

  public errorMessage!: string;
  public formErrors: { [key: string]: string }={};
  private validationMessages: { [key: string]: { [key: string]: string } }={
    destinationType: { required: 'Ce champ est obligatoire' },
    countryName: { required: 'Ce champ est obligatoire' },
    city: { required: 'Ce champ est obligatoire' },
    address: { required: 'Ce champ est obligatoire' },
    zipcode: { required: 'Ce champ est obligatoire' },
    tonnage: { required: 'Ce champ est obligatoire' },
    dateStr: { required: 'Ce champ est obligatoire' }
  }
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;
  public destinationForm!: FormGroup;
  public destination!: IDestination;
  public dataCities: any;

  public $cities!: Observable<any>;

  public orderId!: string;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];
  @Input() childRoute: any;
  @Input() updatedChildRoute: any;
  @Input() childRoutes: any;
  @Input() childOrder: any;
  @Output() emptyChildOrder: EventEmitter<string>=new EventEmitter<string>();
  @Output() emptyChildRoute: EventEmitter<any>=new EventEmitter<any>();

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private messageService: MessageService
  ) {
    this.chargementType=[
      { name: 'LOAD', code: 'LOAD' },
      { name: 'UNLOAD', code: 'UNLOAD' }
    ];
  }

  ngOnInit(): void {
    this.orderId=this.childOrder?.orderId;
    this.getCountries();

    this.globalGenericValidator=new GlobalGenericValidator(this.validationMessages);
    this.destinationForm=this.formBuilder.group({
      destinationType: ['', Validators.required],
      countryName: ['', Validators.required],
      countryCode: [''],
      loadRef: [''],
      geolocation: [''],
      city: ['', Validators.required],
      address: ['', Validators.required],
      zipcode: ['', Validators.required],
      tonnage: ['', Validators.required],
      mpl: [''],
      dateStr: ['', Validators.required]
    })

    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
    this.subscription.add(this.sharedService.currentMessage.subscribe(emitDestinations => this.destinations=emitDestinations));

    this.displayDestination();
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[]=this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));

    merge(this.destinationForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors=this.globalGenericValidator.createErrorMessage(this.destinationForm, this.isFormSubmitted);
      });
  }

  ngOnDestroy() {
    this.childOrder=null;
    this.updatedChildRoute=null;
    this.emptyChildOrder.emit(this.childOrder);
    this.emptyChildRoute.emit(this.updatedChildRoute);
    this.subscription.unsubscribe();
  }

  showDestinationModal() {
    this.sharedService.showOrHideAllModal(!this.displayModal);
  }

  addDestination() {
    this.isFormSubmitted=true;
    this.spinner=true;
    this.destinationForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    this.childRoute? this.destinations=this.childRoute:null;

    if (this.destinationForm.valid) {
      this.finalGeolocation.push(this.destinationForm.get('city')?.value.lat);
      this.finalGeolocation.push(this.destinationForm.get('city')?.value.lng);

      this.destinationForm.patchValue({
        dateStr: this.destinationForm.get('dateStr')?.value,
        loadRef: this.destinationForm.get('loadRef')?.value,
        city: this.destinationForm.get('city')?.value.name,
        countryName: this.destinationForm.get('countryName')?.value.name,
        countryCode: this.destinationForm.get('countryName')?.value.code,
        geolocation: this.finalGeolocation
      });

      console.log(this.destinationForm.value)

      this.destinations.push(this.destinationForm.value);
      this.finalGeolocation=[];
      this.messageService.add({ severity: "success", summary: "Service Message", detail: 'The road has been added' });
    }
    this.spinner=false;
  }

  updateDestination(route: IDestination) {
    this.isFormSubmitted=true;
    this.spinner=true;
    this.destinationForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    const routeIndex=this.childRoutes.indexOf(route);

    this.childRoutes? this.destinations=this.childRoutes:null;

    if (this.destinationForm.valid) {
      this.finalGeolocation.push(this.destinationForm.get('city')?.value.lat);
      this.finalGeolocation.push(this.destinationForm.get('city')?.value.lng);

      this.destinationForm.patchValue({
        dateStr: this.destinationForm.get('dateStr')?.value,
        loadRef: this.destinationForm.get('loadRef')?.value,
        city: this.destinationForm.get('city')?.value.name,
        countryName: this.destinationForm.get('countryName')?.value.name,
        countryCode: this.destinationForm.get('countryName')?.value.code,
        geolocation: this.finalGeolocation
      });

      if (this.childOrder||this.updatedChildRoute) {
        this.destinations.splice(routeIndex, 1);

        if (this.destinationForm.value.destinationType==='LOAD') {
          this.destinations.unshift(this.destinationForm.value);
        } else {
          this.destinations.push(this.destinationForm.value);
        }
      } else {
        this.destinations.push(this.destinationForm.value);
      }

      this.messageService.add({ severity: "success", summary: "Service Message", detail: 'The road has been modified' });
    }

    this.spinner=false;
  }

  displayDestination(): void {
    this.destinationForm.patchValue({
      address: this.childRoute.address,
      loadRef: this.childRoute.loadRef,
      city: this.childRoute.city,
      countryName: this.childRoute.countryName,
      destinationType: this.childRoute.destinationType,
      tonnage: this.childRoute.tonnage,
      zipcode: this.childRoute.zipcode,
      mpl: this.childRoute.mpl,
      dateStr: this.datePipe.transform(this.childRoute.dateStr, 'MM/dd/yyyy hh:mm')
    })
  }

  public getCountriesByEvent(param: any) {
    this.selectedCountryCode=param.code;
    this.sharedService.getCountries().subscribe({
      next: countries => {
        this.countries=countries;
      },
      error: error => this.errorMessage=error
    })
  }

  public getCountries() {
    this.spinner=true;
    this.sharedService.getCountries().subscribe({
      next: countries => {
        this.countries=countries;
        this.spinner=false;
      },
      error: error => this.errorMessage=error
    })
  }

  public getCities(param: any) {
    this.sharedService.getCities(this.selectedCountryCode, param.query).subscribe({
      next: cities => {
        this.dataCities=cities;
        this.cities=this.dataCities.cities;
      },
      error: error => this.errorMessage=error
    })
  }
}
