import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/core/services/shared/shared.service';
import { ConfirmationService, MenuItem, MessageService, PrimeNGConfig } from 'primeng/api';
import { CommercialService } from 'src/app/core/services/commercial/commercial.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-commercial-detail',
  templateUrl: './commercial-detail.component.html',
  styleUrls: ['./commercial-detail.component.scss']
})
export class CommercialDetailComponent implements OnInit, OnDestroy {

  visibleSidebar4: any;

  constructor(
    private sharedService: SharedService,
    private confirmationService: ConfirmationService,
    private commercialService: CommercialService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    private primengConfig: PrimeNGConfig,
  ) { }

  public panelOpenState=true;
  public isDuplicated=false;
  public componentActive=true;
  public displayModal!: boolean;
  public errorMessage!: string;
  public isActive!: boolean;
  public orderData!: any;
  public subscription: Subscription=new Subscription();

  items!: MenuItem[];

  @Input() public data: any;

  public destinations=[];

  public orderId: string=this.route.snapshot.paramMap.get('id')||'';

  ngOnInit(): void {
    this.getOrderById();
    this.primengConfig.ripple=true;
    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
    this.initItem();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.componentActive=false;
  }

  initItem() {
    this.items=[
      {
        visible: this.isActive, label: 'Duplicate', icon: 'pi pi-clone', command: () => {
          this.duplicate();
        }
      },
      {
        label: `${this.isActive? 'Deactivated':'Activated'}`, icon: `${this.isActive? 'pi pi-lock':'pi pi-lock-open'}`, command: () => {
          this.toggleActivate();
        }
      },
      {
        visible: this.isActive, label: 'Update', icon: 'pi pi-refresh', routerLink: ['/commercial/update', this.orderId]
      },
      { separator: true },
      {
        label: 'Delete', icon: 'pi pi-trash', command: () => {
          this.delete();
        }
      },
    ];
  }

  private getOrderById() {
    this.commercialService.getOrderById(this.orderId).subscribe({
      next: order => {
        this.data=order;
        this.destinations=order.route;
        this.isActive=this.data.isActive;
        this.initItem();
      },
      error: error => this.errorMessage=error
    })
  }

  showDestinationModal() {
    this.sharedService.showOrHideAllModal(this.displayModal);
  }

  toggleActivate() {
    this.confirmationService.confirm({
      message: "Are you sure that you want to proceed ?",
      header: "You will disable this command",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.commercialService.toggleActive(this.orderId).subscribe({
          next: order => {
            this.data=order;
            this.isActive=this.data.isActive;

            if (this.isActive) {
              this.messageService.add({ severity: "info", summary: "Service Message", detail: "The command has been deactivated" });
            } else {
              this.messageService.add({ severity: "success", summary: "Service Message", detail: "The command has been activated" });
            }

            this.initItem();
          },
          error: error => (
            this.errorMessage=error,
            this.messageService.add({ severity: "error", summary: "Service Message", detail: "Error when changing status" })
          )
        })
      },
      reject: () => {
        this.messageService.add({ severity: "error", summary: "Service Message", detail: "You have rejected" });
      }
    });
  }

  duplicate() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed ?',
      header: 'You will duplicate this command',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'The command has been duplicated and you will be redirected to the command list' });
        this.commercialService.duplicateOrder(this.orderId).subscribe({
          next: order => {
            this.orderData=order;
            this.isDuplicated=true;
            this.router.navigate(['commercial/detail', this.orderData.orderId]);
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  delete() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this record ?',
      header: 'Delete Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'The order has been deleted and you will be redirected to the list of orders' });
        this.commercialService.deleteOrder(this.orderId).subscribe({
          next: () => {
            this.router.navigate(['commercial/list']);
          },
          error: error => this.errorMessage=error
        })
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

}
