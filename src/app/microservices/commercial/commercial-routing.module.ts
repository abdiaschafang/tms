import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommercialCreateComponent } from './commercial-create/commercial-create.component';
import { CommercialDetailComponent } from './commercial-detail/commercial-detail.component';
import { CommercialListComponent } from './commercial-list/commercial-list.component';

const routes: Routes = [
  { path: 'list', component: CommercialListComponent },
  { path: 'create', component: CommercialCreateComponent },
  { path: 'update/:id', component: CommercialCreateComponent },
  { path: 'detail/:id', component: CommercialDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommercialRoutingModule { }
