import { NgModule } from '@angular/core';

import { CommercialRoutingModule } from "./commercial-routing.module";
import { CommercialListComponent } from './commercial-list/commercial-list.component';
import { CommercialCreateComponent } from './commercial-create/commercial-create.component';
import { CommercialDetailComponent } from './commercial-detail/commercial-detail.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { DestinationFormComponent } from './components/destination-form/destination-form.component';
import { ShareFormComponent } from './components/share-form/share-form.component';

@NgModule({
  declarations: [
    CommercialListComponent,
    CommercialCreateComponent,
    CommercialDetailComponent,
    DestinationFormComponent,
    ShareFormComponent
  ],
  imports: [
    CommercialRoutingModule,
    SharedModule,
  ]
})
export class CommercialModule { }
