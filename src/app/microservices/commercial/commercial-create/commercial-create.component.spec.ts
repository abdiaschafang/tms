import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialCreateComponent } from './commercial-create.component';

describe('CommercialCreateComponent', () => {
  let component: CommercialCreateComponent;
  let fixture: ComponentFixture<CommercialCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommercialCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
