import { Component, ElementRef, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { fromEvent, merge, Observable, Subscription } from 'rxjs';
import { FormGroup, Validators, FormBuilder, FormControlName } from '@angular/forms';
import { SharedService } from 'src/app/core/services/shared/shared.service';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';
import { CommercialService } from 'src/app/core/services/commercial/commercial.service';
import { destinationTableAnimation } from 'src/app/animations/animations';
import { AuthService } from '../../../core/services/auth/auth.service';
import { ConfirmationService, MessageService, PrimeNGConfig } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { IOrder } from 'src/app/core/models/order';
import { ManagementService } from 'src/app/core/services/management/management.service';

@Component({
  selector: 'app-commercial-create',
  templateUrl: './commercial-create.component.html',
  styleUrls: ['./commercial-create.component.scss'],
  animations: [
    destinationTableAnimation(),
  ],
})

export class CommercialCreateComponent implements OnInit, OnDestroy {

  public displayModal!: boolean;
  public spinner: boolean=false;
  public body={ "param": "", "status": "" };
  public recommendedVehicles: any[]=[];
  public subscription: Subscription=new Subscription();
  public order!: IOrder;
  public customers!: any;
  public goods!: any;
  public pallets!: any;
  public goodsData!: any;
  public palletsData!: any;
  public vehiclesData!: any;
  public customerData!: any;
  public chargementType: any;
  public destinations: any;
  public test!: any;
  public destinationTableState!: string;
  public authUser!: any;
  public objetTest: any;
  public vehicles: any;
  public parentRoute: any;
  public updatedParentRoute: any;
  public parentRoutes: any;
  public parentOrder: any;
  public data: any;

  public pageTitle!: string;
  public addOrderObject: any;

  public orderId!: string;
  selectedCountries1: string[]=[];
  public customer: string="CUSTOMER";


  public errorMessage!: string;
  public formErrors: { [key: string]: string }={};
  private validationMessages: { [key: string]: { [key: string]: string } }={
    customerRef: { required: 'Ce champ est obligatoire' },
    recommandedVehiclesIds: { required: 'Ce champ est obligatoire' },
    tonnage: { required: 'Ce champ est obligatoire' },
    typeGoodRef: { required: 'Ce champ est obligatoire' },
    price: { required: 'Ce champ est obligatoire' },
    mpl: { required: 'Ce champ est obligatoire' },
  } 
  public orderForm!: FormGroup;
  private globalGenericValidator!: GlobalGenericValidator;
  private isFormSubmitted!: boolean;

  @ViewChildren(FormControlName, { read: ElementRef }) inputElements!: ElementRef[];

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private commercialService: CommercialService,
    private authService: AuthService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private managementService: ManagementService,
    private route: ActivatedRoute,
    primengConfig: PrimeNGConfig,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.orderId=params.get('id')||'';
    });
    this.getSelectedOrder(this.orderId);

    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
    this.subscription.add(this.sharedService.currentMessage.subscribe(emitDestinations => this.destinations=emitDestinations));

    this.authUser=this.authService.getUser();
    this.sharedService.elementStateObservable$.
      subscribe((newState: string) => {
        this.destinationTableState=newState;
      });

    this.globalGenericValidator=new GlobalGenericValidator(this.validationMessages);
    this.orderForm=this.formBuilder.group({
      customerRef: [this.order?.customerRef, Validators.required],
      typeGoodRef: [this.order?.typeGoodRef, Validators.required],
      price: [this.order?.price, Validators.required],
      customerBillRef: [this.order?.customerBillRef],
      recommandedVehiclesIds: [this.order?.recommandedVehiclesIds, Validators.required],
      tonnage: [this.order?.tonnage, Validators.required],
      mpl: [this.order?.mpl, Validators.required],
      typePalletteRef: [this.order?.typePalletteRef],
      palletteQte: [this.order?.palletteQte],
      comment: [this.order?.comment],
    })
  }

  ngAfterViewInit() {
    const formControlBlurs: Observable<unknown>[]=this.inputElements
      .map((formCOntrolElemRef: ElementRef) => fromEvent(formCOntrolElemRef.nativeElement, 'blur'));
    merge(this.orderForm.valueChanges, ...formControlBlurs)
      .subscribe(() => {
        this.formErrors=this.globalGenericValidator.createErrorMessage(this.orderForm, this.isFormSubmitted);
      });
  }

  ngOnDestroy() {
    this.sharedService.changeMessage(this.destinations=[]);
    this.subscription.unsubscribe();
  }

  showDestinationModal() {
    this.sharedService.showOrHideAllModal(this.displayModal);
    this.parentRoute=this.destinations;
    this.parentRoutes=this.destinations;
  }

  addOrder() {
    this.spinner=true;

    this.getRecommendedVehicles();

    this.isFormSubmitted=true;
    this.orderForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    if (this.orderForm.valid) {
      if (this.orderForm.dirty) {
        this.addOrderObject={
          customerRef: this.orderForm.get('customerRef')?.value.tva,
          typeGoodRef: this.orderForm.get('typeGoodRef')?.value.name,
          customerBillRef: this.orderForm.get('customerBillRef')?.value,
          tonnage: this.orderForm.get('tonnage')?.value,
          price: this.orderForm.get('price')?.value,
          mpl: this.orderForm.get('mpl')?.value,
          palletteQte: this.orderForm.get('palletteQte')?.value,
          tmsUserCompagny: "MisterdD",
          typePalletteRef: this.orderForm.get('typePalletteRef')?.value.type,
          comment: this.orderForm.get('comment')?.value,
          recommandedVehiclesIds: this.recommendedVehicles,
          route: this.destinations,
          users: [
            "admin"
          ]
        } 

        this.commercialService.createOrder(this.addOrderObject).subscribe({
          next: () => {
            this.spinner=false;
            this.messageService.add({ severity: "success", summary: "Service Message", detail: 'The order has been created and you will be redirected to the list of orders' });
            this.router.navigateByUrl('commercial/list');
          },
          error: error => (
            // TODO Get error for http response
            this.errorMessage=error,
            this.spinner=false,
            this.messageService.add({
              severity: "error", summary: "Service Message", detail: `
              Oops reassure you to have informed in the order:

              A loading point first
              The other loading points
              The different unloading points
              An unloading point last
            `})
          )
        })
      }
    }
  }

  updateOrder() {
    this.vehicles=[]
    this.getRecommendedVehicles();
    this.isFormSubmitted=true;

    this.orderForm.updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });

    if (this.orderForm.valid) {
      this.addOrderObject={
        customerRef: this.orderForm.get('customerRef')?.value.tva,
        typeGoodRef: this.orderForm.get('typeGoodRef')?.value.name,
        customerBillRef: this.orderForm.get('customerBillRef')?.value,
        tonnage: this.orderForm.get('tonnage')?.value,
        price: this.orderForm.get('price')?.value,
        mpl: this.orderForm.get('mpl')?.value,
        palletteQte: this.orderForm.get('palletteQte')?.value,
        tmsUserCompagny: "LGL",
        tmsUserId: "LISS",
        typePalletteRef: this.orderForm.get('typePalletteRef')?.value.type,
        status: this.order.status,
        comment: this.orderForm.get('comment')?.value,
        recommandedVehiclesIds: this.recommendedVehicles,
        route: this.destinations,
        users: [
          "admin"
        ]
      }
      this.commercialService.updateOrder(this.addOrderObject, this.orderId).subscribe({
        next: () => {
          this.messageService.add({ severity: "success", summary: "Service Message", detail: "The order has been modified" });
          this.router.navigateByUrl('commercial/list');
        },
        error: error => (
          // TODO Get error for http response
          this.errorMessage=error,
          this.spinner=false,
          this.messageService.add({ severity: "error", summary: "Service Message", detail: "Error when modifying the order" })
        )
      });

      this.commercialService.updateRoute(this.destinations, this.orderId).subscribe();
    }
  }

  updateRoute(route: any) {
    this.parentRoute=route;
    this.updatedParentRoute=route;
    this.parentOrder=this.order;
    this.parentRoutes=this.destinations;
    this.sharedService.showOrHideAllModal(this.displayModal);
  }

  getRecommendedVehicles() {
    for (let key in this.orderForm.get('recommandedVehiclesIds')?.value) {
      this.recommendedVehicles.push(this.orderForm.get('recommandedVehiclesIds')?.value[key].name);
    }
  }

  removeDestination(route: any) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed ?',
      header: 'You will delete this route',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const index=this.destinations.indexOf(route, 0);
        (index>-1)? this.destinations.splice(index, 1):null;
        this.messageService.add({ severity: 'success', summary: 'Service Message', detail: 'The road was well removed' });
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'You have rejected' });
      }
    });
  }

  getSelectedOrder(orderId: string): void {
    if (this.orderId) {
      this.commercialService.getOrderById(orderId).subscribe({
        next: order => {
          this.displayOrder(order);
          this.destinations=order.route;
        }
      })
    }
  }

  getCustomers(event: any) {
    this.body.param=event.query||"";
    this.body.status="ACTIVATED";

    if (this.body.param.length>=3) {
      this.managementService.getAllCustomer(this.body).subscribe({
        next: customers => {
          this.customerData=customers;
          this.customers=this.customerData.customers;
        }
      });
    }
  }

  getGoods(event: any) {
    this.body.param=event.query||"";
    this.body.status="ACTIVATED";
    this.managementService.getAllGoods(this.body).subscribe({
      next: goods => {
        this.goodsData=goods;
        this.goods=this.goodsData.goods;
      }
    });
  }

  getVehicle(event: any) {
    this.body.param=event.query||"";
    this.body.status="ACTIVATED";
    this.managementService.getAllTruck(this.body).subscribe({
      next: vehicles => {
        this.vehiclesData=vehicles;
        this.vehicles=this.vehiclesData.trucks;
      }
    });
  }

  getPallets(event: any) {
    this.body.param=event.query||"";
    this.body.status="ACTIVATED";
    this.managementService.getAllPallets(this.body).subscribe({
      next: pallets => {
        this.palletsData=pallets;
        this.pallets=this.palletsData.pallets;
      }
    });
  }

  displayOrder(order: IOrder): void {
    this.order=order;

    this.orderForm.patchValue({
      customerRef: this.order.customerRef,
      typeGoodRef: this.order.typeGoodRef,
      customerBillRef: this.order.customerBillRef,
      tonnage: this.order.tonnage,
      price: this.order.price,
      mpl: this.order.mpl,
      palletteQte: this.order.palletteQte,
      typePalletteRef: this.order.typePalletteRef,
      comment: this.order.comment,
      recommandedVehiclesIds: this.order.recommandedVehiclesIds,
    })
  }
}
