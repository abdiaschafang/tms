import { NgModule } from '@angular/core';
import { TransportIndexComponent } from './transport-index/transport-index.component';
import { TransportRoutingModule } from './transport-routing.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { TransportDetailComponent } from './transport-detail/transport-detail.component';
import { TransportUpdateComponent } from './transport-update/transport-update.component';

@NgModule({
  declarations: [
    TransportIndexComponent,
    TransportDetailComponent,
    TransportUpdateComponent
  ],
  imports: [
    TransportRoutingModule,
    SharedModule
  ],
  exports: [
    SharedModule
  ]
})
export class TransportModule { }
