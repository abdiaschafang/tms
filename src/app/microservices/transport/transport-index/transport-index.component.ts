import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Observable, Subscription } from 'rxjs';
import { ITransport } from 'src/app/core/models/transport';
import { TransportService } from 'src/app/core/services/transport/transport.service';
import { SharedService } from 'src/app/core/services/shared/shared.service';

@Component({
  selector: 'app-transport-index',
  templateUrl: './transport-index.component.html',
  styleUrls: ['./transport-index.component.scss']
})

export class TransportIndexComponent implements OnInit, OnDestroy {

  public data: any;
  public searchForm!: FormGroup;
  public spinner: boolean=false;
  public loader: boolean=true;
  public errorMessage!: string;
  public status!: any[];
  public statusSearchResult!: string;
  public currentDate: any;
  public code!: string;
  // public referenceOrders!: string;
  // public referenceCarriers!: string;
  // public referenceDrivers!: string;
  // public referenceVehicle!: string;
  public statusE!: any;
  public storedPayload!: any;
  public search!: string;
  public transports=[];
    clicked: any=true;
  clicked1: any=true;
  clicked2: any=true;
  clicked3: any=true;
  clicked4: any=true;
  clicked5: any=true;
  clicked6: any=true;
  clicked7: any=true;
  compteur: any=0;

  public obs!: Observable<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  public subscription: Subscription=new Subscription();

  constructor(
    private messageService: MessageService,
    private transportService: TransportService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private confirmationService: ConfirmationService,
    private sharedService: SharedService 
  ) {
    this.status=[{ name: 'CREATED' }, { name: 'LOADED' }, { name: 'DELIVERED' }, { name: 'CANCEL' }];
  }

  ngOnInit(): void { 
    this.currentDate=this.datePipe.transform(Date(), 'yyyy-MM-dd');
    this.searchForm=this.formBuilder.group({
      startDate: [this.currentDate],
      endDate: [this.currentDate],
      code: [''],
      refOrd: [''],
      refCar: [''],
      refDri: [''],
      refVeh: [''],
      status: ['CREATED']
    })

    this.getTransports();
    this.displayDate();
    this.storedPayload.startDate;
    this.storedPayload.endDate;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  displayDate(): void {
    this.searchForm.patchValue({
      startDate: this.datePipe.transform(this.searchForm.get('startDate')?.value, this.currentDate),
      endDate: this.datePipe.transform(this.searchForm.get('endDate')?.value, this.currentDate)
    })
  }

  setTransportStatus(param: any, status: string) {
    this.confirmationService.confirm({
      message: "Are you sure that you want to proceed ?",
      header: "You will Change the status",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.transportService.setTransportStatus(param.transportId, status).subscribe({
          next: () => {
            this.getTransports();
          }
        });
      },
      reject: () => {
        this.messageService.add({ severity: "error", summary: "Service Message", detail: "You have rejected" });
      }
    });
  }

  getTransports() {
    // this.searchForm.patchValue({
    //   startDate: this.datePipe.transform(this.searchForm.get('startDate')?.value, 'yyyy-MM-dd'),
    //   endDate: this.datePipe.transform(this.searchForm.get('endDate')?.value, 'yyyy-MM-dd')
    // });

    let payload={
      code: this.searchForm.get('code')?.value||'',
      orderRef: this.searchForm.get('refOrd')?.value||'',
      carrierRef: this.searchForm.get('refCar')?.value||'',
      driverRef: this.searchForm.get('refDri')?.value||'',
      vehicleRef: this.searchForm.get('refVeh')?.value||'',
      startDate: this.datePipe.transform(this.searchForm.get('startDate')?.value, 'yyyy-MM-dd'),
      endDate: this.datePipe.transform(this.searchForm.get('endDate')?.value, 'yyyy-MM-dd'),
      status:this.searchForm.get('status')?.value,
    }
    this.storedPayload=payload;
    console.log(this.storedPayload);
    this.transportService.getTransports(payload).subscribe({
      next: transports => {
        this.data=transports;
        this.transports=this.data.content;
        const dataSource: MatTableDataSource<ITransport>=new MatTableDataSource<ITransport>(this.transports);
        dataSource.paginator=this.paginator;
        this.obs=dataSource.connect();
        this.spinner=false;
      },
      error: error => {
        this.errorMessage=error,
          this.sharedService.toastMessageTransports(this.transports)
      }
    })
  }

  loaded() {
    this.messageService.add({ severity: 'info', summary: 'Statue change to', detail: 'ok' });
  }

  inprogress() {
    this.messageService.add({ severity: 'info', summary: 'Statue change to', detail: 'ok' });
  }

  delivered() {
    this.messageService.add({ severity: 'info', summary: 'Statue change to', detail: 'ok' });
  }

  cancel() {
    this.messageService.add({ severity: 'info', summary: 'Statue change to', detail: 'ok' });
  }

  downloadOt() {
    this.messageService.add({ severity: 'info', summary: 'Downloading OT', detail: 'N°54782145' });
  }

  setSearch(input: any) {
    console.log(input);
    switch (input) {
      case '1': {
        this.clicked=false;
        this.storedPayload.code='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }

      case '2': {
        this.clicked1=false;
        this.storedPayload.orderRef='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '3': {
        this.clicked2=false;
        this.storedPayload.carrierRef='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '4': {
        this.clicked3=false;
          this.storedPayload.driverRef='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '5': {
        this.clicked4=false;
        this.storedPayload.vehicleRef='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '6': {
        this.clicked5=false;
        this.storedPayload.startDate='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }

      case '7': {
        this.clicked6=false;
        this.storedPayload.endDate='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      case '8': {
        this.clicked7=false;
        this.storedPayload.status='';
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
      default: {
        this.sendSearchInBackend(this.storedPayload);
        break;
      }
    }
    console.log(this.storedPayload);
  }

  sendSearchInBackend(formValue:any) {
        this.transportService.getTransports(formValue).subscribe({
      next: transports => {
        this.data=transports;
        this.transports=this.data.content;
        const dataSource: MatTableDataSource<ITransport>=new MatTableDataSource<ITransport>(this.transports);
        dataSource.paginator=this.paginator;
        this.obs=dataSource.connect();
        this.spinner=false;
      },
      error: error => {
        this.errorMessage=error,
          this.sharedService.toastMessageTransports(this.transports)
      }
    })
  }

  initialize() {
     this.compteur++;
    if (this.compteur>1) {
    this.storedPayload.code='';
    this.storedPayload.orderRef='';
    this.storedPayload.carrierRef='';
    this.storedPayload.driverRef='';
    this.storedPayload.startDate=this.currentDate;
    this.storedPayload.endDate=this.currentDate;
    this.storedPayload.status='CREATED';
      this.storedPayload.vehicleRef='';
      this.clicked=true;
      this.clicked1=true;
      this.clicked2=true;
      this.clicked3=true;
      this.clicked4=true;
      this.clicked5=true;
      this.clicked6=true;
      this.clicked7=true;
    }
  }

}
