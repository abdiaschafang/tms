import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { TransportService } from 'src/app/core/services/transport/transport.service';
import { SharedService } from 'src/app/core/services/shared/shared.service';

@Component({
  selector: 'app-transport-detail',
  templateUrl: './transport-detail.component.html',
  styleUrls: ['./transport-detail.component.scss']
})
export class TransportDetailComponent implements OnInit {

  public status!: any[];
  public transport: any;
  public displayModal!: boolean;
  public loader: boolean=true;
  public errorMessage!: string;
  public code=this.route.snapshot.paramMap.get('id')||'';
  public transportOrder: any;

  public subscription: Subscription=new Subscription();

  public parentTransport!: any;

  constructor(
    private messageService: MessageService,
    private transportService: TransportService,
    private route: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationService,
    private sharedService: SharedService
  ) {
    this.status=[{ name: 'LOADED' }, { name: 'IN PROGRESS' }, { name: 'DELIVERED' }, { name: 'CANCEL' }];
  }

  ngOnInit(): void {
    this.getTransport();

    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
  }

  showDestinationModal() {
    this.sharedService.showOrHideAllModal(this.displayModal);
  }

  getTransport() {
    this.transportService.getTransportByCode(this.code).subscribe({
      next: transport => {
        this.transport=transport;
        this.parentTransport=transport;
        this.getOrderByOrderId();
      }
    });
  }

  getOrderByOrderId() {
    this.transportService.getOrderByOrderId(this.transport.orderRef).subscribe({
      next: transportOrder => {
        this.transportOrder=transportOrder;
      }
    });
  }

  decommissionTransport() {
    this.confirmationService.confirm({
      message: "Are you sure that you want to proceed ?",
      header: "You will unassign this command",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.transportService.decommissionTransport(this.transport.transportId).subscribe({
          next: () => {

            this.messageService.add({ severity: 'info', summary: 'Service Message', detail: 'The transport has been decommissioned' });
            this.router.navigateByUrl("transport/index");
          },
          error: error => {
            this.errorMessage=error,
              this.messageService.add({ severity: 'error', summary: 'Service Message', detail: 'An error occurred during the deallocation of transportation' });
          }
        })
      },
      reject: () => {
        this.messageService.add({ severity: "error", summary: "Service Message", detail: "You have rejected" });
      }
    })
  }

}
