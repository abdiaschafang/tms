import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransportDetailComponent } from './transport-detail/transport-detail.component';
import { TransportIndexComponent } from './transport-index/transport-index.component';

const routes: Routes=[
  { path: 'index', component: TransportIndexComponent },
  { path: 'detail/:id', component: TransportDetailComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransportRoutingModule { }
