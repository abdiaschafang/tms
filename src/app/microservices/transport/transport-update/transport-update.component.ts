import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { TransportService } from 'src/app/core/services/transport/transport.service';
import { SharedService } from 'src/app/core/services/shared/shared.service';
import { GlobalGenericValidator } from 'src/app/core/validators/global-generic.validator';

@Component({
  selector: 'app-transport-update',
  templateUrl: './transport-update.component.html',
  styleUrls: ['./transport-update.component.scss']
})
export class TransportUpdateComponent implements OnInit {

  public displayModal!: boolean;
  public transportForm!: FormGroup;
  private globalGenericValidator!: GlobalGenericValidator;
  public errorMessage!: string;
  public subscription: Subscription=new Subscription();
  public formErrors: { [key: string]: string }={};
  private validationMessages: { [key: string]: { [key: string]: string } }={
    orderRef: { required: 'Ce champ est obligatoire' },
  }

  @Input() childTransport!: any;

  constructor(
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private sharedService: SharedService,
    private transportService: TransportService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.displayTransport();

    this.subscription.add(this.sharedService.Modal.subscribe(displayModal => this.displayModal=displayModal));
  }

  initForm() {
    this.globalGenericValidator=new GlobalGenericValidator(this.validationMessages);
    this.transportForm=this.formBuilder.group({
      orderRef: ['', Validators.required],
      vehicleRef: ['', Validators.required],
      driverRef: ['', Validators.required],
      carrierRef: ['', Validators.required],
      comment: ['', Validators.required],
      userCompany: [this.childTransport?.userCompany, Validators.required],
      price: ['', Validators.required],
      status: [this.childTransport?.status, Validators.required],
      loadDate: [this.childTransport?.loadDate, Validators.required]
    })
  }

  showDestinationModal() {
    this.sharedService.showOrHideAllModal(!this.displayModal);
  }

  updateTransport() {
    this.transportService.updateTransport(this.childTransport.transportId, this.transportForm.value).subscribe(({
      next: transport => {
        console.log(transport)
      }
    }))
  }

  displayTransport() {
    this.transportForm.patchValue({
      vehicleRef: this.childTransport.vehicleRef,
      driverRef: this.childTransport.vehicleRef,
      carrierRef: this.childTransport.vehicleRef,
      comment: this.childTransport.comment,
      price: this.childTransport.price,
    })
  }

}
