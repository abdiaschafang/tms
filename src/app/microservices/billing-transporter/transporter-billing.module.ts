import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransporterBillingRoutingModule } from './transporter-billing-routing.module';
import { TransporterBillingListComponent } from './transporter-billing-list/transporter-billing-list.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { TransporterBillingAddComponent } from './transporter-billing-add/transporter-billing-add.component';


@NgModule({
  declarations: [
    TransporterBillingListComponent,
    TransporterBillingAddComponent

  ],
  imports: [
    CommonModule,
    TransporterBillingRoutingModule,
    SharedModule
  ]
})
export class TransporterBillingModule { }
