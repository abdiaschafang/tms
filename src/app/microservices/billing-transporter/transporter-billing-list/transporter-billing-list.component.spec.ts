import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransporterBillingListComponent } from './transporter-billing-list.component';

describe('TransporterBillingListComponent', () => {
  let component: TransporterBillingListComponent;
  let fixture: ComponentFixture<TransporterBillingListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransporterBillingListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransporterBillingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
