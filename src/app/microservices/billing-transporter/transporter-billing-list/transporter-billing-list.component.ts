import { Component, OnInit } from '@angular/core'; 
import { FormBuilder, FormGroup } from '@angular/forms';
import { BillingService } from 'src/app/core/services/billing/billing.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-transporter-billing-list',
  templateUrl: './transporter-billing-list.component.html',
  styleUrls: ['./transporter-billing-list.component.scss']
})
export class TransporterBillingListComponent implements OnInit {
  carrierBillLists: any;
  public searchForm!: FormGroup;
  public currentDate: any; 
  public status!: any[];
  public invoiceType!: any[];
  constructor(private billing: BillingService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,) {
      this.invoiceType=[{ name: 'OPEN' }, { name: 'PAY' }];
    this.status=[{ name: 'INVOICE' }, { name: 'DISCOUNT' }];
     }

  ngOnInit(): void {
    this.currentDate=this.datePipe.transform(Date(), 'yyyy-MM-dd');
    this.searchForm=this.formBuilder.group({
      param: [''],
      status: ['OPEN'],
      invoiceType: ['INVOICE'],
      startDate: [this.currentDate],
      endDate: [this.currentDate],
    })


    this.searchCarrierBill();
  }


  searchCarrierBill(){
    let billing : any;
    this.billing.getAllCarrierBill().subscribe({
      next: allbill => {
        billing = allbill;
        this.carrierBillLists = billing
      }
    });
  }

}
