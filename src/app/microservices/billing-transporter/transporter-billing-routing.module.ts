import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransporterBillingAddComponent } from './transporter-billing-add/transporter-billing-add.component';
import { TransporterBillingListComponent } from './transporter-billing-list/transporter-billing-list.component';

const routes: Routes = [
  { path: 'list', component: TransporterBillingListComponent },
  { path: 'transporterBillingAdd', component: TransporterBillingAddComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransporterBillingRoutingModule { }
