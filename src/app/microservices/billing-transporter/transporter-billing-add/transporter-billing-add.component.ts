import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BillingService } from 'src/app/core/services/billing/billing.service';
import { DatePipe } from '@angular/common';
import { ManagementService } from 'src/app/core/services/management/management.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { TransportService } from 'src/app/core/services/transport/transport.service';

@Component({
  selector: 'app-transporter-billing-add',
  templateUrl: './transporter-billing-add.component.html',
  styleUrls: ['./transporter-billing-add.component.scss']
})
export class TransporterBillingAddComponent implements OnInit {
  public body = { "param": "", "status": "" };
  public carriers ! : any;
  public noticeInvoicesData ! : any;
  public carriersData ! : any;
  public carriersBillingInfos!: any;
  public searchCarrierForm!: FormGroup;
  public carrierSaveBillForm!: FormGroup;
  public errorMessage!: string;
  public carrierInfoForm!: FormGroup;
  public currentDate: any;
 public  currentDateDay: any;
 public paymentsDaysPeriod = 0;
 public transports: any;
 public addBillingForm!: FormGroup;
 dueDate: any;
 amount = 0;
 checked1: boolean = false;
 status:any;
//  discount : any;transportDiscounts
public AllSelectedTransportCode: any[] = [];
public AllSelectedTransportPrice: any[] = [];
public AllSelectedTransportDiscounts: any[] = [];
public transportsList: any;
public selectedTransports!: [];
 public allStatus!: any[];
 public dateLine!: any[];
 public spinner: boolean=false;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    carriersRef: { required: 'Ce champ est obligatoire' },
    vat: { required: 'Ce champ est obligatoire' },
    description: { required: 'Ce champ est obligatoire' },
  }
  name: any;
  iban: any;
  bic: any;
  dateLineDevfauld: any;
  carrierVat: any;
  trans: any;

  constructor(
    private billing: BillingService,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private messageService: MessageService,
    private transportService: TransportService,
    private router: Router 
  ) { }

  ngOnInit(): void {
    this.status ='OPEN';
    this.dateLineDevfauld= '60 jours';
    this.currentDate = this.datePipe.transform(Date(), 'dd/MM/yyyy');
    this.currentDateDay = new Date();
    this.searchCarrierForm = this.formBuilder.group({
      carriersRef: ['']
    })

    this.carrierSaveBillForm = this.formBuilder.group({
      numOt: [''],
      carrierName: [''],
      selectedOt: [],
      numBill: [''],
      status: [this.status],
      amount: [''],
      discount: [''],
      amountEdited: [''],
      deadlines: [this.dateLineDevfauld],
      dateIssued: [this.currentDateDay],
      receptionDate: [this.currentDateDay],
      dueDate: [''],
      bic: [''],
      iban: [''],
      checked: [''],
      informations: [''],
      transportdiscout: [''],

    })

    this.carrierInfoForm = this.formBuilder.group({
      vat: [''],
      facturationDate: [this.currentDateDay],
      dueDate: [''],
      description: [''],
      comment: [''],
    })

    this.addBillingForm = this.formBuilder.group({
      comment: [''],
      description: [''],
      carrierId: [''],
      carrierName: [''],
      carrierVat: [''],
      carrierCode:[''],
      carrierAddress: [''],
      name:[],
      vatValue: [''],
      transportCodes: ["string"],
      transportPrices: [],
      transportDiscounts:[],
      bic: [],
      iban: [],
      receiveDate: [],
    })

    this.allStatus=[
      { id: "OPEN", name: "OPEN" },
      { id: "BLOCKED", name: "BLOCKED" },
    ]
    this.dateLine=[
      { id: "60", name: "60 jours" },
      { id: "45", name: "45 jours" },
    ]
  }



  getCarrierBillInformation(event: any) {
    let date = new Date(); 
    this.paymentsDaysPeriod = event.paymentPeriod;
    this.name = event.name;
    this.iban = event.bankingInfos[0].iban;
    this.bic = event.bankingInfos[0].bic;

    date.setDate(date.getDate() + this.paymentsDaysPeriod)
    this.carrierSaveBillForm.patchValue({
      vat :event.tva,
      dueDate: date,
    })

    this.billing.getCarrierBillInformation(event.tva).subscribe({
      next: billingInfos => {
        this.transports = billingInfos
        this.transportsList = this.transports
      }
    });
  }

  getCarrier(event: any) {
    this.body.param = event.query || "";
    this.body.status = "ACTIVATED";

    if (this.body.param.length >= 3) {
      this.managementService.getAllTransporter(this.body).subscribe({
        next: carriers => {
          this.carriersData = carriers;
          this.carriers = this.carriersData.transporters;
        }
      });
    }
  }

  getOtCodes(event: any) {
      this.transportService.getNotInvoiceByCode(event.target.value).subscribe({
        next: noticeInvoiceinfos => {
          this.noticeInvoicesData = noticeInvoiceinfos;
          this.transports = this.noticeInvoicesData.transports;
          for(this.trans of this.transports!){
            this.trans.transportdiscout = 0;
          }
          this.setCarrierSaveBillForm();
        }
      });
  }

  setCarrierSaveBillForm(){
    let date = new Date(); 
    this.paymentsDaysPeriod = this.noticeInvoicesData.paymentPeriod;
    this.carrierVat = this.noticeInvoicesData.carrierVat
    date.setDate(date.getDate() + this.paymentsDaysPeriod)

    this.carrierSaveBillForm.patchValue({
      carrierName:this.noticeInvoicesData.carrierName,
      selectedOt: this.noticeInvoicesData.code,
      amount: this.noticeInvoicesData.amount,
      dueDate: date,
      bic:this.noticeInvoicesData.bankingInformations[0].bic,
      iban:this.noticeInvoicesData.bankingInformations[0].iban,
    })
  }

  addDate() {
    let date = new Date(this.carrierSaveBillForm.get('receptionDate')?.value); 
    this.paymentsDaysPeriod = this.noticeInvoicesData.paymentPeriod;
    date.setDate(date.getDate() + this.paymentsDaysPeriod)
    this.carrierSaveBillForm.patchValue({
      dueDate: date,
    })
  }


  saveInvoce() {
     let transport: any
    for(transport of this.selectedTransports){
      this.AllSelectedTransportCode.push(transport.code) ;
      this.AllSelectedTransportPrice.push(transport.price) ;
      this.AllSelectedTransportDiscounts.push(transport.transportdiscout)
      this.amount +=transport.price ; 
    }

    this.carrierSaveBillForm.patchValue({
      selectedOt:this.AllSelectedTransportCode,
      checked: this.checked1,
      amount: this.carrierSaveBillForm.get('amount')?.value + this.amount 
    })
    console.log(this.carrierSaveBillForm.value)
   
    // this.selectedTransports = [];

    let carrierBillSaveObject = {

      paymentDate: this.carrierSaveBillForm.get('dueDate')?.value,
      comment: this.carrierSaveBillForm.get('informations')?.value,
      status: this.carrierSaveBillForm.get('status')?.value ,
      dilemma: false ,
      carrierCode: this.noticeInvoicesData.code ,
      carrierName: this.carrierSaveBillForm.get('carrierName')?.value,
      carrierVat: this.carrierVat,
      amount: this.carrierSaveBillForm.get('amount')?.value,
      paymentPeriod: this.paymentsDaysPeriod,
      dueDate: this.carrierSaveBillForm.get('dueDate')?.value ,
      receiveDate: this.carrierSaveBillForm.get('receptionDate')?.value ,
      iban:this.carrierSaveBillForm.get('iban')?.value ,
      bic: this.carrierSaveBillForm.get('bic')?.value,
      transportCodes: this.AllSelectedTransportCode,
      transportPrices: this.AllSelectedTransportPrice,
      transportDiscounts: this.AllSelectedTransportDiscounts,
    }

    console.log(carrierBillSaveObject);
    // this.router.navigateByUrl('billing/list');

    this.billing.saveTransporterBill(carrierBillSaveObject).subscribe({
      next: billing => {
        console.log(billing);
        this.messageService.add({ severity: "success", summary: "Service Message", detail: 'The Bill has been succesful created ' });
      },
      error: error => this.errorMessage=error
    })

    this.AllSelectedTransportCode = [];
    this.AllSelectedTransportPrice = [];
    this.AllSelectedTransportDiscounts = [];
    // this.selectedTransports = [];
  }

}
