import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransporterBillingAddComponent } from './transporter-billing-add.component';

describe('TransporterBillingAddComponent', () => {
  let component: TransporterBillingAddComponent;
  let fixture: ComponentFixture<TransporterBillingAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransporterBillingAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransporterBillingAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
