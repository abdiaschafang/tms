import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DatePipe } from '@angular/common';
import { BillingService } from 'src/app/core/services/billing/billing.service';

@Component({
  selector: 'app-billing-list',
  templateUrl: './billing-list.component.html',
  styleUrls: ['./billing-list.component.scss']
})
export class BillingListComponent implements OnInit {

  public searchForm!: FormGroup;
  public status!: any[];
  public invoiceType!: any[];
  public currentDate: any; 
  billLists: any;

  constructor(
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private billing: BillingService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.invoiceType=[{ name: 'OPEN' }, { name: 'PAY' }];
    this.status=[{ name: 'INVOICE' }, { name: 'DISCOUNT' }];
  }
 
  ngOnInit(): void {
    this.currentDate=this.datePipe.transform(Date(), 'yyyy-MM-dd');
    this.searchForm=this.formBuilder.group({
      param: [''],
      status: ['OPEN'],
      invoiceType: ['INVOICE'],
      startDate: [this.currentDate],
      endDate: [this.currentDate],
    })

    this. searchBill();
  }

  searchBill(){
    let billing : any;
    this.billing.getAllBill(this.searchForm.value).subscribe({
      next: allbill => {
        billing = allbill;
        this.billLists = billing.data
        console.log(this.billLists);  
      }
    });
  }

  openAddInvoice(){

  }

}
