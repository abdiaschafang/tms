import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BillingService } from 'src/app/core/services/billing/billing.service';
import { ManagementService } from 'src/app/core/services/management/management.service';
import { DatePipe } from '@angular/common';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-invoice',
  templateUrl: './add-invoice.component.html',
  styleUrls: ['./add-invoice.component.scss']
})
export class AddInvoiceComponent implements OnInit {
  public body = { "param": "", "status": "" };
  disabled: boolean = true;
  public searchCustomerForm!: FormGroup;
  public currentDate: any;
 public  currentDateDay: any;
  public customerInfoForm!: FormGroup;
  public addBillingForm!: FormGroup;
  public errorMessage!: string;
  public customers!: any;
  public billingInfos!: any;
  public transports: any;
  public paymentsDaysPeriod = 0;
  amount = 0;
  transportsList: any;
  AllSelectedTransportCode: any[] = [];
  AllSelectedTransportPrice: any[] = [];
  dueDate: any
  facturationDate : any;
  selectedTransports!: [];
  public customerData!: any;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    customerRef: { required: 'Ce champ est obligatoire' },
    vat: { required: 'Ce champ est obligatoire' },
    description: { required: 'Ce champ est obligatoire' },
  }
  
  constructor(
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private billing: BillingService,
    private datePipe: DatePipe,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private router: Router 
  ) { }

  ngOnInit(): void {
    this.currentDate = this.datePipe.transform(Date(), 'dd/MM/yyyy');
    this.currentDateDay = new Date();
    this.searchCustomerForm = this.formBuilder.group({
      customerRef: ['']
    })

    this.customerInfoForm = this.formBuilder.group({
      vat: [''],
      facturationDate: [this.currentDateDay],
      dueDate: [''],
      description: [''],
      comment: [''],
    }) 

    this.addBillingForm = this.formBuilder.group({
      comment: [''],
      description: [''],
      customerId: [''],
      customerName: [''],
      customerVat: [''],
      customerAdress: [''],
      amount: [''],
      billDate: [''],
      dueDate: [''],
      vatValue: [''],
      transportCodes: ["string"],
      transportPrices: []
    })
  }

  addDate() {
    let date = new Date(this.customerInfoForm.get('facturationDate')?.value);
    date.setDate(date.getDate() + this.paymentsDaysPeriod)
    this.customerInfoForm.patchValue({
      dueDate: date,
    })
  }

  getCustomers(event: any) {
    this.body.param = event.query || "";
    this.body.status = "ACTIVATED";

    if (this.body.param.length >= 3) {
      this.managementService.getAllCustomer(this.body).subscribe({
        next: customers => {
          this.customerData = customers;
          this.customers = this.customerData.customers;
        }
      });
    }
  }

  getCustomerBillInformation(event: any) {

    let date = new Date(); 
    this.paymentsDaysPeriod = event.paymentsDaysPeriod;
    date.setDate(date.getDate() + this.paymentsDaysPeriod)
    this.customerInfoForm.patchValue({
      vat :event.tva,
      dueDate: date,
    })

    this.billing.getBillingCustomerInformations(event.tva).subscribe({
      next: billingInfos => {
        this.transports = billingInfos
        this.transportsList = this.transports.transports
      }
    });
  }

  
  saveInvoce() {
    console.log('this.customerInfoForm.value');
    console.log(this.customerInfoForm.value)
    let transport: any
    for(transport of this.selectedTransports){
      this.AllSelectedTransportCode.push(transport.transportCode) ;
      this.AllSelectedTransportPrice.push(transport.amount) ;
      this.amount +=transport.amount ; 
    }
    this.selectedTransports = [];
    this.addBillingForm.patchValue({
      comment: this.customerInfoForm.get('comment')?.value,
      description: this.customerInfoForm.get('description')?.value,
      customerId: this.customers[0].id,
      customerName: this.customers[0].name,
      customerAdress: this.customers[0].address,
      customerVat: this.customers[0].tva,
      billDate: new Date(this.customerInfoForm.get('facturationDate')?.value),
      amount: this.amount,
      dueDate: new Date(this.dueDate),
      vatValue: this.customers[0].valueTva,
      transportCodes: this.AllSelectedTransportCode,
      transportPrices: this.AllSelectedTransportPrice
    })

    console.log('this.addBillingForm.value');
    console.log(this.addBillingForm.value);
    this.billing.saveBill(this.addBillingForm.value).subscribe({
      next: billing => {
        console.log(billing);
        this.messageService.add({ severity: "success", summary: "Service Message", detail: 'The Bill has been succesful created ' });
        this.router.navigateByUrl('billing/list');
      },
      error: error => this.errorMessage=error
    })

    this.AllSelectedTransportCode = [];
    this.AllSelectedTransportPrice = [];

  }

}
