import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillingRoutingModule } from './billing-routing.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { CreateBillingComponent } from './create-billing/create-billing.component';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { CreditNoteComponent } from './credit-note/credit-note.component';


@NgModule({
  declarations: [
    CreateBillingComponent,
    AddInvoiceComponent,
    CreditNoteComponent 
  ],
  imports: [
    CommonModule,
    BillingRoutingModule,
    SharedModule
  ]
})
export class BillingModule { }
