import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { BillingListComponent } from './billing-list/billing-list.component';
import { CreateBillingComponent } from './create-billing/create-billing.component';
import { CreditNoteComponent } from './credit-note/credit-note.component';

const routes: Routes = [
  { path: 'list', component: BillingListComponent },
  { path: 'create-billing', component: CreateBillingComponent },
  { path: 'add-invoice', component: AddInvoiceComponent },
  { path: 'add-credit-note', component: CreditNoteComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingRoutingModule { }
