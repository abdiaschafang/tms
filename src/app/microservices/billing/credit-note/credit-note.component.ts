import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ManagementService } from 'src/app/core/services/management/management.service';
import { BillingService } from 'src/app/core/services/billing/billing.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-credit-note',
  templateUrl: './credit-note.component.html',
  styleUrls: ['./credit-note.component.scss']
})
export class CreditNoteComponent implements OnInit {
  public body = { "param": "", "status": "" };
  public backToReadyForBill: boolean = false; 
  public customers!: any;
  public customerData!: any;
  public currentDate: any;
  public searchCustomerForm!: FormGroup;
  public customerInfoForm!: FormGroup;
  public addBillingForm!: FormGroup;
  public billingInfos!: any;
  public selectedTransports!: any[];
  public AllSelectedTransportCode: any[] = [];
  public AllSelectedTransportPrice: any[] = [];
  public errorMessage!: string;
  public transports: any;
  public billLists: any;
  public paymentsDaysPeriod = 0;
  dueDate: any
  facturationDate : any;
  currentDateDay: any;
  public transportList: any;
  public visibleInvoceConcerned: boolean = false;
  public formErrors: { [key: string]: string } = {};
  private validationMessages: { [key: string]: { [key: string]: string } } = {
    customerRef: { required: 'this field is require' },
    vat: { required: 'Ce champ est obligatoire' },
    description: { required: 'Ce champ est obligatoire' },
  }
  amount = 0;
  billId: any;
  customerVat: any;
  billDate: any;
  public isEdit: boolean = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private managementService: ManagementService,
    private datePipe: DatePipe,
    private billing: BillingService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.currentDate = this.datePipe.transform(Date(), 'dd/MM/yyyy');
    this.currentDateDay = new Date();
    this.searchCustomerForm = this.formBuilder.group({
      customerRef: ['']
    })

    this.customerInfoForm = this.formBuilder.group({
      vat: [''],
      facturationDate: [this.currentDateDay],
      dueDate: [''],
      description: [''],
      comment: [''],
      invoiceConcerned: [''],
    })

    this.addBillingForm = this.formBuilder.group({
      comment: [''],
      description: [''],
      customerId: [''],
      customerName: [''],
      customerVat: [''],
      customerAdress: [''],
      amount: [''],
      billDate: [''],
      dueDate: [''],
      vatValue: [''],
      transportCodes: ["string"],
      transportPrices: []
    })
  }


  setremise(){
    console.log("setRemise");
    console.log("isEdit");
    console.log(this.isEdit);
    // if(event == undefined || event == null){
    //   this.isEdit = false;
    // }{
    //   this.isEdit = true;
    // }

    console.log("isEdit");
    console.log(this.isEdit);
  }

  getCustomers(event: any) {
    this.body.param = event.query || "";
    this.body.status = "ACTIVATED";

    if (this.body.param.length >= 3) {
      this.managementService.getAllCustomer(this.body).subscribe({
        next: customers => {
          this.customerData = customers;
          this.customers = this.customerData.customers;
        }
      });
    }
  }

  addDate() {
    let date = new Date(this.customerInfoForm.get('facturationDate')?.value);
    date.setDate(date.getDate() + this.paymentsDaysPeriod);
    this.dueDate = date;
    this.customerInfoForm.patchValue({
      dueDate: date,
    })
  }

  getCustomerBillInformation(event: any) {
    this.billing.getCustomerBillingInformations(event.tva).subscribe({
      next: billingInfos => {
        this.billLists = billingInfos;
        console.log('this.billLists')
        console.log(this.billLists);

        let date = new Date(); 
        this.paymentsDaysPeriod = event.paymentsDaysPeriod;
        date.setDate(date.getDate() + this.paymentsDaysPeriod);
        this.dueDate = date;
        this.customerVat = event.tva
        this.customerInfoForm.patchValue({
          vat :event.tva,
          dueDate: date,
        })
    
        if(this.billLists.length){
          this.visibleInvoceConcerned = true;
        }else{
          this.visibleInvoceConcerned = false;
          this.messageService.add({ severity: "warn", summary: "Service Message", detail: 'This customer dont have Discount ' });
        }
      },
      error: error => this.errorMessage = error
    });
  }

  getTransportBillInformation(event: any) {
    console.log(event);
    this.billDate = event.billDate
    this.billId = event.billId;
    this.billing.getTransportsInformationsByBillId(event.billId).subscribe({
      next: transportInfos => {
        this.transportList = transportInfos;
        console.log('transportInfos');
        console.log(transportInfos);
      }
    });
  }

  saveCredit() {
    // for(let transport of this.transportList){
      
    //    for(let selectedTransp of this.selectedTransports){
            
    //     if (transport.transportCode == selectedTransp.transportCode ){
    //       transport.select = true;
    //     } 
    //    }
    // }

    let saveCreditObject = {
      backToReadyForBill : this.backToReadyForBill,
      customerVat: this.customerVat,
      comment: this.customerInfoForm.get('comment')?.value,
      billId: this.billId,
      billDate: new Date(this.customerInfoForm.get('facturationDate')?.value),
      dueDate: new Date(this.dueDate),
      transportReadyForAvoirs: this.selectedTransports
    }

    console.log(saveCreditObject);

    this.billing.saveCredit(saveCreditObject).subscribe({
      next: credit => {
        console.log(credit);
        this.messageService.add({ severity: "success", summary: "Service Message", detail: 'The Credit has been succesfull created ' });
        this.router.navigateByUrl('billing/list');
      },
      error: error => this.errorMessage = error
    })

    this.selectedTransports = [];
    
  }

}
