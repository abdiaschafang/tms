import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { TranslateConfigService } from 'src/app/core/services/translate-config/translate-config.service';
import { PrimeNGConfig } from 'primeng/api';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/core/services/menu.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  name!: string;
  menu: Array<any> = [];
  breadcrumbList: Array<any> = [];
  visibleSidebar1: any;
   today=new Date();
  date=String(this.today.getDate()).padStart(2, '0')+'/'+String(this.today.getMonth()+1).padStart(2, '0')+'/'+this.today.getFullYear()+' '+this.today.getHours()+':'+this.today.getMinutes();
  siteLanguage = 'English';
  languageList = [
    { code: 'en', label: 'English' },
    { code: 'fr', label: 'French' },
  ];


  constructor(
     private translateConfigService: TranslateConfigService,
    private authService: AuthService,
    private primengConfig: PrimeNGConfig,
    private _router: Router,
    private menuService: MenuService,
    private translate:TranslateService) { }

    langCode!: any;

    ngOnInit(): void {
      this.langCode=localStorage.getItem('user-language');
      this.primengConfig.ripple=true;
      this.menu = this.menuService.getMenu();
    this.listenRouting();
    }

    changeLanguage(type: string) {
      this.translateConfigService.changeLanguage(type);
      localStorage.setItem('user-language', type);
      this.langCode = type
  }

   changeSiteLanguage(localeCode: string): void {
    const selectedLanguage = this.languageList
      .find((language) => language.code === localeCode)
      ?.label.toString();
    if (selectedLanguage) {
      this.siteLanguage = selectedLanguage;
      this.translate.use(localeCode);
    }
    const currentLanguage = this.translate.currentLang;
    console.log('currentLanguage', currentLanguage);
  }


   listenRouting() {
    let routerUrl: string, routerList: Array<any>, target: any;
    this._router.events.subscribe((router: any) => {
      routerUrl = router.urlAfterRedirects;
      if (routerUrl && typeof routerUrl === 'string') {

        target = this.menu;
        this.breadcrumbList.length = 0;

        routerList = routerUrl.slice(1).split('/');
        routerList.forEach((router, index) => {

          target = target.find((page:any) => page.path.slice(2) === router);

          this.breadcrumbList.push({
            name: target.name,

            path: (index === 0) ? target.path : `${this.breadcrumbList[index-1].path}/${target.path.slice(2)}`
          });


          if (index+1 !== routerList.length) {
            target = target.children;
          }
        });

        console.log(this.breadcrumbList);
      }
    });
  }

    signout() {
      this.authService.signout();
    }
  }
