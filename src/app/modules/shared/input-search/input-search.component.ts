import { ManagementService } from 'src/app/core/services/management/management.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.scss']
})
export class InputSearchComponent implements OnInit {

  @Input() type!: string;
  @Input() typingCount: number=0;

  public items!: string[];
  public customerData!: any;
  public body={ "param": "", "status": "" };


  constructor(private managementService: ManagementService) { }

  ngOnInit() {

  }

  searchItem(param: string) {
    this.getCustomers(param);
  }

  getCustomers(param: string) {
    this.body.param=param;
    this.managementService.getAllCustomer(this.body).subscribe({
      next: customers => {
        this.customerData=customers;
        this.items=this.customerData.customers;
      }
    });
  }
}
