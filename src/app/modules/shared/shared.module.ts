
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { ReactiveFormsModule } from '@angular/forms';

import { AppButtonComponent } from './app-button/app-button.component';
import { AppBreadcrumbComponent } from './app-breadcrumb/app-breadcrumb.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import { PrimengModule } from './primeng.module';
import { FormsModule } from '@angular/forms';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MaterialModule } from './material.module';
import { ToggleSidebarComponent } from './toggle-sidebar/toggle-sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './map/map.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SeletonLoaderModule } from './seleton-loader/seleton-loader.module';
import { InputSearchComponent } from './input-search/input-search.component';
import { ButtonModule } from 'primeng/button';
import { SidebarModule } from 'primeng/sidebar';
import { LoaderComponent } from './loader/loader.component';



@NgModule({
  declarations: [
    AppButtonComponent,
    AppBreadcrumbComponent,
    NavBarComponent,
    SidebarComponent,
    ToggleSidebarComponent,
    MapComponent,
    InputSearchComponent,
    LoaderComponent

  ],
  imports: [
    CommonModule,
    PrimengModule,
    MaterialModule,
    TranslateModule,
    SidebarModule,
    ButtonModule,
    TranslateModule.forRoot(),
    FormsModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    SeletonLoaderModule,
  ],
  exports: [
    CommonModule,
    NgSelectModule,
    AppButtonComponent,
    AppBreadcrumbComponent,
    NavBarComponent,
    SidebarComponent,
    PrimengModule,
    MaterialModule,
    MapComponent,
    SeletonLoaderModule,
    InputSearchComponent,
    ReactiveFormsModule,
     LoaderComponent
  ]
})
export class SharedModule { }
