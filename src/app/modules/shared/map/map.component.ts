import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet'; 
import { ActivatedRoute } from '@angular/router';
import { MapService } from '../../../core/services/shared/mapservices/map.service';


const iconRetinaUrl = 'assets/img/marker-icon-2x.png';
const iconUrl = 'assets/img/marker-icon.png';
const shadowUrl = 'assets/img/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  private map : any; 

  constructor(private route: ActivatedRoute,
    private mapService: MapService,) { }

  public orderId: string = this.route.snapshot.paramMap.get('id') || '';

  ngOnInit(): void {
    this.map = L.map('map', {
      center: [ 53.0000, 9.0000 ],
      zoom: 3
    });
     const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
     });
    tiles.addTo(this.map);
    this.mapService.getOrderIdFormMapComponent(this.orderId);
    
  }

  ngAfterViewInit(): void {
    this.mapService.getOrderById(this.orderId);
    this.mapService.makeMarkersByOrderGeolocationId(this.map); 
  }

}
