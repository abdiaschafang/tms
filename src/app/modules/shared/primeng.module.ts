import { NgModule } from '@angular/core';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { MenuModule } from 'primeng/menu';
import { ToolbarModule } from 'primeng/toolbar';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { ImageModule } from 'primeng/image';
import { SidebarModule } from 'primeng/sidebar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ContextMenuModule } from 'primeng/contextmenu';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { BadgeModule } from 'primeng/badge';
import { RippleModule } from 'primeng/ripple';
import { DividerModule } from 'primeng/divider';
import { TagModule } from 'primeng/tag';
import { InputNumberModule } from 'primeng/inputnumber';
import { PaginatorModule } from 'primeng/paginator';
import { GMapModule } from 'primeng/gmap';
import { FieldsetModule } from 'primeng/fieldset';
import { AccordionModule } from 'primeng/accordion';
import { ListboxModule } from 'primeng/listbox';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SplitButtonModule } from 'primeng/splitbutton';
import { MultiSelectModule } from 'primeng/multiselect';
import { MessagesModule } from 'primeng/messages';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TabViewModule } from 'primeng/tabview';
import { TooltipModule } from 'primeng/tooltip';
import {PickListModule} from 'primeng/picklist';
import {CheckboxModule} from 'primeng/checkbox';
import {InputSwitchModule} from 'primeng/inputswitch';
import { NgSelectModule } from '@ng-select/ng-select';




@NgModule({
  declarations: [],
  exports: [
    MenubarModule,
    ButtonModule,
    InputTextModule,
    MenuModule,
    ToolbarModule,
    AvatarModule,
    AvatarGroupModule,
    ImageModule,
    SidebarModule,
    PanelMenuModule,
    CardModule,
    TableModule,
    ToastModule,
    ContextMenuModule,
    InputTextareaModule,
    DialogModule,
    DropdownModule,
    CalendarModule,
    BadgeModule,
    RippleModule,
    DividerModule,
    TagModule,
    InputNumberModule,
    PaginatorModule,
    GMapModule,
    FieldsetModule,
    AccordionModule,
    ListboxModule,
    ConfirmPopupModule,
    ConfirmDialogModule,
    SplitButtonModule,
    MultiSelectModule,
    ConfirmDialogModule,
    ButtonModule,
    MessagesModule,
    OverlayPanelModule,
    AutoCompleteModule,
    TabViewModule,
    PickListModule,
    TooltipModule,
    CheckboxModule,
    InputSwitchModule,
    NgSelectModule
  ]
})
export class PrimengModule { }
