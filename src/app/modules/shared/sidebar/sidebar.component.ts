import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { iconAnimation, labelAnimation, sidebarAnimation } from 'src/app/animations/animations';
import { SidebarService } from 'src/app/core/services/sidebar/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    sidebarAnimation(),
    iconAnimation(),
    labelAnimation(),
  ]
})
export class SidebarComponent implements OnInit {

  public sidebarState!: string;
  public id: string = 'aa54f3c0-31bb-47ff-bf9e-6a61a1b359f8';

  items = [
    {
      name: 'Orders',
      clicked: true,
      urlIcon: '../assets/img/grid.png',
      url:'commercial/list'
    },
    {
      name: 'Exploitation',
      clicked: false,
      urlIcon: '../assets/img/layers.png',
      url:'exploitation/list'
    },
    {
      name: 'Transport',
      clicked: false,
      urlIcon: '../assets/img/truck.png',
      url:'transport/index'
    },
    {
      name: 'Management',
      clicked: false,
      urlIcon: '../assets/img/cpu.png',
      url:'management/index'
    },
    {
      name: 'Monotoring',
      clicked: false,
      urlIcon: '../assets/img/copy.png',
      url:'monitoring/list'
    },
    {
      name: 'Customer Billing',
      clicked: false,
      urlIcon: '../assets/img/copy.png',
      url:'billing/list'
    },
    {
      name: 'Carrier Billing',
      clicked: false,
      urlIcon: '../assets/img/copy.png',
      url:'transporterBilling/list'
    },
  ];
  constructor(
    private sidebarService: SidebarService,
    private router: Router) { }


  ngOnInit(): void {
    this.sidebarService.sidebarStateObservable$.
      subscribe((newState: string) => {
        this.sidebarState = newState;
      });
  }

  isActiveRouteCommercial() {
    switch (this.router.url) {
      case '/commercial/list':
        return true;
        case '/commercial/create':
        return true;
        case `/commercial/update/${this.id}`:
        return true;
        case `/commercial/detail/${this.id}`:
        return true;
      default:
        break;
    }
    return false;
  }

  isActiveRouteExploitation() {
    return ["exploitation/list", "exploitation/detail/:id"]
  }

   onClick(i: any) {
    for (let item of this.items) {
      item.clicked = false;
    }
    this.items[i].clicked = true;
  }
}
