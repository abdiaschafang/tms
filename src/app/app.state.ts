import { IOrder } from './core/models/order'

export interface AppState {
  readonly order: IOrder[]
}
