// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment={
  production: false,
  microserviceCommercial: 'http://192.168.10.117:9001/api/commercial/v1',
  microserviceExploitation: 'http://192.168.10.117:9002/api/exploitation/v1',
  microserviceManagement: 'http://192.168.10.117:9004/api/management/v1',
  restCountries: 'http://192.168.10.117:9010/api/countries/v1',
  restCities: 'http://192.168.10.117:9010/api/countries/v1',
  microserviceTransport: 'http://192.168.10.117:9003/api/transport/tracking/v1',
  microserviceBilling: 'http://192.168.10.117:9005/api/billing/v1',
  microserviceCarrierBilling: 'http://192.168.10.25:9005/api/billing/v1',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
